Using BSMEM from the console
============================

This document explains how to use the “traditional” BSMEM console
application, which combines a command-line interface for specifying
the input parameters with plotting using the PGPLOT library. The
“iterative reconstruction mode”, where commands could be typed at the
console to control the algorithm, has been temporarily removed.

Setting up the reconstruction parameters
----------------------------------------

The reconstruction parameters are set up using command line arguments.
Type `bsmem -h` to get help on the available options. After processing
its command line options, BSMEM will prompt before proceeding with the
reconstruction (unless you used the `-noui` flag).

Models in BSMEM
---------------

The prior probability in BSMEM is currently the Gull-Skilling entropy
function H:

$$ H(i_k)= i_k - m_k + i_k \log (i_k/m_k) $$

where $i_k$ is the kth pixel of the image $i$, and $m_k$ the kth pixel
of the model $m$.

The default model used is a Gaussian of FWHM given by the `-mw`
command-line option, but a flat, Dirac delta-function, uniform disk or
Lorentzian model can be specified using the `-mt` option. All are
normalized to the flux given by the `-mf` option (default 0.01).  The
flat model does not introduce any particular “bias” and allows for
truly model independent reconstruction, while the other model types
are helpful to constrain the position of the center of the map which
is floating for bispectrum + powerspectrum reconstructions.

An arbitrary model can be read from a FITS image file using
the `-sf` command-line option.
