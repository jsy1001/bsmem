# BSMEM common interface

The bsmem-ci executable implements the common interface defined in the
document "Interface to Image Reconstruction" (available at
<https://github.com/JMMC-OpenDev/OI-Imaging-JRA/>). This program has no
built-in capability for displaying the data or reconstructed image,
but relies on a third-party user interface to provide this
functionality.

## Command-line interface

Usage is as follows:

    bsmem-ci [-clobber] INPUTFILE [OUTPUTFILE]

The `-clobber` argument allows the output file to be overwritten if it
already exists. If the output filename is omitted, the default
filename `output.fits` (in the current directory) is used.

## Process management

The bsmem-ci program is a single process, and would normally be
spawned as a sub-process of the user interface application. The input
FITS file is read when the process starts. A single output FITS file
is written. The output file is updated after each iteration, and when
the reconstruction has completed. Updates to the output file are made
by creating a new output file in a temporary sub-directory of the
output directory, then renaming the temporary file to the output
file.

Success or failure of the image reconstruction should be inferred from
the exit status of the process: 0 indicates success; 1 failure; or 2
that the command-line options were invalid. Details of any failure are
written to standard output.

## Inputs

The program accepts the standard parameters listed in Table 1 of the
interface document, with the following exceptions/clarifications:

- The `USE_VIS` parameter is ignored, as BSMEM does not currently make
  use of complex visibility data.

- `RGL_NAME` is ignored, as BSMEM only supports a single
  regularization method (Maximum Entropy).

- If a prior image is not supplied, the initial image is used as the
  prior image.

- `WAVE_MIN` and `WAVE_MAX` are assumed to be in units of metres.

- `CDELT1` and `CDELT2` are assumed to be in units of degrees.

- The `TARGET` parameter may have a blank value (refer to the FITS
  specification) or be omitted altogether. In both cases the first
  target in the OI_TARGET table (regardless of its ID number) will be
  selected. If `TARGET` has a string value, the target whose name
  matches is selected.

The following non-standard parameters are accepted:

`UV_MAX` (real)
:   Maximum UV radius to select /waves

`FLUXERR` (real)
:   Error on zero-baseline powerspectrum used to enforce total flux
    (the value of this powerspectrum is given by the square of the
    standard `FLUX` parameter)

`INITFLUX` (real)
:   Initial image flux (the initial image specifies the relative pixel
    values)

`V2A` (real), `V2B` (real)
:   Adjust squared visibility errors (e'= a * e + b)

`T3AMPA` (real), `T3AMPB` (real)
:   Adjust triple amplitude errors (e'= a * e + b)

`T3PHIA` (real), `T3PHIB` (real)
:   Adjust closure phase errors (e'= a * e + b)

`FAKET3A` (string)
:   Triple amplitudes to fake (`MISSING`, `ALL`, or `NONE`)

Further BSMEM-specific parameters may be added in a future release.

All of the parameters understood by BSMEM are written to the output
file, to provide a complete record of the inputs. If the target name
was not specified, a blank value is written for the `TARGET` keyword
(rather than the name of the selected target).

## Outputs

The standard keywords `PROCSOFT`, `LAST_IMG`, `NITER`, `CHISQ`, `FLUX`
and `CONVERGE` are written to the output parameters HDU. BSMEM also
writes the following non-standard parameters:

`ENTROPY` (real)
:   Entropy value

`RGL_WGT` (real)
:   Regularization weight (perhaps chosen automatically)

A copy of the input data is written to the output file. Only the
tables containing selected data are copied. Within the tables,
unselected data are replaced by NULL values.

Model data, corresponding to the selected input data only, are written
to the output file using the additional columns defined in Table 4 of
the interface document.

## Bugs

The temporary directory used to update the output file is not always
deleted when an error or program crash occurs.

<!-- Local Variables: -->
<!-- fill-column: 70 -->
<!-- End: -->
