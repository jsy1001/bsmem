/**
 * @file
 * BSMEM application with common interface.
 *
 * Copyright (C) 2015-2017 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

/*
 * The single-file input/output interface is described at
 * https://github.com/JMMC-OpenDev/OI-Imaging-JRA .
 */

#include "bsmem-config.h"
#include "BsmemRunner.h"

#include <glib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
  int clobber;
  char inputFilename[FLEN_FILENAME];
  char outFilename[FLEN_FILENAME];

} BsmemOptions;

/**
 * Print help on command-line options.
 */
static void print_usage(const char *argv0)
{
  printf("Usage: %s [OPTIONS] INPUTFILE [OUTPUTFILE]\n", argv0);
  printf("-h:\t\t Display this information.\n");
  printf("-clobber:\t Overwrite existing output file.\n");
  printf("-ver:\t\t Version info.\n");
}

/**
 * Set default option values.
 */
static void init_options(BsmemOptions *pOpt)
{
  g_assert(pOpt != NULL);
  pOpt->clobber = 0;
  pOpt->inputFilename[0] = '\0';
  strcpy(pOpt->outFilename, "output.fits");
}

/**
 * Parse command line options in @a argv.
 *
 * Prints an error message and calls exit() on error.
 */
static void parse_options(int argc, char *argv[], BsmemOptions *pOpt)
{
  g_assert(pOpt != NULL);
  init_options(pOpt);

  int i = 0;
  while (++i < argc && argv[i][0] == '-')
  {
    if (strcmp(argv[i], "-clobber") == 0)
    {
      pOpt->clobber = 1;
    }
    else if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
    {
      print_usage(argv[0]);
      exit(0);
    }
    else if (strcmp(argv[i], "-ver") == 0)
    {
      printf("%s %s\n", argv[0], bsmem_VERSION);
      exit(0);
    }
    else
    {
      printf("A parameter was not recognized: '%s'\n", argv[i]);
      printf("Type '%s -h' to display the command line help.\n", argv[0]);
      exit(2);
    }
  }
  if (i < argc)
    g_strlcpy(pOpt->inputFilename, argv[i++], sizeof(pOpt->inputFilename));
  if (i < argc)
    g_strlcpy(pOpt->outFilename, argv[i++], sizeof(pOpt->outFilename));

  if (pOpt->inputFilename[0] == '\0')
  {
    printf("Input file not specified -> Exiting...\n");
    exit(2);
  }
  if (pOpt->outFilename[0] == '\0')
  {
    printf("Output file not specified -> Exiting...\n");
    exit(2);
  }
}

int main(int argc, char *argv[])
{
  BsmemOptions opt;
  int ret = 1;

  parse_options(argc, argv, &opt);
  if (!opt.clobber && g_file_test(opt.outFilename, G_FILE_TEST_EXISTS))
  {
    printf("Output file '%s' exists and '-clobber' not specified "
           "-> Exiting...\n",
           opt.outFilename);
    exit(ret);
  }

  e4c_using_context(E4C_TRUE)
  {
    BsmemRunner *runner = NULL;
    int niter = 0;
    try
    {
      runner = BsmemRunnerFromInput(opt.inputFilename);
      BsmemRunnerPrintParam(runner);

      while (!BsmemRunnerIterate(runner, 1, NULL))
      {
        BsmemRunnerWriteTo(runner, opt.outFilename, true);
        niter = runner->output->niter;
      }
      BsmemRunnerWriteTo(runner, opt.outFilename, true);
      niter = runner->output->niter;
      DestroyBsmemRunner(runner);
      ret = 0;
    }
    catch (RuntimeException)
    {
      const e4c_exception *exception = e4c_get_exception();
      printf("\n*** Image reconstruction failed ***\n");
      printf("%s: %s\n\n", exception->name, exception->message);
      if (runner->output->niter > niter)
      {
        g_debug("Writing pre-failure results");
        BsmemRunnerWriteTo(runner, opt.outFilename, true);
      }
    }
  }
  exit(ret);
}
