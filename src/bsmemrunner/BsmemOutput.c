/**
 * @file
 * Implementation of BsmemOutput class.
 *
 * Copyright (C) 2015-2017, 2022 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "bsmem-config.h"
#include "BsmemOutput.h"
#include "e4c_fitsio.h"

#include <glib.h>

#include <string.h>

#define OUTPUT_PARAM_NAME "IMAGE-OI OUTPUT PARAM"

/**
 * @memberof BsmemOutput
 */
BsmemOutput *CreateBsmemOutput(void)
{
  BsmemOutput *this = g_malloc(sizeof(*this));
  this->last_img[0] = '\0';
  this->niter = 0;
  this->converge = false;
  this->chisq = 0.0F;
  this->entropy = 0.0F;
  this->flux = 0.0F;
  this->alpha = 0.0F;

  return this;
}

static void writefloatkey(fitsfile *fits, const char *keyname, float value,
                          const char *comment)
{
  if (keyname == NULL)
    throw(NullPointerException, "Argument 'keyname' is NULL.");
  if (comment == NULL)
    throw(NullPointerException, "Argument 'comment' is NULL.");

  int status = 0;
  fits_write_key(fits, TFLOAT, keyname, &value, comment, &status);
  if (status == BAD_F2C)
    g_warning("Not writing bad value %s = %f to output parameters - %s",
              keyname, value, e4c_fits_error_report(status));
  else if (status)
    throwf(FitsException, "Failed to write %s to output parameters - %s",
           keyname, e4c_fits_error_report(status));
}

/**
 * @memberof BsmemParam
 */
void BsmemOutputAppendTo(BsmemOutput *this, const char *filename)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");

  /* Write output parameters HDU */
  fitsfile *fits;
  e4c_using_fits(fits, filename, FITS_OPEN_READWRITE)
  {
    int status = 0;
    fits_create_tbl(fits, BINARY_TBL, 0, 0, NULL, NULL, NULL, OUTPUT_PARAM_NAME,
                    &status);
    if (status)
      throwf(FitsException, "Failed to create output parameters table - %s",
             e4c_fits_error_report(status));

    /* Write version */
    char *procsoft = "bsmem " bsmem_VERSION;
    fits_write_key(fits, TSTRING, "PROCSOFT", procsoft,
                   "Image reconstruction software and version", &status);

    /* Write parameters */
    fits_write_key(fits, TSTRING, "LAST_IMG", this->last_img,
                   "Identifier of the final image HDU", &status);
    fits_write_key(fits, TINT, "NITER", &this->niter,
                   "Total iterations done in current program run", &status);
    int converge = this->converge;
    fits_write_key(fits, TLOGICAL, "CONVERGE", &converge,
                   "Did the reconstruction converge?", &status);
    if (status)
      throwf(FitsException, "Failed to write output parameters - %s",
             e4c_fits_error_report(status));

    writefloatkey(fits, "CHISQ", this->chisq, "Reduced chi-squared");
    writefloatkey(fits, "ENTROPY", this->entropy, "Entropy value");
    writefloatkey(fits, "FLUX", this->flux, "Total image flux");
    writefloatkey(fits, "RGL_WGT", this->alpha, "Weight of the regularization");
  }
}

/**
 * @memberof BsmemOutput
 */
void DestroyBsmemOutput(BsmemOutput *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  g_free(this);
}
