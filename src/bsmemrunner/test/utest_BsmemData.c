/**
 * @file
 * Unit tests of BsmemData class.
 *
 * Copyright (C) 2015-2018, 2021-2023 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "BsmemData.h"
#include "e4c_fitsio.h"

#include <oiiter.h>
#include <oicheck.h>
#include <glib.h>

#include <math.h>
#include <string.h>

#define WAVE_MAX 1.0F
#define UV_MAX 1.0E11
#define DIR "OIFITS1/"
#define INPUT_FILENAME "utest_GreyImg_in.fits"
#define OUTPUT_FILENAME "utest_BsmemData_out.fits"
#define TOL 1E-5F

typedef struct
{
  const char *datafile;
  float wave_min;
  float wave_max;
  float uv_max;
  int npow;
  int nbis;
  int nuv; /**< number of unique uv points including one at zero frequency */

} TestCase;

#define assert_float_near(a, b, tol)                                           \
  do                                                                           \
  {                                                                            \
    if (fabs((a) - (b)) >= (tol))                                              \
    {                                                                          \
      fprintf(stderr,                                                          \
              "ERROR:%s:%d:%s: "                                               \
              "%f differs significantly from %f\n",                            \
              __FILE__, __LINE__, __func__, (a), (b));                         \
      abort();                                                                 \
    }                                                                          \
  } while (0)

typedef struct
{
  int dummy;

} TestFixture;

static const TestCase cases[] = {
    {DIR "Contest12_H.oifits", 0.0F, WAVE_MAX, UV_MAX, 601, 800, 601},
    {DIR "Contest1_JHK.oifits", 0.0F, WAVE_MAX, 1.5E8, 1120, 886, 1120},
    {DIR "Contest1_JHK.oifits", 0.0F, WAVE_MAX, UV_MAX, 1801, 2400, 1801},
    {DIR "Mystery-Low_HK_1949nm.oifits", 0.0F, WAVE_MAX, UV_MAX, 181, 61, 184},
    {DIR "Mystery-Low_HK_flagt3.oifits", 0.0F, WAVE_MAX, UV_MAX, 3627, 1151,
     3631},
    {DIR "Mystery-Low_HK_flagvis2.oifits", 0.0F, WAVE_MAX, UV_MAX, 995, 1220,
     3661},
    {DIR "Mystery-Low_HK.oifits", 0.0F, WAVE_MAX, UV_MAX, 3661, 1220, 3661},
    {DIR "Mystery-Low_HK.oifits", 0.0F, WAVE_MAX, 3.5E7, 2359, 577, 2359},
    {DIR "Mystery-Low_HK_split.oifits", 0.0F, WAVE_MAX, UV_MAX, 3627, 1220,
     3661},
    {DIR "Mystery-Low_HK_t3.oifits", 0.0F, WAVE_MAX, UV_MAX, 1, 1220, 3661},
    {DIR "Mystery-Low_HK_vis2.oifits", 0.0F, WAVE_MAX, UV_MAX, 3627, 0, 3627},
    {DIR "Mystery-Low_HK_vis2_t3phi.oifits", 0.0F, WAVE_MAX, UV_MAX, 3627, 1220,
     3661},
    {DIR "Mystery-Med_H-AmberVISPHI_vis.oifits", 0.0F, WAVE_MAX, UV_MAX, 1, 0,
     1},
    {DIR "Contest1_JHK.oifits", 1520E-9F, 1580E-9F, UV_MAX, 151, 200, 151},
    {DIR "Mystery-Low_HK.oifits", 1600E-9F, 1800E-9F, UV_MAX, 733, 244, 733}};

static const int numCases = sizeof(cases) / sizeof(cases[0]);

static gboolean ignore_harmless(const char *logDomain, GLogLevelFlags logLevel,
                                const char *message, gpointer userData)
{
  return (
      !(g_str_has_suffix(message, "failed because of missing powerspectrum") ||
        strstr(message, "Selecting first target") != NULL));
}

static void delete_fits(const char *filename)
{
  fitsfile *fits;
  int status = 0;

  fits_open_file(&fits, filename, READWRITE, &status);
  fits_delete_file(fits, &status);
  if (status)
    g_error("Failed to delete FITS file '%s' - CFITSIO error %d", filename,
            status);
}

static void check_data(const BsmemData *data)
{
  g_assert_cmpint(data->nuv, <=, 1 + data->npow + 3 * data->nbis);
  g_assert_cmpint(data->pow[0].extver, <, 0);
  g_assert_cmpint(data->pow[0].irec, <, 0);
  g_assert_cmpint(data->pow[0].iwave, <, 0);
  for (int ipow = 0; ipow < data->npow; ipow++)
  {
    g_assert_cmpint(data->pow[ipow].ab.iuv, >=, 0);
    g_assert_cmpint(data->pow[ipow].ab.iuv, <, data->nuv);
    g_assert_cmpint(abs(data->pow[ipow].ab.sign), ==, 1);
  }
  for (int ibis = 0; ibis < data->nbis; ibis++)
  {
    g_assert_cmpint(data->bis[ibis].ab.iuv, >=, 0);
    g_assert_cmpint(data->bis[ibis].ab.iuv, <, data->nuv);
    g_assert_cmpint(abs(data->bis[ibis].ab.sign), ==, 1);

    g_assert_cmpint(data->bis[ibis].bc.iuv, >=, 0);
    g_assert_cmpint(data->bis[ibis].bc.iuv, <, data->nuv);
    g_assert_cmpint(abs(data->bis[ibis].bc.sign), ==, 1);

    g_assert_cmpint(data->bis[ibis].ca.iuv, >=, 0);
    g_assert_cmpint(data->bis[ibis].ca.iuv, <, data->nuv);
    g_assert_cmpint(abs(data->bis[ibis].ca.sign), ==, 1);

    g_assert_cmpint(data->bis[ibis].ab.iuv, !=, data->bis[ibis].bc.iuv);
    g_assert_cmpint(data->bis[ibis].ab.iuv, !=, data->bis[ibis].ca.iuv);
    g_assert_cmpint(data->bis[ibis].bc.iuv, !=, data->bis[ibis].ca.iuv);
  }
}

static void check_oi_fits(const oi_fits *pData)
{
  check_func checks[] = {check_tables,
                         check_header,
                         check_keywords,
                         check_visrefmap,
                         check_unique_targets,
                         check_targets_present,
                         check_elements_present,
                         check_corr_present,
                         check_flagging,
                         check_t3amp,
                         check_waveorder,
                         check_time,
                         check_flux,
                         NULL};

  /* Run checks */
  oi_breach_level worst = OI_BREACH_NONE;
  int i = 0;
  while (checks[i] != NULL)
  {
    oi_check_result result;
    if ((*checks[i++])(pData, &result) != OI_BREACH_NONE)
    {
      print_check_result(&result);
      g_warning("Check failed");
      if (result.level > worst) worst = result.level;
    }
    free_check_result(&result);
  }

  if (worst == OI_BREACH_NONE) g_debug("All checks passed");

  g_assert_cmpint(pData->numArray, ==, g_list_length(pData->arrayList));
  g_assert_cmpint(pData->numWavelength, ==,
                  g_list_length(pData->wavelengthList));
  g_assert_cmpint(pData->numCorr, ==, g_list_length(pData->corrList));
  g_assert_cmpint(pData->numInspol, ==, g_list_length(pData->inspolList));
  g_assert_cmpint(pData->numVis, ==, g_list_length(pData->visList));
  g_assert_cmpint(pData->numVis2, ==, g_list_length(pData->vis2List));
  g_assert_cmpint(pData->numT3, ==, g_list_length(pData->t3List));
  g_assert_cmpint(pData->numFlux, ==, g_list_length(pData->fluxList));

  g_assert_cmpint(pData->numArray, ==, g_hash_table_size(pData->arrayHash));
  g_assert_cmpint(pData->numWavelength, ==,
                  g_hash_table_size(pData->wavelengthHash));
  g_assert_cmpint(pData->numCorr, ==, g_hash_table_size(pData->corrHash));
}

static void uncaught_handler(const e4c_exception *exception)
{
  e4c_print_exception(exception);
  g_error("Uncaught exception");
}

static void setup_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_begin(E4C_FALSE);
  e4c_context_set_handlers(uncaught_handler, NULL, NULL, NULL);
}

static void teardown_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_end();
}

static void test_create(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = CreateBsmemParam();
  param->flux = 0.8F;
  param->fluxerr = 1E-3F;
  BsmemData *data = CreateBsmemData(param);
  g_assert_cmpfloat(data->pow[0].pow, ==, param->flux * param->flux);
  g_assert_cmpfloat(data->pow[0].powerr, ==, param->fluxerr);
  DestroyBsmemParam(param);
  check_data(data);
  DestroyBsmemData(data);
}

// TODO: force extrapolation and compare
static void test_from_oi_fits(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = CreateBsmemParam();
  g_test_log_set_fatal_handler(ignore_harmless, NULL);

  for (int i = 0; i < numCases; i++)
  {
    BsmemData *data = NULL;
    g_message("Scanning %s", cases[i].datafile);
    param->wave_min = cases[i].wave_min;
    param->wave_max = cases[i].wave_max;
    param->uv_max = cases[i].uv_max;
    param->use_vis2 = true;
    param->use_t3 = USE_ALL;
    /* Select VIS2 & T3 */
    data = BsmemDataFromFilename(cases[i].datafile, param);
    g_assert_cmpint(data->npow, ==, cases[i].npow);
    g_assert_cmpint(data->nbis, ==, cases[i].nbis);
    g_assert_cmpint(data->nuv, ==, cases[i].nuv);
    check_data(data);
    DestroyBsmemData(data);
    /* Select VIS2 only */
    param->use_vis2 = true;
    param->use_t3 = USE_NONE;
    data = BsmemDataFromFilename(cases[i].datafile, param);
    g_assert_cmpint(data->npow, ==, cases[i].npow);
    g_assert_cmpint(data->nbis, ==, 0);
    check_data(data);
    DestroyBsmemData(data);
    /* Select T3 only */
    param->use_vis2 = false;
    param->use_t3 = USE_ALL;
    data = BsmemDataFromFilename(cases[i].datafile, param);
    g_assert_cmpint(data->npow, ==, 1);
    g_assert_cmpint(data->nbis, ==, cases[i].nbis);
    check_data(data);
    DestroyBsmemData(data);
    /* Select VIS2 and T3PHI */
    param->use_vis2 = true;
    param->use_t3 = USE_PHI; /* => all T3PHI missing */
    param->fake_t3amp = FAKE_MISSING;
    data = BsmemDataFromFilename(cases[i].datafile, param);
    g_assert_cmpint(data->npow, ==, cases[i].npow);
    g_assert_cmpint(data->nbis, ==, cases[i].nbis);
    for (int j = 0; j < data->nbis; j++)
      g_assert_true(data->bis[j].fakeamp);
    check_data(data);
    DestroyBsmemData(data);
  }
  DestroyBsmemParam(param);
}

static void test_find_uv(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = BsmemParamFromFilename(INPUT_FILENAME);
  BsmemData *data = BsmemDataFromFilename(INPUT_FILENAME, param);
  DestroyBsmemParam(param);
  for (int i = 0; i < data->nuv; i++)
  {
    BsmemUVRef ref;
    g_assert(BsmemDataFindUV(data, data->uv[i], 1E-5F, &ref));
    g_assert_cmpint(ref.iuv, ==, i);
  }
  DestroyBsmemData(data);
}

static void test_get_pow_uv(BsmemData *data, oi_filter_spec *pFilter)
{
  oi_vis2_iter iter;

  oi_vis2_iter_init(&iter, &data->origdata, pFilter);
  for (int ipow = 1; ipow < data->npow; ipow++)
  {
    g_assert(oi_vis2_iter_next(&iter, NULL, NULL, NULL, NULL, NULL));
    double u, v;
    oi_vis2_iter_get_uv(&iter, NULL, &u, &v);

    BsmemUV ab = BsmemDataGetPowAB(data, ipow);
    assert_float_near(ab.u, (float)u, TOL);
    assert_float_near(ab.v, (float)v, TOL);
  }
}

static void test_get_bis_uv(BsmemData *data, oi_filter_spec *pFilter)
{
  oi_t3_iter iter;

  oi_t3_iter_init(&iter, &data->origdata, pFilter);
  for (int ibis = 0; ibis < data->nbis; ibis++)
  {
    g_assert(oi_t3_iter_next(&iter, NULL, NULL, NULL, NULL, NULL));
    double u1, v1, u2, v2;
    oi_t3_iter_get_uv(&iter, NULL, &u1, &v1, &u2, &v2);

    BsmemUV ab = BsmemDataGetBisAB(data, ibis);
    assert_float_near(ab.u, (float)u1, TOL);
    assert_float_near(ab.v, (float)v1, TOL);
    BsmemUV bc = BsmemDataGetBisBC(data, ibis);
    assert_float_near(bc.u, (float)u2, TOL);
    assert_float_near(bc.v, (float)v2, TOL);
    BsmemUV ca = BsmemDataGetBisCA(data, ibis);
    assert_float_near(ca.u, (float)(-u1 - u2), TOL);
    assert_float_near(ca.v, (float)(-v1 - v2), TOL);
  }
}

static void test_get_uv(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = BsmemParamFromFilename(INPUT_FILENAME);
  BsmemData *data = BsmemDataFromFilename(INPUT_FILENAME, param);

  oi_filter_spec filt;
  BsmemParamSetFilter(param, &filt);
  if (strlen(param->target) > 0)
  {
    target *pTarg =
        oi_fits_lookup_target_by_name(&data->origdata, param->target);
    filt.target_id = pTarg->target_id;
  }
  DestroyBsmemParam(param);
  test_get_pow_uv(data, &filt);
  test_get_bis_uv(data, &filt);
  DestroyBsmemData(data);
}

static void test_calc(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = BsmemParamFromFilename(INPUT_FILENAME);
  BsmemData *data = BsmemDataFromFilename(INPUT_FILENAME, param);
  DestroyBsmemParam(param);
  float complex *vis = g_malloc0(data->nuv * sizeof(vis[0]));
  for (int ipow = 0; ipow < data->npow; ipow++)
    BsmemDataCalcModelPow(data, ipow, vis);
  for (int ibis = 0; ibis < data->nbis; ibis++)
    BsmemDataCalcModelBis(data, ibis, vis);
  g_free(vis);
  DestroyBsmemData(data);
}

static void test_write_to(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = BsmemParamFromFilename(INPUT_FILENAME);
  param->wave_max = 1650E-9F;
  BsmemData *data = BsmemDataFromFilename(INPUT_FILENAME, param);
  DestroyBsmemParam(param);
  BsmemDataWriteTo(data, OUTPUT_FILENAME, true);
  DestroyBsmemData(data);

  oi_fits result;
  int status = 0;
  read_oi_fits(OUTPUT_FILENAME, &result, &status);
  g_assert(!status);
  check_oi_fits(&result);
  free_oi_fits(&result);

  delete_fits(OUTPUT_FILENAME);
}

static void test_append_to(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = BsmemParamFromFilename(INPUT_FILENAME);
  BsmemData *data = BsmemDataFromFilename(INPUT_FILENAME, param);
  DestroyBsmemParam(param);
  fitsfile *fits;
  e4c_using_fits(fits, OUTPUT_FILENAME, FITS_CLOBBER)
  {
    int status = 0, nhdu;
    if (fits_get_num_hdus(fits, &nhdu, &status) == 0)
    {
      /* primary HDU doesn't exist, so create it */
      fits_create_img(fits, BYTE_IMG, 0, NULL, &status);
    }
    else
    {
      fits_movabs_hdu(fits, 1, NULL, &status);
    }
    fits_write_key(fits, TSTRING, "OBSERVER", "A. Michelson", NULL, &status);
    if (status)
      throwf(FitsException, "Error writing primary header keyword - %s",
             e4c_fits_error_report(status));
  }
  BsmemDataAppendTo(data, OUTPUT_FILENAME);
  DestroyBsmemData(data);

  oi_fits result;
  int status = 0;
  read_oi_fits(OUTPUT_FILENAME, &result, &status);
  g_assert(!status);
  check_oi_fits(&result);
  free_oi_fits(&result);

  delete_fits(OUTPUT_FILENAME);
}

int main(int argc, char *argv[])
{
  g_test_init(&argc, &argv, NULL);

  g_test_add("/bsmem/BsmemData/create", TestFixture, NULL, setup_fixture,
             test_create, teardown_fixture);
  g_test_add("/bsmem/BsmemData/from_oi_fits", TestFixture, NULL, setup_fixture,
             test_from_oi_fits, teardown_fixture);
  g_test_add("/bsmem/BsmemData/find_uv", TestFixture, NULL, setup_fixture,
             test_find_uv, teardown_fixture);
  g_test_add("/bsmem/BsmemData/get_uv", TestFixture, NULL, setup_fixture,
             test_get_uv, teardown_fixture);
  g_test_add("/bsmem/BsmemData/calc", TestFixture, NULL, setup_fixture,
             test_calc, teardown_fixture);
  g_test_add("/bsmem/BsmemData/write_to", TestFixture, NULL, setup_fixture,
             test_write_to, teardown_fixture);
  g_test_add("/bsmem/BsmemData/append_to", TestFixture, NULL, setup_fixture,
             test_append_to, teardown_fixture);

  return g_test_run();
}
