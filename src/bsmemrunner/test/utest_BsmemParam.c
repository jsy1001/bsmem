/**
 * @file
 * Unit tests of BsmemParam class.
 *
 * Copyright (C) 2015-2018, 2021-2023 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "BsmemParam.h"
#include "e4c_fitsio.h"

#include <glib.h>

#include <stdio.h>
#include <string.h>

#define INPUT_STD_FILENAME "utest_BsmemParam_std.fits"
#define INPUT_ALL_FILENAME "utest_BsmemParam_all.fits"
#define OUTPUT_FILENAME "utest_BsmemParam_out.fits"

typedef struct
{
  int dummy;

} TestFixture;

static void delete_fits(const char *filename)
{
  fitsfile *fits;
  int status = 0;

  fits_open_file(&fits, filename, READWRITE, &status);
  fits_delete_file(fits, &status);
  if (status)
    g_error("Failed to delete FITS file '%s' - CFITSIO error %d", filename,
            status);
}

static void uncaught_handler(const e4c_exception *exception)
{
  e4c_print_exception(exception);
  g_error("Uncaught exception");
}

static void setup_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_begin(E4C_FALSE);
  e4c_context_set_handlers(uncaught_handler, NULL, NULL, NULL);
}

static void teardown_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_end();
}

static void debug_param(const BsmemParam *param, bool showall)
{
  g_debug("=== MANDATORY PARAMETERS ===");
  g_debug("INIT_IMG='%s'", param->initimgname);
  g_debug("WAVE_MIN=%g", param->wave_min);
  g_debug("WAVE_MAX=%g", param->wave_max);
  g_debug("USE_VIS='%s'", useAmpPhiName[param->use_vis]);
  g_debug("USE_VIS2=%c", param->use_vis2 ? 'T' : 'F');
  g_debug("USE_T3='%s'", useAmpPhiName[param->use_t3]);
  g_debug("MAXITER=%d", param->maxiter);
  if (showall)
  {
    g_debug("=== OPTIONAL PARAMETERS (standard) ===");
    g_debug("AUTO_WGT=%c", (param->regularization == 3) ? 'F' : 'T');
    g_debug("RGL_WGT=%g", param->alpha);
    g_debug("RGL_PRIO='%s'", param->priorimgname);
    g_debug("TARGET='%s'", param->target);
    g_debug("FLUX=%g", param->flux);
    g_debug("=== OPTIONAL PARAMETERS (BSMEM)");
    g_debug("UV_MAX=%g", param->uv_max);
    g_debug("FLUXERR=%f", param->fluxerr);
    g_debug("INITFLUX=%f", param->initflux);
    // TODO: print entropy_func
    g_debug("V2A=%f", param->v2a);
    g_debug("T3AMPA=%f", param->t3ampa);
    g_debug("T3PHIA=%f", param->t3phia);
    g_debug("V2B=%f", param->v2b);
    g_debug("T3AMPB=%f", param->t3ampb);
    g_debug("T3PHIB=%f", param->t3phib);
    // TODO: print biserrtype
    g_debug("FAKET3A='%s'", fakeT3AmpName[param->fake_t3amp]);
  }
}

static void test_create(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = CreateBsmemParam();
  g_assert_true(param != NULL);
  debug_param(param, true);
  g_assert_true(param->filename[0] == '\0');
  g_assert_true(param->initimgname[0] == '\0');
  g_assert_true(param->priorimgname[0] == '\0');
  g_assert_true(param->target[0] == '\0');
  DestroyBsmemParam(param);
}

static void test_from_input(TestFixture *fix, gconstpointer userData)
{
  /* Get default parameters for comparison */
  BsmemParam *def = CreateBsmemParam();

  /* File has parameters HDU containing mandatory standard parameters */
  BsmemParam *std = BsmemParamFromFilename(INPUT_STD_FILENAME);
  debug_param(std, false);
  g_assert_cmpstr(std->filename, ==, INPUT_STD_FILENAME);
  g_assert_cmpint(strlen(std->initimgname), >, 0); /* INIT_IMG */
  g_assert_cmpfloat(std->wave_min, !=, def->wave_min);
  g_assert_cmpfloat(std->wave_max, !=, def->wave_max);
  g_assert_cmpint(std->maxiter, !=, def->maxiter);
  g_assert_true(std->use_vis2 != def->use_vis2);
  g_assert_cmpint(std->use_t3, !=, def->use_t3);
  g_assert_cmpint(std->maxiter, !=, def->maxiter);
  /* parameters not in file must have default values */
  g_assert_cmpstr(std->priorimgname, ==, def->priorimgname); /* RGL_PRIO */
  g_assert_cmpstr(std->target, ==, def->target);
  g_assert_cmpfloat(std->uv_max, ==, def->uv_max);
  g_assert_cmpfloat(std->alpha, ==, def->alpha);
  g_assert_cmpfloat(std->flux, ==, def->flux);
  g_assert_cmpfloat(std->fluxerr, ==, def->fluxerr);
  g_assert_cmpfloat(std->initflux, ==, def->initflux);
  g_assert_cmpint(std->regularization, ==, def->regularization); /* AUTO_WGT */
  g_assert_cmpfloat(std->v2a, ==, def->v2a);
  g_assert_cmpfloat(std->t3ampa, ==, def->t3ampa);
  g_assert_cmpfloat(std->t3phia, ==, def->t3phia);
  g_assert_cmpfloat(std->v2b, ==, def->v2b);
  g_assert_cmpfloat(std->t3ampb, ==, def->t3ampb);
  g_assert_cmpfloat(std->t3phib, ==, def->t3phib);
  g_assert_cmpint(std->fake_t3amp, ==, def->fake_t3amp);
  DestroyBsmemParam(std);

  /* File has parameters HDU containing all possible parameters */
  BsmemParam *all = BsmemParamFromFilename(INPUT_ALL_FILENAME);
  debug_param(all, true);
  g_assert_cmpstr(all->filename, ==, INPUT_ALL_FILENAME);
  g_assert_cmpint(strlen(all->initimgname), >, 0);
  g_assert_cmpstr(all->priorimgname, !=, def->priorimgname);
  g_assert_cmpstr(all->target, !=, def->target);
  g_assert_cmpfloat(all->wave_min, !=, def->wave_min);
  g_assert_cmpfloat(all->wave_max, !=, def->wave_max);
  g_assert_cmpfloat(all->uv_max, !=, def->uv_max);
  g_assert_true(all->use_vis2 != def->use_vis2);
  g_assert_cmpint(all->use_t3, !=, def->use_t3);
  g_assert_cmpint(all->maxiter, !=, def->maxiter);
  g_assert_cmpfloat(all->alpha, !=, def->alpha);
  g_assert_cmpfloat(all->flux, !=, def->flux);
  g_assert_cmpfloat(all->fluxerr, !=, def->fluxerr);
  g_assert_cmpfloat(all->initflux, !=, def->initflux);
  g_assert_cmpint(all->regularization, !=, def->regularization);
  g_assert_cmpfloat(all->v2a, !=, def->v2a);
  g_assert_cmpfloat(all->t3ampa, !=, def->t3ampa);
  g_assert_cmpfloat(all->t3phia, !=, def->t3phia);
  g_assert_cmpfloat(all->v2b, !=, def->v2b);
  g_assert_cmpfloat(all->t3ampb, !=, def->t3ampb);
  g_assert_cmpfloat(all->t3phib, !=, def->t3phib);
  g_assert_cmpint(all->fake_t3amp, !=, def->fake_t3amp);
  DestroyBsmemParam(all);

  DestroyBsmemParam(def);
}

static void test_filter(TestFixture *fix, gconstpointer userData)
{
  BsmemParam *param = BsmemParamFromFilename(INPUT_ALL_FILENAME);
  oi_filter_spec filt;
  BsmemParamSetFilter(param, &filt);
  g_assert_cmpint(filt.target_id, ==, param->target_id);
  g_assert_cmpfloat(filt.wave_range[0], ==, param->wave_min);
  g_assert_cmpfloat(filt.wave_range[1], ==, param->wave_max);
  g_assert_cmpfloat(filt.uvrad_range[0], ==, 0.);
  g_assert_cmpfloat(filt.uvrad_range[1], ==, param->uv_max);
  g_assert_cmpint(filt.accept_vis, ==, (param->use_vis == USE_ALL));
  g_assert_cmpint(filt.accept_vis2, ==, param->use_vis2);
  g_assert_cmpint(filt.accept_t3amp, ==,
                  (param->use_t3 == USE_ALL || param->use_t3 == USE_AMP));
  g_assert_cmpint(filt.accept_t3phi, ==,
                  (param->use_t3 == USE_ALL || param->use_t3 == USE_PHI));
  g_assert_false(filt.accept_flagged);
  DestroyBsmemParam(param);
}

static void test_append(TestFixture *fix, gconstpointer userData)
{
  fitsfile *fits;
  e4c_using_fits(fits, OUTPUT_FILENAME, FITS_CLOBBER)
  {
    int status = 0;
    fits_create_img(fits, BYTE_IMG, 0, NULL, &status);
    fits_write_key(fits, TSTRING, "TELESCOP", "COAST", NULL, &status);
    if (status)
      throwf(FitsException, "Error writing to primary HDU - %s",
             e4c_fits_error_report(status));
  }

  BsmemParam *param = CreateBsmemParam();
  /* Override the defaults for a proper test */
  g_strlcpy(param->target, "Alf Ori", FLEN_VALUE);
  param->wave_min = 1200E-9F;
  param->wave_max = 1370E-9F;
  param->use_vis = USE_ALL;
  param->use_vis2 = false;
  param->use_t3 = USE_NONE;
  param->uv_max = 2E9F;
  param->maxiter = 500;
  param->alpha = 5000.0F;
  param->flux = 0.5F;
  param->fluxerr = 1E-3F;
  param->initflux = 0.02F;
  param->regularization = 3;
  param->v2a = 1.5F;
  param->t3ampa = 1.2F;
  param->t3phia = 1.1F;
  param->v2b = 0.1F;
  param->t3ampb = 0.2F;
  param->t3phib = 2.0F;
  param->fake_t3amp = FAKE_NONE;
  BsmemParamAppendTo(param, OUTPUT_FILENAME);

  BsmemParam *inparam = BsmemParamFromFilename(OUTPUT_FILENAME);
  g_assert_cmpstr(inparam->filename, ==, OUTPUT_FILENAME);
  g_assert_cmpstr(inparam->initimgname, ==, param->initimgname);
  g_assert_cmpstr(inparam->priorimgname, ==, param->priorimgname);
  g_assert_cmpstr(inparam->target, ==, param->target);
  g_assert_cmpint(inparam->target_id, ==, param->target_id);
  g_assert_cmpfloat(inparam->wave_min, ==, param->wave_min);
  g_assert_cmpfloat(inparam->wave_max, ==, param->wave_max);
  g_assert_cmpint(inparam->use_vis, ==, param->use_vis);
  g_assert_true(inparam->use_vis2 == param->use_vis2);
  g_assert_cmpint(inparam->use_t3, ==, param->use_t3);
  g_assert_cmpfloat(inparam->uv_max, ==, param->uv_max);
  g_assert_cmpint(inparam->maxiter, ==, param->maxiter);
  g_assert_cmpfloat(inparam->alpha, ==, param->alpha);
  g_assert_cmpfloat(inparam->flux, ==, param->flux);
  g_assert_cmpfloat(inparam->fluxerr, ==, param->fluxerr);
  g_assert_cmpfloat(inparam->initflux, ==, param->initflux);
  g_assert_cmpint(inparam->regularization, ==, param->regularization);
  g_assert_cmpfloat(inparam->v2a, ==, param->v2a);
  g_assert_cmpfloat(inparam->t3ampa, ==, param->t3ampa);
  g_assert_cmpfloat(inparam->t3phia, ==, param->t3phia);
  g_assert_cmpfloat(inparam->v2b, ==, param->v2b);
  g_assert_cmpfloat(inparam->t3ampb, ==, param->t3ampb);
  g_assert_cmpfloat(inparam->t3phib, ==, param->t3phib);
  g_assert_cmpint(inparam->fake_t3amp, ==, param->fake_t3amp);

  DestroyBsmemParam(param);
  DestroyBsmemParam(inparam);
  delete_fits(OUTPUT_FILENAME);
}

int main(int argc, char *argv[])
{
  g_test_init(&argc, &argv, NULL);

  g_test_add("/bsmem/BsmemParam/create", TestFixture, NULL, setup_fixture,
             test_create, teardown_fixture);
  g_test_add("/bsmem/BsmemParam/from_input", TestFixture, NULL, setup_fixture,
             test_from_input, teardown_fixture);
  g_test_add("/bsmem/BsmemParam/filter", TestFixture, NULL, setup_fixture,
             test_filter, teardown_fixture);
  g_test_add("/bsmem/BsmemParam/append", TestFixture, NULL, setup_fixture,
             test_append, teardown_fixture);

  return g_test_run();
}
