/**
 * @file
 * Unit tests of BsmemRunner class.
 *
 * Copyright (C) 2015-2018, 2021, 2022 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "BsmemRunner.h"
#include "e4c_fitsio.h"

#include <glib.h>

#define DIR "OIFITS1/"
#define DATA_FILENAME DIR "Contest1_JHK.oifits"
#define INPUT_FILENAME "utest_GreyImg_in.fits"
#define OUTPUT_FILENAME "utest_BsmemRunner_out.fits"

typedef struct
{
  int dummy;

} TestFixture;

static void delete_fits(const char *filename)
{
  fitsfile *fits;
  int status = 0;

  fits_open_file(&fits, filename, READWRITE, &status);
  fits_delete_file(fits, &status);
  if (status)
    g_error("Failed to delete FITS file '%s' - CFITSIO error %d", filename,
            status);
}

static void uncaught_handler(const e4c_exception *exception)
{
  e4c_print_exception(exception);
  g_error("Uncaught exception");
}

static void setup_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_begin(E4C_FALSE);
  e4c_context_set_handlers(uncaught_handler, NULL, NULL, NULL);
}

static void teardown_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_end();
}

/** Test with complete input file. */
static void test_from_input(TestFixture *fix, gconstpointer userData)
{
  BsmemRunner *runner = BsmemRunnerFromInput(INPUT_FILENAME);
  // TODO: check all images have same dimensions and pixelsize
  BsmemRunnerPrintData(runner);
  BsmemRunnerPrintParam(runner);
  BsmemRunnerPrintState(runner);
  DestroyBsmemRunner(runner);
}

static float test_from_data1(float pixelsize, int dim, float uv_max)
{
  BsmemRunner *runner = NULL;
  BsmemParam *param = CreateBsmemParam();
  param->uv_max = uv_max;

  runner = BsmemRunnerFromArgs(DATA_FILENAME, param, pixelsize, dim, 3, 10.0F);
  float data_uv_max = runner->data->uv_max;
  g_assert_cmpfloat(data_uv_max, <=, uv_max);
  g_assert_cmpfloat(runner->initimg->pixelsize, <=,
                    1.001 / (2.0 * MAS * OVERSAMPLING * data_uv_max));
  g_assert_cmpint(runner->initimg->naxis1, ==, dim);
  g_assert_cmpint(runner->initimg->naxis2, ==, dim);
  BsmemRunnerPrintData(runner);
  BsmemRunnerPrintParam(runner);
  BsmemRunnerPrintState(runner);
  DestroyBsmemRunner(runner);

  if (pixelsize > 0.0)
  {
    GreyImg *initimg = CreateGreyImg(INIT_IMG_NAME, dim, dim, pixelsize);
    runner = BsmemRunnerFromImage(DATA_FILENAME, param, initimg);
    DestroyGreyImg(initimg);
    g_assert_cmpfloat(runner->data->uv_max, <=, uv_max);
    BsmemRunnerPrintData(runner);
    BsmemRunnerPrintParam(runner);
    BsmemRunnerPrintState(runner);
    DestroyBsmemRunner(runner);
  }

  DestroyBsmemParam(param);
  return data_uv_max;
}

/** Test with OIFITS data only. */
static void test_from_data(TestFixture *fix, gconstpointer userData)
{
  /* Automatic pixel size */
  float uv_max = test_from_data1(-1.0F, 128, 1E11);

  /* Automatic pixel size with uv taper */
  test_from_data1(-1.0F, 128, 0.712F * uv_max);

  /* User-defined pixel size */
  test_from_data1(0.15F, 256, 1E11);

  /* User-defined pixel size with uv taper */
  test_from_data1(0.125F, 256, 0.546F * uv_max);
}

static void test_iterate(TestFixture *fix, gconstpointer userData)
{
  BsmemRunner *runner = BsmemRunnerFromInput(INPUT_FILENAME);
  bool converged;
  int niter, niter_total = 0;
  do
  {
    converged = BsmemRunnerIterate(runner, 5, &niter);
    g_assert(converged || niter == 5);
    niter_total += niter;
  } while (!converged);
  g_assert_cmpint(runner->output->niter, ==, niter_total);
  g_assert_true(runner->output->converge);
  DestroyBsmemRunner(runner);
}

static void test_maxiter(TestFixture *fix, gconstpointer userData)
{
  BsmemRunner *runner = BsmemRunnerFromInput(INPUT_FILENAME);
  runner->inparam->maxiter = 12;
  bool converged, thrown = false;
  int niter;
  do
  {
    try
    {
      converged = BsmemRunnerIterate(runner, 5, &niter);
      g_assert(converged || niter == 5);
    }
    catch (RuntimeException)
    {
      g_message("Maximum iterations reached");
      thrown = true;
    }
  } while (!(converged || thrown));
  g_assert_cmpint(runner->output->niter, <=, runner->inparam->maxiter);
  g_assert_false(runner->output->converge);
  DestroyBsmemRunner(runner);
}

static void test_restart(TestFixture *fix, gconstpointer userData)
{
  BsmemRunner *runner = BsmemRunnerFromInput(INPUT_FILENAME);
  BsmemRunnerIterate(runner, 5, NULL);
  BsmemRunnerRestart(runner);
  g_assert_cmpint(runner->output->niter, ==, 0);
  g_assert_false(runner->output->converge);
  BsmemRunnerIterate(runner, -1, NULL);
  DestroyBsmemRunner(runner);
}

static void test_write_to(TestFixture *fix, gconstpointer userData)
{
  BsmemRunner *runner = BsmemRunnerFromInput(INPUT_FILENAME);
  try
  {
    BsmemRunnerWriteTo(runner, "!" OUTPUT_FILENAME, true);
    g_error("Failed to throw IllegalArgumentException");
  }
  catch (IllegalArgumentException)
  {
  }
  BsmemRunnerWriteTo(runner, OUTPUT_FILENAME, true);
  BsmemRunnerWriteTo(runner, OUTPUT_FILENAME, true);
  try
  {
    BsmemRunnerWriteTo(runner, OUTPUT_FILENAME, false);
    g_error("Failed to throw RuntimeException");
  }
  catch (RuntimeException)
  {
  }
  DestroyBsmemRunner(runner);

  BsmemRunner *runner2 = BsmemRunnerFromInput(OUTPUT_FILENAME);
  BsmemRunnerPrintData(runner2);
  BsmemRunnerPrintParam(runner2);
  BsmemRunnerPrintState(runner2);
  DestroyBsmemRunner(runner2);
  // TODO: compare inputs and/or results for 2 runs

  delete_fits(OUTPUT_FILENAME);
}

static void test_write_to_dir(TestFixture *fix, gconstpointer userData)
{
  BsmemRunner *runner = BsmemRunnerFromInput(INPUT_FILENAME);
  BsmemRunnerWriteTo(runner, DIR OUTPUT_FILENAME, true);
  BsmemRunnerWriteTo(runner, DIR OUTPUT_FILENAME, true);
  bool thrown = false;
  try
  {
    BsmemRunnerWriteTo(runner, "noexist/output.fits", false);
  }
  catch (RuntimeException)
  {
    thrown = true;
  }
  g_assert(thrown);
  DestroyBsmemRunner(runner);

  delete_fits(DIR OUTPUT_FILENAME);
}

int main(int argc, char *argv[])
{
  g_test_init(&argc, &argv, NULL);

  g_test_add("/bsmem/BsmemRunner/from_input", TestFixture, NULL, setup_fixture,
             test_from_input, teardown_fixture);
  g_test_add("/bsmem/BsmemRunner/from_data", TestFixture, NULL, setup_fixture,
             test_from_data, teardown_fixture);
  g_test_add("/bsmem/BsmemRunner/iterate", TestFixture, NULL, setup_fixture,
             test_iterate, teardown_fixture);
  g_test_add("/bsmem/BsmemRunner/maxiter", TestFixture, NULL, setup_fixture,
             test_maxiter, teardown_fixture);
  g_test_add("/bsmem/BsmemRunner/restart", TestFixture, NULL, setup_fixture,
             test_restart, teardown_fixture);
  g_test_add("/bsmem/BsmemRunner/write_to", TestFixture, NULL, setup_fixture,
             test_write_to, teardown_fixture);
  g_test_add("/bsmem/BsmemRunner/write_to_dir", TestFixture, NULL,
             setup_fixture, test_write_to_dir, teardown_fixture);

  return g_test_run();
}
