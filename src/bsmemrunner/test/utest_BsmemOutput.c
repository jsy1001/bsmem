/**
 * @file
 * Unit tests of BsmemOutput class.
 *
 * Copyright (C) 2015-2017, 2022 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "BsmemOutput.h"
#include "e4c_fitsio.h"

#include <glib.h>

#include <stdio.h>
#include <string.h>

#define OUTPUT_FILENAME "utest_BsmemOutput.fits"

typedef struct
{
  int dummy;

} TestFixture;

static void delete_fits(const char *filename)
{
  fitsfile *fits;
  int status = 0;

  fits_open_file(&fits, filename, READWRITE, &status);
  fits_delete_file(fits, &status);
  if (status)
    g_error("Failed to delete FITS file '%s' - CFITSIO error %d", filename,
            status);
}

static void uncaught_handler(const e4c_exception *exception)
{
  e4c_print_exception(exception);
  g_error("Uncaught exception");
}

static void setup_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_begin(E4C_FALSE);
  e4c_context_set_handlers(uncaught_handler, NULL, NULL, NULL);
}

static void teardown_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_end();
}

static void debug_output(const BsmemOutput *output)
{
  g_debug("LAST_IMG='%s'", output->last_img);
  g_debug("NITER=%d", output->niter);
  g_debug("CONVERGE=%c", output->converge ? 'T' : 'F');
  g_debug("CHISQ=%f", output->chisq);
  g_debug("ENTROPY=%f", output->entropy);
  g_debug("FLUX=%f", output->flux);
  g_debug("RGL_WGT=%f", output->alpha);
}

static void test_create(TestFixture *fix, gconstpointer userData)
{
  BsmemOutput *output = CreateBsmemOutput();
  g_assert(output != NULL);
  debug_output(output);
  g_assert_cmpint(output->niter, ==, 0);
  g_assert_false(output->converge);
  DestroyBsmemOutput(output);
}

static void test_append(TestFixture *fix, gconstpointer userData)
{
  fitsfile *fits;
  e4c_using_fits(fits, OUTPUT_FILENAME, FITS_CLOBBER)
  {
    int status = 0;
    fits_create_img(fits, BYTE_IMG, 0, NULL, &status);
    fits_write_key(fits, TSTRING, "TELESCOP", "COAST", NULL, &status);
    if (status)
      throwf(FitsException, "Error writing to primary HDU - %s",
             e4c_fits_error_report(status));
  }

  BsmemOutput *output = CreateBsmemOutput();
  /* Override the defaults for a proper test */
  g_strlcpy(output->last_img, "OUTPUT37", FLEN_VALUE);
  output->niter = 37;
  output->converge = true;
  output->chisq = 1.05431F;
  output->entropy = 143.683F;
  output->flux = 0.72413F;
  output->alpha = 1078.5314;
  BsmemOutputAppendTo(output, OUTPUT_FILENAME);
  DestroyBsmemOutput(output);
  delete_fits(OUTPUT_FILENAME);
}

int main(int argc, char *argv[])
{
  g_test_init(&argc, &argv, NULL);

  g_test_add("/bsmem/BsmemOutput/create", TestFixture, NULL, setup_fixture,
             test_create, teardown_fixture);
  g_test_add("/bsmem/BsmemOutput/append", TestFixture, NULL, setup_fixture,
             test_append, teardown_fixture);

  return g_test_run();
}
