/**
 * @file
 * Unit tests of GreyImg class.
 *
 * Copyright (C) 2015-2018, 2022 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "GreyImg.h"
#include "e4c_fitsio.h"

#include <glib.h>

#include <stdio.h>
#include <math.h> /* fabs() */

#define NAME "utest"
#define OUTPUT_FILENAME "utest_GreyImg_out.fits"
#define TOL 1E-5F

#define INPUT_FILENAME "utest_GreyImg_in.fits"
#define IMAGE_FILENAME "utest_GreyImg_in.fits"
#define INPUT_NAXIS 256
#define INPUT_PIXELSIZE 0.05F

#define assert_float_near(a, b, tol)                                           \
  do                                                                           \
  {                                                                            \
    if (fabs((a) - (b)) >= (tol))                                              \
    {                                                                          \
      fprintf(stderr,                                                          \
              "ERROR:%s:%d:%s: "                                               \
              "%f differs significantly from %f\n",                            \
              __FILE__, __LINE__, __func__, (a), (b));                         \
      abort();                                                                 \
    }                                                                          \
  } while (0)

typedef struct
{
  int dummy;

} TestFixture;

static void delete_fits(const char *filename)
{
  fitsfile *fits;
  int status = 0;

  fits_open_file(&fits, filename, READWRITE, &status);
  fits_delete_file(fits, &status);
  if (status)
    g_error("Failed to delete FITS file '%s' - CFITSIO error %d", filename,
            status);
}

static void uncaught_handler(const e4c_exception *exception)
{
  e4c_print_exception(exception);
  g_error("Uncaught exception");
}

static void setup_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_begin(E4C_FALSE);
  e4c_context_set_handlers(uncaught_handler, NULL, NULL, NULL);
}

static void teardown_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_end();
}

static void test_create(TestFixture *fix, gconstpointer userData)
{
  GreyImg *img;

  img = CreateGreyImg(NAME, 64, 64, 0.5F);
  g_assert_cmpstr(img->name, ==, NAME);
  g_assert_cmpint(img->naxis1, ==, 64);
  g_assert_cmpint(img->naxis2, ==, 64);
  assert_float_near(img->pixelsize, 0.5F, TOL);
  g_assert(GreyImgIsSquare(img));
  DestroyGreyImg(img);

  img = CreateGreyImg(NAME, 128, 32, 0.25F);
  g_assert_cmpstr(img->name, ==, NAME);
  g_assert_cmpint(img->naxis1, ==, 128);
  g_assert_cmpint(img->naxis2, ==, 32);
  assert_float_near(img->pixelsize, 0.25F, TOL);
  g_assert(!GreyImgIsSquare(img));
  DestroyGreyImg(img);
}

static void test_from_input(TestFixture *fix, gconstpointer userData)
{
  GreyImg *img = GreyImgFromInputFilename(INPUT_FILENAME, "INIT_IMG");
  g_assert_cmpstr(img->name, ==, "IMAGE-OI INITIAL IMAGE");
  g_assert_cmpint(img->naxis1, ==, INPUT_NAXIS);
  g_assert_cmpint(img->naxis2, ==, INPUT_NAXIS);
  assert_float_near(img->pixelsize, INPUT_PIXELSIZE, TOL);
  g_assert(GreyImgIsSquare(img));
  DestroyGreyImg(img);
}

static void test_from_image(TestFixture *fix, gconstpointer userData)
{
  GreyImg *img = GreyImgFromImageFilename(IMAGE_FILENAME);
  g_assert_cmpstr(img->name, ==, "IMAGE-OI INITIAL IMAGE");
  g_assert_cmpint(img->naxis1, ==, INPUT_NAXIS);
  g_assert_cmpint(img->naxis2, ==, INPUT_NAXIS);
  assert_float_near(img->pixelsize, INPUT_PIXELSIZE, TOL);
  g_assert(GreyImgIsSquare(img));
  DestroyGreyImg(img);

  GreyImg *img2 = GreyImgFromImageFilenameNoWCS(IMAGE_FILENAME, 0.10F);
  g_assert_cmpstr(img2->name, ==, "IMAGE-OI INITIAL IMAGE");
  g_assert_cmpint(img2->naxis1, ==, INPUT_NAXIS);
  g_assert_cmpint(img2->naxis2, ==, INPUT_NAXIS);
  assert_float_near(img2->pixelsize, 0.10F, TOL);
  g_assert(GreyImgIsSquare(img2));
  DestroyGreyImg(img2);

  try
  {
    GreyImg *img3 = GreyImgFromImageFilenameNoWCS(IMAGE_FILENAME, -1.0F);
    g_error("Failed to throw IllegalArgumentException");
  }
  catch (IllegalArgumentException)
  {
    g_debug("Caught IllegalArgumentException");
  }
}

static void test_normalise(TestFixture *fix, gconstpointer userData)
{
  GreyImg *img = CreateGreyImg(NAME, 64, 64, 0.5F);
  for (int j = 0; j < img->naxis2; j++)
    for (int i = 0; i < img->naxis1; i++)
      img->image[j * img->naxis1 + i] = 3.38F;

  GreyImgNormalise(img);
  assert_float_near(GreyImgGetFlux(img), 1.0F, TOL);
  DestroyGreyImg(img);
}

static void test_blank(TestFixture *fix, gconstpointer userData)
{
  GreyImg *img = CreateGreyImg(NAME, 64, 64, 0.5F);
  for (int j = 0; j < img->naxis2; j++)
    for (int i = 0; i < img->naxis1; i++)
      img->image[j * img->naxis1 + i] = 1.24F;

  g_assert(GreyImgSetBlank(img, 1.241F, 1.0F));
  assert_float_near(GreyImgGetFlux(img), 64 * 64 * 1.0F, TOL);
  DestroyGreyImg(img);
}

static void test_write(TestFixture *fix, gconstpointer userData)
{
  GreyImg *img = CreateGreyImg(NAME, 64, 64, 0.5F);
  for (int j = 0; j < img->naxis2; j++)
    for (int i = 0; i < img->naxis1; i++)
      img->image[j * img->naxis1 + i] = 0.001F * (j * img->naxis1 + i);
  GreyImgWritePrimaryTo(img, OUTPUT_FILENAME, true);

  fitsfile *fits;
  e4c_using_fits(fits, OUTPUT_FILENAME, FITS_OPEN_READONLY)
  {
    int status = 0;
    char hduname[FLEN_VALUE];
    long naxes[2], fpixel[2] = {1L, 1L};
    float cdelt;
    float *data;

    fits_movabs_hdu(fits, 1, NULL, &status);

    /* Check header */
    fits_read_key(fits, TSTRING, "HDUNAME", &hduname, NULL, &status);
    fits_get_img_size(fits, 2, naxes, &status);
    fits_read_key(fits, TFLOAT, "CDELT1", &cdelt, NULL, &status);
    if (status)
      throwf(FitsException, "Error reading image header - %s",
             e4c_fits_error_report(status));
    g_assert_cmpstr(hduname, ==, img->name);
    g_assert_cmpint(naxes[0], ==, img->naxis1);
    g_assert_cmpint(naxes[1], ==, img->naxis2);
    assert_float_near(cdelt, img->pixelsize * MAS_TO_DEG, TOL);

    /* Check data */
    data = g_malloc(naxes[0] * naxes[1] * sizeof(data[0]));
    fits_read_pix(fits, TFLOAT, fpixel, naxes[0] * naxes[1], 0, data, 0,
                  &status);
    if (status)
      throwf(FitsException, "Error reading image data - %s",
             e4c_fits_error_report(status));
    for (int j = 0; j < naxes[1]; j++)
      for (int i = 0; i < naxes[0]; i++)
        assert_float_near(data[j * naxes[0] + i], img->image[j * naxes[0] + i],
                          TOL);
    g_free(data);
  }

  DestroyGreyImg(img);
  delete_fits(OUTPUT_FILENAME);
}

static void test_append(TestFixture *fix, gconstpointer userData)
{
  GreyImg *img = CreateGreyImg(NAME, 64, 64, 0.5F);
  for (int j = 0; j < img->naxis2; j++)
    for (int i = 0; i < img->naxis1; i++)
      img->image[j * img->naxis1 + i] = 0.001F * (j * img->naxis1 + i);
  GreyImgWritePrimaryTo(img, OUTPUT_FILENAME, true);
  GreyImgAppendImageTo(img, OUTPUT_FILENAME);

  fitsfile *fits;
  e4c_using_fits(fits, OUTPUT_FILENAME, FITS_OPEN_READONLY)
  {
    int status = 0;
    char hduname[FLEN_VALUE], extname[FLEN_VALUE];
    long naxes[2], fpixel[2] = {1L, 1L};
    float cdelt;
    float *data;

    fits_movabs_hdu(fits, 2, NULL, &status);

    /* Check header */
    fits_read_key(fits, TSTRING, "HDUNAME", &hduname, NULL, &status);
    fits_read_key(fits, TSTRING, "EXTNAME", &extname, NULL, &status);
    fits_get_img_size(fits, 2, naxes, &status);
    fits_read_key(fits, TFLOAT, "CDELT1", &cdelt, NULL, &status);
    if (status)
      throwf(FitsException, "Error reading image header - %s",
             e4c_fits_error_report(status));
    g_assert_cmpstr(hduname, ==, img->name);
    g_assert_cmpstr(extname, ==, img->name);
    g_assert_cmpint(naxes[0], ==, img->naxis1);
    g_assert_cmpint(naxes[1], ==, img->naxis2);
    assert_float_near(cdelt, img->pixelsize * MAS_TO_DEG, TOL);

    /* Check data */
    data = g_malloc(naxes[0] * naxes[1] * sizeof(data[0]));
    fits_read_pix(fits, TFLOAT, fpixel, naxes[0] * naxes[1], 0, data, 0,
                  &status);
    if (status)
      throwf(FitsException, "Error reading image data - %s",
             e4c_fits_error_report(status));
    for (int j = 0; j < naxes[1]; j++)
      for (int i = 0; i < naxes[0]; i++)
        assert_float_near(data[j * naxes[0] + i], img->image[j * naxes[0] + i],
                          TOL);
    g_free(data);
  }

  DestroyGreyImg(img);
  delete_fits(OUTPUT_FILENAME);
}

int main(int argc, char *argv[])
{
  g_test_init(&argc, &argv, NULL);

  g_test_add("/bsmem/GreyImg/create", TestFixture, NULL, setup_fixture,
             test_create, teardown_fixture);
  g_test_add("/bsmem/GreyImg/from_input", TestFixture, NULL, setup_fixture,
             test_from_input, teardown_fixture);
  g_test_add("/bsmem/GreyImg/from_image", TestFixture, NULL, setup_fixture,
             test_from_image, teardown_fixture);
  g_test_add("/bsmem/GreyImg/normalise", TestFixture, NULL, setup_fixture,
             test_normalise, teardown_fixture);
  g_test_add("/bsmem/GreyImg/blank", TestFixture, NULL, setup_fixture,
             test_blank, teardown_fixture);
  g_test_add("/bsmem/GreyImg/write", TestFixture, NULL, setup_fixture,
             test_write, teardown_fixture);
  g_test_add("/bsmem/GreyImg/append", TestFixture, NULL, setup_fixture,
             test_append, teardown_fixture);

  return g_test_run();
}
