/**
 * @file
 * Definition of BsmemRunner class.
 *
 * Copyright (C) 2015-2017 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#ifndef BSMEM_RUNNER_H
#define BSMEM_RUNNER_H

#include "BsmemParam.h"
#include "BsmemData.h"
#include "BsmemOutput.h"
#include "GreyImg.h"

#include <nfft3.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <complex.h>
#include <stdbool.h>

#define INIT_IMG_NAME "IMAGE-OI INITIAL IMAGE"
#define PRIOR_IMG_NAME "IMAGE-OI PRIOR IMAGE"
/* Note INPUT_PARAM_NAME is defined in GreyImg.h */
#define OUTPUT_PARAM_NAME "IMAGE-OI OUTPUT PARAM"

#define OVERSAMPLING 3.0 /* compared to Shannon. 6 pix/fringe -> 3.0 */
#define MAS ((M_PI / 180.0) / 3600000.0)
#define MOD_BLANK 1.0E-8

typedef struct
{
  int iNpow;
  int iNbis;
  int iNUV;
  int iNX;
  int iNY;
  float xyint;
  /* Work environments */
  float complex *current_visi;
  float complex *current_diffvisi;
  float complex *data_phasor;
  float imflux;
  const BsmemData *data; // TODO: or copy data->bis[i].ab, .bc, .ca
  /* Work environment for transform/gridding routines */
  float *UV_point;
  nfft_plan p;

} USER;

/**
 * @class BsmemRunner
 * BSMEM algorithm class.
 */
typedef struct
{
  /** @publicsection */
  BsmemParam *inparam;
  BsmemData *data;
  BsmemOutput *output;

  GreyImg *initimg;
  GreyImg *priorimg;
  GreyImg *currentimg;

  bool verbose;

  /** @privatesection */
  float *st;
  int kb[40];
  USER user;
  int ndata;
  int total_iter;

} BsmemRunner;

/** @cond DOCUMENT_METHODS_AS_FUNCTIONS */
BsmemRunner *BsmemRunnerFromInput(const char *);
BsmemRunner *BsmemRunnerFromImage(const char *, const BsmemParam *,
                                  const GreyImg *);
BsmemRunner *BsmemRunnerFromArgs(const char *, const BsmemParam *, float, int,
                                 int, float);
// TODO: other options: plotting on/off, UI on/off, verbosity
void BsmemRunnerPrintData(const BsmemRunner *);
void BsmemRunnerPrintParam(const BsmemRunner *);
void BsmemRunnerPrintState(const BsmemRunner *);
void BsmemRunnerRestart(BsmemRunner *);
// void BsmemRunnerSetModel(BsmemRunner *, ...);
bool BsmemRunnerIterate(BsmemRunner *, int, int *const);
void BsmemRunnerCentreImage(BsmemRunner *);

// static void BsmemRunnerWriteData(const BsmemRunner *, const char *, bool);
void BsmemRunnerWriteTo(const BsmemRunner *, const char *, bool);
void DestroyBsmemRunner(BsmemRunner *);
/** @endcond */

#endif /* #ifndef BSMEM_RUNNER_H */
