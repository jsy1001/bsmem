/**
 * @file
 * Definition of BsmemOutput class.
 *
 * Copyright (C) 2015-2017, 2022 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#ifndef BSMEM_OUTPUT_H
#define BSMEM_OUTPUT_H

#include <fitsio.h>

#include <stdbool.h>

/**
 * @class BsmemOutput
 * BSMEM scalar outputs class.
 */
typedef struct
{
  /** @publicsection */
  char last_img[FLEN_VALUE];
  int niter;
  bool converge;
  float chisq;
  float entropy;
  float flux;
  float alpha;

} BsmemOutput;

/** @cond DOCUMENT_METHODS_AS_FUNCTIONS */
BsmemOutput *CreateBsmemOutput(void);
void BsmemOutputAppendTo(BsmemOutput *, const char *);
void DestroyBsmemOutput(BsmemOutput *);
/** @endcond */

#endif /* #ifndef BSMEM_OUTPUT_H */
