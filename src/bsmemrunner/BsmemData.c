/**
 * @file
 * Implementation of BsmemData class.
 *
 * Copyright (C) 2015-2018, 2021-2023 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "BsmemData.h"
#include "BsmemParam.h"
#include "e4c_fitsio.h"

#include <exchange.h>
#include <oifile.h>
#include <oiiter.h>

#include <string.h>
#include <limits.h>

/** Typedef to specify pointer to a function that frees its argument. */
typedef void (*free_func)(gpointer);

/** Data for OI_VIS2 FITS table with additional column for model values. */
typedef struct
{
  char date_obs[FLEN_VALUE];
  char arrname[FLEN_VALUE]; /**< empty string "" means not specified */
  char insname[FLEN_VALUE];
  char corrname[FLEN_VALUE]; /**< empty string "" means not specified */
  long numrec;
  int nwave;
  int ipow0;
  int npow;

} oi_vis2_model;

/** Data for OI_T3 FITS table with additional columns for model values. */
typedef struct
{
  char date_obs[FLEN_VALUE];
  char arrname[FLEN_VALUE]; /**< empty string "" means not specified */
  char insname[FLEN_VALUE];
  char corrname[FLEN_VALUE]; /**< empty string "" means not specified */
  long numrec;
  int nwave;
  int ibis0;
  int nbis;

} oi_t3_model;

static BsmemUVRef BsmemDataAddUV(BsmemData *, BsmemUV);

static float square(float x)
{
  return x * x;
}

static BsmemPow get_flux_pow(const BsmemParam *param, int iuv)
{
  BsmemPow flux;
  flux.pow = square(param->flux);
  flux.powerr = param->fluxerr;
  flux.powmodel = flux.pow;
  flux.ab.iuv = iuv;
  flux.ab.sign = 1;
  flux.target_id = -1;
  flux.mjd = 0.0;
  flux.int_time = 0.0;
  flux.eff_wave = 1.0;
  flux.sta_index[0] = -1;
  flux.sta_index[1] = -1;
  flux.corrindx_vis2data = -1;
  flux.extver = -1;
  flux.irec = -1;
  flux.iwave = -1;
  return flux;
}

/**
 * Conjugate uv point into positive u half-plane.
 */
static BsmemUV uvconj(BsmemUV uv, short *const pSign)
{
  if (uv.u < 0.0)
  {
    uv.u = -uv.u;
    uv.v = -uv.v;
    if (pSign != NULL) *pSign = -1;
  }
  else
  {
    if (pSign != NULL) *pSign = 1;
  }
  return uv;
}

/**
 * Compare uv points @a ua and @a ub.
 *
 * @return int  -1 if ua == -ub within @a threshold;
 *              +1 if ua ==  ub within @a threshold; or
 *               0 (i.e. false) otherwise.
 */
static int uvcmp(BsmemUV ua, BsmemUV ub, float threshold)
{
  float pcompu, pcompv;
  float mcompu, mcompv;
  float sqthresh = threshold * threshold;
  pcompu = 2.0 * (ua.u - ub.u) * (ua.u - ub.u) / (ua.u * ua.u + ub.u * ub.u);
  pcompv = 2.0 * (ua.v - ub.v) * (ua.v - ub.v) / (ua.v * ua.v + ub.v * ub.v);
  mcompu = 2.0 * (ua.u + ub.u) * (ua.u + ub.u) / (ua.u * ua.u + ub.u * ub.u);
  mcompv = 2.0 * (ua.v + ub.v) * (ua.v + ub.v) / (ua.v * ua.v + ub.v * ub.v);

  /* Handle zeros */
  if (ua.u - ub.u == 0.0) pcompu = 0.0;
  if (ua.v - ub.v == 0.0) pcompv = 0.0;
  if (ua.u + ub.u == 0.0) mcompu = 0.0;
  if (ua.v + ub.v == 0.0) mcompv = 0.0;

  /* If ua is same as ub then return 1 */
  if (pcompu < sqthresh && pcompv < sqthresh) return 1;

  /* If ua is same as ub but conjugated then return -1 */
  if (mcompu < sqthresh && mcompv < sqthresh) return -1;

  return 0;
}

static void init_oi_fits_model(oi_fits_model *pOi)
{
  pOi->object.target_id = -1;
  pOi->arrayList = NULL;
  pOi->wavelengthList = NULL;
  pOi->corrList = NULL;
  pOi->vis2List = NULL;
  pOi->t3List = NULL;
  pOi->arrayHash = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, NULL);
  pOi->wavelengthHash =
      g_hash_table_new_full(g_str_hash, g_str_equal, NULL, NULL);
  pOi->corrHash = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, NULL);
}

/** Free linked list and contents. */
static void free_list(GList *list, free_func internalFree)
{
  GList *link;

  link = list;
  while (link != NULL)
  {
    if (internalFree) (*internalFree)(link->data);
    free(link->data);
    link = link->next;
  }
  g_list_free(list);
}

static void free_oi_fits_model(oi_fits_model *pOi)
{
  g_hash_table_destroy(pOi->arrayHash);
  g_hash_table_destroy(pOi->wavelengthHash);
  g_hash_table_destroy(pOi->corrHash);
  free_list(pOi->arrayList, (free_func)free_oi_array);
  free_list(pOi->wavelengthList, (free_func)free_oi_wavelength);
  free_list(pOi->corrList, (free_func)free_oi_corr);
  g_list_free_full(pOi->vis2List, g_free);
  g_list_free_full(pOi->t3List, g_free);
}

/**
 * Constructor.
 * @memberof BsmemData
 */
BsmemData *CreateBsmemData(const BsmemParam *param)
{
  if (param == NULL) throw(NullPointerException, "Argument 'param' is NULL.");

  BsmemData *this = g_malloc(sizeof(*this));
  this->nuv = 1;
  this->npow = 1;
  this->nbis = 0;
  this->uv = g_malloc(this->nuv * sizeof(this->uv[0]));
  this->pow = g_malloc(this->npow * sizeof(this->pow[0]));
  this->bis = NULL;
  this->uv_max = 0.0;
  init_oi_fits(&this->origdata);
  this->uv[0].u = 0.0;
  this->uv[0].v = 0.0;
  this->pow[0] = get_flux_pow(param, 0);
  init_oi_fits_model(&this->outdata);
  return this;
}

/**
 * Count data that pass filter.
 */
static void countdata(const oi_fits *pData, const oi_filter_spec *pFilter,
                      int *const pNpow, int *const pNbis)
{
  oi_vis2_iter vis2Iter;
  oi_vis2_iter_init(&vis2Iter, pData, pFilter);
  int npow = 0;
  while (oi_vis2_iter_next(&vis2Iter, NULL, NULL, NULL, NULL, NULL))
  {
    npow++;
    g_assert_cmpint(npow, <, INT_MAX);
  }

  oi_t3_iter t3Iter;
  oi_t3_iter_init(&t3Iter, pData, pFilter);
  int nbis = 0;
  while (oi_t3_iter_next(&t3Iter, NULL, NULL, NULL, NULL, NULL))
  {
    nbis++;
    g_assert_cmpint(nbis, <, INT_MAX);
  }

  if (pNpow != NULL) *pNpow = npow;
  if (pNbis != NULL) *pNbis = nbis;
}

/** Return maximum uv radius in selected data. */
static float uvmax(const oi_fits *pData, const oi_filter_spec *pFilter)
{
  float uvrad, uvmax = 0.;

  oi_vis2_iter vis2Iter;
  oi_vis2_iter_init(&vis2Iter, pData, pFilter);
  while (oi_vis2_iter_next(&vis2Iter, NULL, NULL, NULL, NULL, NULL))
  {
    double u, v;
    oi_vis2_iter_get_uv(&vis2Iter, NULL, &u, &v);
    uvrad = sqrt(u * u + v * v);
    if (uvrad > uvmax) uvmax = uvrad;
  }

  oi_t3_iter t3Iter;
  oi_t3_iter_init(&t3Iter, pData, pFilter);
  while (oi_t3_iter_next(&t3Iter, NULL, NULL, NULL, NULL, NULL))
  {
    double u1, v1, u2, v2;
    oi_t3_iter_get_uv(&t3Iter, NULL, &u1, &v1, &u2, &v2);
    uvrad = sqrt(u1 * u1 + v1 * v1);
    if (uvrad > uvmax) uvmax = uvrad;
    uvrad = sqrt(u2 * u2 + v2 * v2);
    if (uvrad > uvmax) uvmax = uvrad;
    uvrad = sqrt((u1 + u2) * (u1 + u2) + (v1 + v2) * (v1 + v2));
    if (uvrad > uvmax) uvmax = uvrad;
  }
  return uvmax;
}

static void BsmemDataSetTarget(BsmemData *this, int targetId)
{
  if (this->outdata.object.target_id == targetId) return;

  target *pInTarget = oi_fits_lookup_target(&this->origdata, targetId);
  if (pInTarget == NULL)
    throwf(RuntimeException, "Input data OI_TARGET table missing TARGET_ID %d",
           targetId);
  memcpy(&this->outdata.object, pInTarget, sizeof(*pInTarget));
}

static void BsmemDataAddOiArray(BsmemData *this, const char *arrname)
{
  if (arrname[0] != '\0')
  {
    oi_array *pInArray = oi_fits_lookup_array(&this->origdata, arrname);
    if (pInArray == NULL)
      throwf(RuntimeException,
             "Input data missing OI_ARRAY table with ARRNAME='%s'", arrname);
    if (!g_hash_table_lookup(this->outdata.arrayHash, arrname))
    {
      oi_array *pOutArray = dup_oi_array(pInArray);
      this->outdata.arrayList =
          g_list_append(this->outdata.arrayList, pOutArray);
      g_hash_table_insert(this->outdata.arrayHash, pOutArray->arrname,
                          pOutArray);
    }
  }
}

static void BsmemDataAddOiWavelength(BsmemData *this, const char *insname)
{
  g_assert(insname[0] != '\0');

  oi_wavelength *pInWave = oi_fits_lookup_wavelength(&this->origdata, insname);
  if (pInWave == NULL)
    throwf(RuntimeException,
           "Input data missing OI_WAVELENGTH table with INSNAME='%s'", insname);
  if (!g_hash_table_lookup(this->outdata.wavelengthHash, insname))
  {
    oi_wavelength *pOutWave = dup_oi_wavelength(pInWave);
    this->outdata.wavelengthList =
        g_list_append(this->outdata.wavelengthList, pOutWave);
    g_hash_table_insert(this->outdata.wavelengthHash, pOutWave->insname,
                        pOutWave);
  }
}

static void BsmemDataAddOiCorr(BsmemData *this, const char *corrname)
{
  if (corrname[0] != '\0')
  {
    oi_corr *pInCorr = oi_fits_lookup_corr(&this->origdata, corrname);
    if (pInCorr == NULL)
      throwf(RuntimeException,
             "Input data missing OI_CORR table with CORRNAME='%s'", corrname);
    if (!g_hash_table_lookup(this->outdata.corrHash, corrname))
    {
      oi_corr *pOutCorr = dup_oi_corr(pInCorr);
      this->outdata.corrList = g_list_append(this->outdata.corrList, pOutCorr);
      g_hash_table_insert(this->outdata.corrHash, pOutCorr->corrname, pOutCorr);
    }
  }
}

static oi_vis2_model *BsmemDataAddOiVis2(BsmemData *this, const oi_vis2 *pInTab,
                                         int ipow0)
{
  oi_vis2_model *pVis2 = g_malloc(sizeof(*pVis2));
  g_strlcpy(pVis2->date_obs, pInTab->date_obs, FLEN_VALUE);
  g_strlcpy(pVis2->arrname, pInTab->arrname, FLEN_VALUE);
  g_strlcpy(pVis2->insname, pInTab->insname, FLEN_VALUE);
  g_strlcpy(pVis2->corrname, pInTab->corrname, FLEN_VALUE);
  pVis2->numrec = pInTab->numrec;
  pVis2->nwave = pInTab->nwave;
  pVis2->ipow0 = ipow0;
  pVis2->npow = 0;
  this->outdata.vis2List = g_list_append(this->outdata.vis2List, pVis2);

  BsmemDataAddOiArray(this, pInTab->arrname);
  BsmemDataAddOiWavelength(this, pInTab->insname);
  BsmemDataAddOiCorr(this, pInTab->corrname);
  return pVis2;
}

static void BsmemDataSetPow(BsmemData *this, const oi_fits *pData,
                            const oi_filter_spec *pFilter)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (pData == NULL) throw(NullPointerException, "Argument 'pData' is NULL.");
  if (pFilter == NULL)
    throw(NullPointerException, "Argument 'pFilter' is NULL.");
  if (pData->targets.ntarget > 0 && pFilter->target_id < 0)
    throw(IllegalArgumentException,
          "There are multiple targets but no target is selected.");

  oi_vis2_iter iter;
  oi_vis2 *pInTab;
  oi_vis2_record *pRec;
  int extver, iwave, i = 1, numOiVis2 = 0;
  long irec;
  oi_vis2_iter_init(&iter, pData, pFilter);
  while (oi_vis2_iter_next(&iter, &extver, &pInTab, &irec, &pRec, &iwave))
  {
    double effWave, u, v;
    BsmemUV ab;
    oi_vis2_model *pOutTab;
    if (extver > numOiVis2)
    {
      pOutTab = BsmemDataAddOiVis2(this, pInTab, i);
      numOiVis2++;
    }
    if (this->outdata.object.target_id != pRec->target_id)
      BsmemDataSetTarget(this, pRec->target_id);
    oi_vis2_iter_get_uv(&iter, &effWave, &u, &v);
    ab.u = (float)u;
    ab.v = (float)v;
    g_assert_cmpint(i, <, this->npow);
    this->pow[i].ab.iuv = this->nuv;
    this->uv[this->nuv++] = uvconj(ab, &this->pow[i].ab.sign);
    this->pow[i].pow = (float)pRec->vis2data[iwave];
    this->pow[i].powerr = (float)pRec->vis2err[iwave];
    g_assert(!pRec->flag[iwave]);
    this->pow[i].powmodel = this->pow[i].pow;
    this->pow[i].target_id = pRec->target_id;
    this->pow[i].mjd = pRec->mjd;
    this->pow[i].int_time = pRec->int_time;
    this->pow[i].eff_wave = effWave;
    this->pow[i].sta_index[0] = pRec->sta_index[0];
    this->pow[i].sta_index[1] = pRec->sta_index[1];
    this->pow[i].corrindx_vis2data = pRec->corrindx_vis2data + iwave;
    this->pow[i].extver = extver;
    this->pow[i].irec = irec;
    this->pow[i].iwave = iwave;
    pOutTab->npow++;
    i++;
  }
}

static void extrapolate_bisamp(BsmemPow pow1, BsmemPow pow2, BsmemPow pow3,
                               BsmemBis *const pBis)
{
  float sqamp1, sqamp2, sqamp3;
  float sqamperr1, sqamperr2, sqamperr3;

  sqamp1 = (pow1.pow + sqrt(square(pow1.pow) + 2.0 * square(pow1.powerr))) / 2.;
  sqamperr1 =
      1. / (1. / sqamp1 + 2. * (3. * sqamp1 - pow1.pow) / square(pow1.powerr));
  sqamp2 = (pow2.pow + sqrt(square(pow2.pow) + 2.0 * square(pow2.powerr))) / 2.;
  sqamperr2 =
      1. / (1. / sqamp2 + 2. * (3. * sqamp2 - pow2.pow) / square(pow2.powerr));
  sqamp3 = (pow3.pow + sqrt(square(pow3.pow) + 2.0 * square(pow3.powerr))) / 2.;
  sqamperr3 =
      1. / (1. / sqamp3 + 2. * (3. * sqamp3 - pow3.pow) / square(pow3.powerr));

  pBis->bisamp = sqrt(sqamp1 * sqamp2 * sqamp3);
  pBis->bisamperr =
      (fabs(pBis->bisamp) *
       sqrt(sqamperr1 / sqamp1 + sqamperr2 / sqamp2 + sqamperr3 / sqamp3));
}

static oi_t3_model *BsmemDataAddOiT3(BsmemData *this, const oi_t3 *pInTab,
                                     int ibis0)
{
  oi_t3_model *pT3 = g_malloc(sizeof(*pT3));
  g_strlcpy(pT3->date_obs, pInTab->date_obs, FLEN_VALUE);
  g_strlcpy(pT3->arrname, pInTab->arrname, FLEN_VALUE);
  g_strlcpy(pT3->insname, pInTab->insname, FLEN_VALUE);
  g_strlcpy(pT3->corrname, pInTab->corrname, FLEN_VALUE);
  pT3->numrec = pInTab->numrec;
  pT3->nwave = pInTab->nwave;
  pT3->ibis0 = ibis0;
  pT3->nbis = 0;
  this->outdata.t3List = g_list_append(this->outdata.t3List, pT3);

  BsmemDataAddOiArray(this, pInTab->arrname);
  BsmemDataAddOiWavelength(this, pInTab->insname);
  BsmemDataAddOiCorr(this, pInTab->corrname);
  return pT3;
}

static void BsmemDataSetBis(BsmemData *this, const oi_fits *pData,
                            const oi_filter_spec *pFilter, FakeT3Amp fake_t3amp)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (pData == NULL) throw(NullPointerException, "Argument 'pData' is NULL.");
  if (pFilter == NULL)
    throw(NullPointerException, "Argument 'pFilter' is NULL.");
  if (pData->targets.ntarget > 0 && pFilter->target_id < 0)
    throw(IllegalArgumentException,
          "There are multiple targets but no target is selected.");

  oi_t3_iter iter;
  oi_t3 *pInTab;
  oi_t3_record *pRec;
  int extver, iwave, i = 0, numOiT3 = 0;
  long irec;
  oi_t3_iter_init(&iter, pData, pFilter);
  while (oi_t3_iter_next(&iter, &extver, &pInTab, &irec, &pRec, &iwave))
  {
    double effWave, u1, v1, u2, v2;
    BsmemUV ab, bc, ca;
    oi_t3_model *pOutTab;
    if (extver > numOiT3)
    {
      pOutTab = BsmemDataAddOiT3(this, pInTab, i);
      numOiT3++;
    }
    if (this->outdata.object.target_id != pRec->target_id)
      BsmemDataSetTarget(this, pRec->target_id);
    oi_t3_iter_get_uv(&iter, &effWave, &u1, &v1, &u2, &v2);
    ab.u = (float)u1;
    ab.v = (float)v1;
    bc.u = (float)u2;
    bc.v = (float)v2;
    ca.u = (float)(-u1 - u2);
    ca.v = (float)(-v1 - v2);
    g_debug("AB = (%.2f, %.2f), BC = (%.2f, %.2f), CA = (%.2f, %.2f)", ab.u,
            ab.v, bc.u, bc.v, ca.u, ca.v);
    g_assert_cmpint(i, <, this->nbis);
    this->bis[i].ab = BsmemDataAddUV(this, ab);
    this->bis[i].bc = BsmemDataAddUV(this, bc);
    this->bis[i].ca = BsmemDataAddUV(this, ca);

    if (fake_t3amp == FAKE_ALL ||
        (fake_t3amp == FAKE_MISSING &&
         (!pFilter->accept_t3amp || isnan(pRec->t3amp[iwave]))))
    {
      this->bis[i].fakeamp = true;
      if (this->bis[i].ab.iuv < this->npow &&
          this->bis[i].bc.iuv < this->npow && this->bis[i].ca.iuv < this->npow)
      {
        /* Corresponding powerspectrum points are available, so derive
         * pseudo triple amplitudes from powerspectrum data */
        extrapolate_bisamp(this->pow[this->bis[i].ab.iuv],
                           this->pow[this->bis[i].bc.iuv],
                           this->pow[this->bis[i].ca.iuv], &this->bis[i]);
      }
      else
      {
        g_warning("Extrapolation of triple amplitude #%d from powerspectrum "
                  "failed because of missing powerspectrum",
                  i);
        this->bis[i].bisamp = 1.0F;
        this->bis[i].bisamperr = infinity;
      }
    }
    else if (!pFilter->accept_t3amp || isnan(pRec->t3amp[iwave]))
    {
      g_assert(fake_t3amp == FAKE_NONE);
      this->bis[i].fakeamp = false;
      this->bis[i].bisamp = 1.0F;
      this->bis[i].bisamperr = infinity;
    }
    else
    {
      this->bis[i].fakeamp = false;
      this->bis[i].bisamp = (float)pRec->t3amp[iwave];
      this->bis[i].bisamperr = (float)pRec->t3amperr[iwave];
    }
    if (!pFilter->accept_t3phi || isnan(pRec->t3phi[iwave]))
    {
      this->bis[i].bisphi = 0.0F;
      this->bis[i].bisphierr = infinity;
    }
    else
    {
      this->bis[i].bisphi = (float)pRec->t3phi[iwave];
      this->bis[i].bisphierr = (float)pRec->t3phierr[iwave];
    }
    g_assert(!pRec->flag[iwave]);
    this->bis[i].bisampmodel = this->bis[i].bisamp;
    this->bis[i].bisphimodel = this->bis[i].bisphi;
    this->bis[i].target_id = pRec->target_id;
    this->bis[i].mjd = pRec->mjd;
    this->bis[i].int_time = pRec->int_time;
    this->bis[i].eff_wave = effWave;
    this->bis[i].sta_index[0] = pRec->sta_index[0];
    this->bis[i].sta_index[1] = pRec->sta_index[1];
    this->bis[i].sta_index[2] = pRec->sta_index[2];
    this->bis[i].corrindx_t3amp = pRec->corrindx_t3amp + iwave;
    this->bis[i].corrindx_t3phi = pRec->corrindx_t3phi + iwave;
    this->bis[i].extver = extver;
    this->bis[i].irec = irec;
    this->bis[i].iwave = iwave;
    pOutTab->nbis++;
    i++;
  }
}

/**
 * Read from @a filename, selecting data according to @a param.
 * @memberof BsmemData
 */
BsmemData *BsmemDataFromFilename(const char *filename, const BsmemParam *param)
{
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");
  if (param == NULL) throw(NullPointerException, "Argument 'param' is NULL.");

  BsmemData *this = g_malloc0(sizeof(*this));

  /* Read unfiltered data */
  int status = 0;
  read_oi_fits(filename, &this->origdata, &status);
  if (status)
    throwf(FitsException, "Failed to read OIFITS data from '%s'.", filename);
  float max = uvmax(&this->origdata, NULL);
  g_debug("Data has max UV radius %g waves; filtering at %g waves", max,
          param->uv_max);
  if (max > param->uv_max) g_message("Tapering UV at %g waves", param->uv_max);

  /* Initialise filter */
  oi_filter_spec filt;
  BsmemParamSetFilter(param, &filt);
  if (strlen(param->target) > 0)
  {
    target *pTarg =
        oi_fits_lookup_target_by_name(&this->origdata, param->target);
    if (pTarg == NULL)
      throwf(RuntimeException, "Target '%s' not present in data.",
             param->target);
    filt.target_id = pTarg->target_id;
  }
  else
  {
    /* Select first target */
    if (this->origdata.targets.ntarget == 0)
      throw(RuntimeException, "OI_TARGET table is empty.");
    filt.target_id = this->origdata.targets.targ[0].target_id;
    if (this->origdata.targets.ntarget > 1)
      g_warning("Selecting first target (TARGET_ID=%d) out of %d",
                filt.target_id, this->origdata.targets.ntarget);
  }
  // TODO: want square not circular taper?
  this->uv_max = uvmax(&this->origdata, &filt);

  /* Allocate storage */
  int npow, nbis, nuv_alloc;
  countdata(&this->origdata, &filt, &npow, &nbis);
  nuv_alloc = 1 + npow + 3 * nbis; /* upper limit */
  this->nuv = 1;
  this->npow = 1 + npow;
  this->nbis = nbis;
  this->uv = g_malloc(nuv_alloc * sizeof(this->uv[0]));
  this->pow = g_malloc(this->npow * sizeof(this->pow[0]));
  this->bis = g_malloc(this->nbis * sizeof(this->bis[0]));
  init_oi_fits_model(&this->outdata);

  /* Set zero-baseline point */
  this->uv[0].u = 0.0;
  this->uv[0].v = 0.0;
  this->pow[0] = get_flux_pow(param, 0);

  /* Read power spectra from OI_VIS2 tables */
  BsmemDataSetPow(this, &this->origdata, &filt);

  /* Read bispectra from OI_T3 tables */
  BsmemDataSetBis(this, &this->origdata, &filt, param->fake_t3amp);

  g_assert_cmpint(this->nuv, <=, nuv_alloc);
  this->uv = g_realloc(this->uv, this->nuv * sizeof(this->uv[0]));

  return this;
}

/**
 * Find stored uv point matching @a uv or its conjugate.
 * @memberof BsmemData
 *
 * @param this       BsmemData instance.
 * @param uv         The uv point to match.
 * @param threshold  Matching tolerance.
 * @param pRef       Return location for reference to existing uv point,
 *                   or NULL.
 * @return bool  true if match found within @a threshold.
 */
bool BsmemDataFindUV(const BsmemData *this, BsmemUV uv, float threshold,
                     BsmemUVRef *const pRef)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  for (int iuv = 0; iuv < this->nuv; iuv++)
  {
    int cmp;
    if ((cmp = uvcmp(uv, this->uv[iuv], threshold)))
    {
      if (pRef != NULL)
      {
        pRef->iuv = iuv;
        pRef->sign = cmp;
      }
      return true;
    }
  }
  return false;
}

/**
 * Add uv point if neither the point or its conjugate are already stored.
 *
 * The coordinates are conjugated to the positive u half-plane.
 *
 * @param this   BsmemData instance.
 * @param uv     Potential new uv point.
 * @return BsmemUVRef  Reference to stored uv point.
 */
static BsmemUVRef BsmemDataAddUV(BsmemData *this, BsmemUV uv)
{
  const float uv_threshold = 5.0E-5F;
  BsmemUVRef ref;

  if (!BsmemDataFindUV(this, uv, uv_threshold, &ref))
  {
    ref.iuv = this->nuv++;
    this->uv[ref.iuv] = uvconj(uv, &ref.sign);
  }
  return ref;
}

/**
 * @memberof BsmemData
 */
BsmemUV BsmemDataGetPowAB(const BsmemData *this, int ipow)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (ipow < 0 || ipow >= this->npow)
    throwf(IllegalArgumentException,
           "Powerspectrum index out of range [0, %d): %d", this->npow, ipow);

  BsmemUV ret;
  ret.u = this->pow[ipow].ab.sign * this->uv[this->pow[ipow].ab.iuv].u;
  ret.v = this->pow[ipow].ab.sign * this->uv[this->pow[ipow].ab.iuv].v;
  return ret;
}

/**
 * @memberof BsmemData
 */
BsmemUV BsmemDataGetBisAB(const BsmemData *this, int ibis)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (ibis < 0 || ibis >= this->nbis)
    throwf(IllegalArgumentException,
           "Bispectrum index out of range [0, %d): %d", this->nbis, ibis);

  BsmemUV ret;
  ret.u = this->bis[ibis].ab.sign * this->uv[this->bis[ibis].ab.iuv].u;
  ret.v = this->bis[ibis].ab.sign * this->uv[this->bis[ibis].ab.iuv].v;
  return ret;
}

/**
 * @memberof BsmemData
 */
BsmemUV BsmemDataGetBisBC(const BsmemData *this, int ibis)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (ibis < 0 || ibis >= this->nbis)
    throwf(IllegalArgumentException,
           "Bispectrum index out of range [0, %d): %d", this->nbis, ibis);

  BsmemUV ret;
  ret.u = this->bis[ibis].bc.sign * this->uv[this->bis[ibis].bc.iuv].u;
  ret.v = this->bis[ibis].bc.sign * this->uv[this->bis[ibis].bc.iuv].v;
  return ret;
}

/**
 * @memberof BsmemData
 */
BsmemUV BsmemDataGetBisCA(const BsmemData *this, int ibis)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (ibis < 0 || ibis >= this->nbis)
    throwf(IllegalArgumentException,
           "Bispectrum index out of range [0, %d): %d", this->nbis, ibis);

  BsmemUV ret;
  ret.u = this->bis[ibis].ca.sign * this->uv[this->bis[ibis].ca.iuv].u;
  ret.v = this->bis[ibis].ca.sign * this->uv[this->bis[ibis].ca.iuv].v;
  return ret;
}

/**
 * @memberof BsmemData
 */
float BsmemDataCalcModelPow(const BsmemData *this, int ipow,
                            const float complex *vis)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (ipow < 0 || ipow >= this->npow)
    throwf(IllegalArgumentException,
           "Powerspectrum index out of range [0, %d): %d", this->npow, ipow);
  if (vis == NULL) throw(NullPointerException, "Argument 'vis' is NULL.");

  return square(cabsf(vis[this->pow[ipow].ab.iuv]));
}

/**
 * @memberof BsmemData
 */
float complex BsmemDataCalcModelBis(const BsmemData *this, int ibis,
                                    const float complex *vis)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (ibis < 0 || ibis >= this->nbis)
    throwf(IllegalArgumentException,
           "Bispectrum index out of range [0, %d): %d", this->nbis, ibis);
  if (vis == NULL) throw(NullPointerException, "Argument 'vis' is NULL.");

  float complex V0ab, V0bc, V0ca;
  V0ab = vis[this->bis[ibis].ab.iuv];
  V0bc = vis[this->bis[ibis].bc.iuv];
  V0ca = vis[this->bis[ibis].ca.iuv];
  if (this->bis[ibis].ab.sign < 0) V0ab = conj(V0ab);
  if (this->bis[ibis].bc.sign < 0) V0bc = conj(V0bc);
  if (this->bis[ibis].ca.sign < 0) V0ca = conj(V0ca);

  return V0ab * V0bc * V0ca;
}

/**
 * @memberof BsmemData
 */
void BsmemDataSetModel(BsmemData *this, const float complex *vis)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (vis == NULL) throw(NullPointerException, "Argument 'vis' is NULL.");

  for (int ipow = 0; ipow < this->npow; ipow++)
    this->pow[ipow].powmodel = BsmemDataCalcModelPow(this, ipow, vis);

  for (int ibis = 0; ibis < this->nbis; ibis++)
  {
    float complex cplx = BsmemDataCalcModelBis(this, ibis, vis);
    this->bis[ibis].bisampmodel = cabsf(cplx);
    this->bis[ibis].bisphimodel = cargf(cplx) * 180. / M_PI;
  }
}

/**
 * Make deep copy of array of @a n strings, substituting @a value for any
 * initial '?' in @a template.
 */
static char **make_tform(const char **template, int n, int value)
{
  char **tform = g_malloc(n * sizeof(*tform));
  for (int i = 0; i < n; i++)
  {
    if (template[i][0] == '?')
    {
      int size = strlen(template[i]) + 4; /* allow for extra digits + \0 */
      tform[i] = g_malloc(size);
      int needed = g_snprintf(tform[i], size, "%d%s", value, &template[i][1]);
      g_assert_cmpint(needed, <, size); /* fails if string was truncated */
    }
    else
    {
      tform[i] = g_malloc((strlen(template[i]) + 1)); /* +1 for \0 */
      strcpy(tform[i], template[i]);
    }
  }
  return tform;
}

/**
 * Free array of n strings returned by make_tform().
 */
static void free_tform(char **tform, int n)
{
  for (int i = 0; i < n; i++)
    g_free(tform[i]);
  g_free(tform);
}

/**
 * Write single OI_VIS2 table with additional column for model values.
 */
static void BsmemDataWriteOiVis2(const BsmemData *this, fitsfile *fits,
                                 oi_vis2_model vis2, int extver)
{
  int status = 0;
  const int tfields = 11; /* mandatory columns */
  double zerotime = 0.0;
  char unflagged = 0;
  char *ttype[] = {"TARGET_ID", "TIME",      "MJD",           "INT_TIME",
                   "VIS2DATA",  "VIS2ERR",   "NS_MODEL_VIS2", "UCOORD",
                   "VCOORD",    "STA_INDEX", "FLAG"};
  const char *tformTpl[] = {"I",  "D",  "D",  "D",  "?D", "?D",
                            "?D", "1D", "1D", "2I", "?L"};
  char **tform;
  char *tunit[] = {"\0", "s", "day", "s",  "\0", "\0",
                   "\0", "m", "m",   "\0", "\0"};
  char extname[] = "OI_VIS2";
  int revision = 2;
  bool correlated;

  /* Create table structure */
  tform = make_tform(tformTpl, tfields, vis2.nwave);
  fits_create_tbl(fits, BINARY_TBL, 0, tfields, ttype, tform, tunit, extname,
                  &status);
  free_tform(tform, tfields);

  /* Write mandatory keywords */
  fits_write_key(fits, TINT, "OI_REVN", &revision,
                 "Revision number of the table definition", &status);
  fits_write_key(fits, TSTRING, "DATE-OBS", &vis2.date_obs,
                 "UTC start date of observations", &status);
  if (strlen(vis2.arrname) > 0)
    fits_write_key(fits, TSTRING, "ARRNAME", &vis2.arrname, "Array name",
                   &status);
  else
    g_warning("vis2.arrname not set");
  fits_write_key(fits, TSTRING, "INSNAME", &vis2.insname, "Detector name",
                 &status);
  fits_write_key(fits, TINT, "EXTVER", &extver, "ID number of this OI_VIS2",
                 &status);

  /* Write optional keywords */
  correlated = (strlen(vis2.corrname) > 0);
  if (correlated)
    fits_write_key(fits, TSTRING, "CORRNAME", &vis2.corrname,
                   "Correlated data set name", &status);

  int irec = -1, irow = 0;
  for (int ipow = 0; ipow < vis2.npow; ipow++)
  {
    BsmemPow *pPow = &this->pow[vis2.ipow0 + ipow];
    if (pPow->irec > irec)
    {
      /* New input record, map to new output record */
      irec = pPow->irec;
      irow++;
      fits_write_col(fits, TINT, 1, irow, 1, 1, &pPow->target_id, &status);
      fits_write_col(fits, TDOUBLE, 2, irow, 1, 1, &zerotime, &status);
      fits_write_col(fits, TDOUBLE, 3, irow, 1, 1, &pPow->mjd, &status);
      fits_write_col(fits, TDOUBLE, 4, irow, 1, 1, &pPow->int_time, &status);
      fits_write_col_null(fits, 5, irow, 1, vis2.nwave, &status);
      fits_write_col_null(fits, 6, irow, 1, vis2.nwave, &status);
      fits_write_col_null(fits, 7, irow, 1, vis2.nwave, &status);
      double ucoord, vcoord;
      ucoord = this->uv[pPow->ab.iuv].u * pPow->eff_wave;
      vcoord = this->uv[pPow->ab.iuv].v * pPow->eff_wave;
      fits_write_col(fits, TDOUBLE, 8, irow, 1, 1, &ucoord, &status);
      fits_write_col(fits, TDOUBLE, 9, irow, 1, 1, &vcoord, &status);
      fits_write_col(fits, TINT, 10, irow, 1, 2, pPow->sta_index, &status);
      fits_write_col_null(fits, 11, irow, 1, vis2.nwave, &status);
    }
    fits_write_col(fits, TFLOAT, 5, irow, 1 + pPow->iwave, 1, &pPow->pow,
                   &status);
    fits_write_col(fits, TFLOAT, 6, irow, 1 + pPow->iwave, 1, &pPow->powerr,
                   &status);
    fits_write_col(fits, TFLOAT, 7, irow, 1 + pPow->iwave, 1, &pPow->powmodel,
                   &status);
    fits_write_col(fits, TLOGICAL, 11, irow, 1 + pPow->iwave, 1, &unflagged,
                   &status);
  }

  /* Write optional columns */
  if (correlated)
  {
    fits_insert_col(fits, 8, "CORRINDX_VIS2DATA", "J", &status);
    int irec = -1, irow = 1;
    for (int ipow = 0; ipow < (vis2.numrec * vis2.nwave); ipow++)
    {
      BsmemPow *pPow = &this->pow[vis2.ipow0 + ipow];
      if (pPow->irec > irec)
      {
        irec = pPow->irec;
        fits_write_col(fits, TINT, 7, irow++, 1, 1, &pPow->corrindx_vis2data,
                       &status);
      }
    }
  }

  fits_write_chksum(fits, &status);

  if (status)
    throwf(FitsException, "Error writing OI_VIS2 - %s",
           e4c_fits_error_report(status));
}

/**
 * Write single OI_T3 table with additional columns for model values.
 */
static void BsmemDataWriteOiT3(const BsmemData *this, fitsfile *fits,
                               oi_t3_model t3, int extver)
{
  int status = 0;
  const int tfields = 16;
  double zerotime = 0.0;
  char unflagged = 0;
  char *ttype[] = {"TARGET_ID",      "TIME",           "MJD",       "INT_TIME",
                   "T3AMP",          "T3AMPERR",       "T3PHI",     "T3PHIERR",
                   "NS_MODEL_T3AMP", "NS_MODEL_T3PHI", "U1COORD",   "V1COORD",
                   "U2COORD",        "V2COORD",        "STA_INDEX", "FLAG"};
  const char *tformTpl[] = {"I",  "D",  "D",  "D",  "?D", "?D", "?D", "?D",
                            "?D", "?D", "1D", "1D", "1D", "1D", "3I", "?L"};
  char **tform;
  char *tunit[] = {"\0", "s",   "day", "s", "\0", "\0", "deg", "deg",
                   "\0", "deg", "m",   "m", "m",  "m",  "\0",  "\0"};
  char extname[] = "OI_T3";
  int revision = 2;
  bool correlated;

  /* Create table structure */
  tform = make_tform(tformTpl, tfields, t3.nwave);
  fits_create_tbl(fits, BINARY_TBL, 0, tfields, ttype, tform, tunit, extname,
                  &status);
  free_tform(tform, tfields);

  /* Write mandatory keywords */
  fits_write_key(fits, TINT, "OI_REVN", &revision,
                 "Revision number of the table definition", &status);
  fits_write_key(fits, TSTRING, "DATE-OBS", &t3.date_obs,
                 "UTC start date of observations", &status);
  if (strlen(t3.arrname) > 0)
    fits_write_key(fits, TSTRING, "ARRNAME", &t3.arrname, "Array name",
                   &status);
  else
    g_warning("t3.arrname not set");
  fits_write_key(fits, TSTRING, "INSNAME", &t3.insname, "Detector name",
                 &status);
  fits_write_key(fits, TINT, "EXTVER", &extver, "ID number of this OI_T3",
                 &status);

  /* Write optional keywords */
  correlated = (strlen(t3.corrname) > 0);
  if (correlated)
    fits_write_key(fits, TSTRING, "CORRNAME", &t3.corrname,
                   "Correlated data set name", &status);

  int irec = -1, irow = 0;
  for (int ibis = 0; ibis < t3.nbis; ibis++)
  {
    BsmemBis *pBis = &this->bis[t3.ibis0 + ibis];
    if (pBis->irec > irec)
    {
      /* New input record, map to new output record */
      irec = pBis->irec;
      irow++;
      fits_write_col(fits, TINT, 1, irow, 1, 1, &pBis->target_id, &status);
      fits_write_col(fits, TDOUBLE, 2, irow, 1, 1, &zerotime, &status);
      fits_write_col(fits, TDOUBLE, 3, irow, 1, 1, &pBis->mjd, &status);
      fits_write_col(fits, TDOUBLE, 4, irow, 1, 1, &pBis->int_time, &status);
      fits_write_col_null(fits, 5, irow, 1, t3.nwave, &status);
      fits_write_col_null(fits, 6, irow, 1, t3.nwave, &status);
      fits_write_col_null(fits, 7, irow, 1, t3.nwave, &status);
      fits_write_col_null(fits, 8, irow, 1, t3.nwave, &status);
      fits_write_col_null(fits, 9, irow, 1, t3.nwave, &status);
      fits_write_col_null(fits, 10, irow, 1, t3.nwave, &status);
      double ucoord, vcoord;
      ucoord = (pBis->ab.sign * this->uv[pBis->ab.iuv].u * pBis->eff_wave);
      vcoord = (pBis->ab.sign * this->uv[pBis->ab.iuv].v * pBis->eff_wave);
      fits_write_col(fits, TDOUBLE, 11, irow, 1, 1, &ucoord, &status);
      fits_write_col(fits, TDOUBLE, 12, irow, 1, 1, &vcoord, &status);
      ucoord = (pBis->bc.sign * this->uv[pBis->bc.iuv].u * pBis->eff_wave);
      vcoord = (pBis->bc.sign * this->uv[pBis->bc.iuv].v * pBis->eff_wave);
      fits_write_col(fits, TDOUBLE, 13, irow, 1, 1, &ucoord, &status);
      fits_write_col(fits, TDOUBLE, 14, irow, 1, 1, &vcoord, &status);
      fits_write_col(fits, TINT, 15, irow, 1, 3, pBis->sta_index, &status);
      fits_write_col_null(fits, 16, irow, 1, t3.nwave, &status);
    }
    fits_write_col(fits, TFLOAT, 5, irow, 1 + pBis->iwave, 1, &pBis->bisamp,
                   &status);
    fits_write_col(fits, TFLOAT, 6, irow, 1 + pBis->iwave, 1, &pBis->bisamperr,
                   &status);
    fits_write_col(fits, TFLOAT, 7, irow, 1 + pBis->iwave, 1, &pBis->bisphi,
                   &status);
    fits_write_col(fits, TFLOAT, 8, irow, 1 + pBis->iwave, 1, &pBis->bisphierr,
                   &status);
    fits_write_col(fits, TFLOAT, 9, irow, 1 + pBis->iwave, 1,
                   &pBis->bisampmodel, &status);
    fits_write_col(fits, TFLOAT, 10, irow, 1 + pBis->iwave, 1,
                   &pBis->bisphimodel, &status);
    fits_write_col(fits, TLOGICAL, 16, irow, 1 + pBis->iwave, 1, &unflagged,
                   &status);
  }

  /* Write optional columns */
  if (correlated)
  {
    fits_insert_col(fits, 9, "CORRINDX_T3AMP", "J", &status);
    fits_insert_col(fits, 11, "CORRINDX_T3PHI", "J", &status);
    int irec = -1, irow = 1;
    for (int ibis = 0; ibis < (t3.numrec * t3.nwave); ibis++)
    {
      BsmemBis *pBis = &this->bis[t3.ibis0 + ibis];
      if (pBis->irec > irec)
      {
        irec = pBis->irec;
        fits_write_col(fits, TINT, 9, irow, 1, 1, &pBis->corrindx_t3amp,
                       &status);
        fits_write_col(fits, TINT, 11, irow++, 1, 1, &pBis->corrindx_t3phi,
                       &status);
      }
    }
  }

  fits_write_chksum(fits, &status);

  if (status)
    throwf(FitsException, "Error writing OI_T3 - %s",
           e4c_fits_error_report(status));
}

static void BsmemDataWriteOiFits(BsmemData *this, fitsfile *fits)
{
  int status = 0;

  /* Write primary header keywords */
  write_oi_header(fits, this->origdata.header, &status);

  /* Write OI_TARGET table */
  oi_target targets;
  targets.revision = 2;
  targets.ntarget = 1;
  targets.targ = &this->outdata.object;
  targets.usecategory = false;
  write_oi_target(fits, targets, &status);

  /* Write all OI_ARRAY tables as v2 */
  {
    GList *link = this->outdata.arrayList;
    int extver = 1;
    while (link != NULL)
    {
      oi_array *pTab = link->data;
      upgrade_oi_array(pTab);
      write_oi_array(fits, *pTab, extver++, &status);
      link = link->next;
    }
  }

  /* Write all OI_WAVELENGTH tables as v2 */
  {
    GList *link = this->outdata.wavelengthList;
    int extver = 1;
    while (link != NULL)
    {
      oi_wavelength *pTab = link->data;
      upgrade_oi_wavelength(pTab);
      write_oi_wavelength(fits, *pTab, extver++, &status);
      link = link->next;
    }
  }

  /* Write all OI_CORR tables */
  {
    GList *link = this->outdata.corrList;
    int extver = 1;
    while (link != NULL)
    {
      write_oi_corr(fits, *((oi_corr *)link->data), extver++, &status);
      link = link->next;
    }
  }

  if (status)
    throwf(FitsException, "Error writing OIFITS - %s",
           e4c_fits_error_report(status));

  /* Write all OI_VIS2 tables */
  {
    GList *link = this->outdata.vis2List;
    int extver = 1;
    while (link != NULL)
    {
      BsmemDataWriteOiVis2(this, fits, *((oi_vis2_model *)link->data),
                           extver++);
      link = link->next;
    }
  }

  /* Write all OI_T3 tables */
  {
    GList *link = this->outdata.t3List;
    int extver = 1;
    while (link != NULL)
    {
      BsmemDataWriteOiT3(this, fits, *((oi_t3_model *)link->data), extver++);
      link = link->next;
    }
  }
}

/**
 * @memberof BsmemData
 */
void BsmemDataWriteTo(BsmemData *this, const char *filename, bool clobber)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (this == NULL) throw(NullPointerException, "Argument 'filename' is NULL.");

  fitsfile *fits;
  e4c_using_fits(fits, filename, clobber ? FITS_CLOBBER : FITS_CREATE)
  {
    BsmemDataWriteOiFits(this, fits);
  }
}

/**
 * @memberof BsmemData
 */
void BsmemDataAppendTo(BsmemData *this, const char *filename)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (this == NULL) throw(NullPointerException, "Argument 'filename' is NULL.");

  fitsfile *fits;
  e4c_using_fits(fits, filename, FITS_OPEN_READWRITE)
  {
    BsmemDataWriteOiFits(this, fits);
  }
}

/**
 * @memberof BsmemData
 */
void DestroyBsmemData(BsmemData *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  g_free(this->uv);
  g_free(this->pow);
  g_free(this->bis);
  free_oi_fits(&this->origdata);
  free_oi_fits_model(&this->outdata);
  g_free(this);
}
