/**
 * @file
 * Definition of GreyImg class.
 *
 * Copyright (C) 2015-2017, 2021 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#ifndef GREY_IMG_H
#define GREY_IMG_H

#include "e4c.h"

#include <stdbool.h>

#define MAS_TO_DEG (1. / 3600. / 1000.)
#ifndef INPUT_PARAM_NAME
#define INPUT_PARAM_NAME "IMAGE-OI INPUT PARAM"
#endif
#define LEN_NAME 71

/**
 * @class GreyImg
 * Greyscale image class.
 */
typedef struct
{
  /** @publicsection */
  char name[LEN_NAME];
  long naxis1;
  long naxis2;
  float pixelsize;
  float *image;

} GreyImg;

/** @cond DOCUMENT_METHODS_AS_FUNCTIONS */
GreyImg *CreateGreyImg(const char *, long, long, float);
GreyImg *GreyImgFromInputFilename(const char *, const char *);
GreyImg *GreyImgFromImageFilename(const char *);
GreyImg *GreyImgFromImageFilenameNoWCS(const char *, float);
GreyImg *GreyImgCopy(const GreyImg *);
bool GreyImgIsSquare(const GreyImg *);
float GreyImgGetFlux(const GreyImg *);
void GreyImgNormalise(GreyImg *);
bool GreyImgSetBlank(GreyImg *, float, float);
void GreyImgAddRectangle(GreyImg *, float, float, float, float, float);
void GreyImgAddDirac(GreyImg *, float, float, float);
void GreyImgAddDisk(GreyImg *, float, float, float, float);
void GreyImgAddGaussian(GreyImg *, float, float, float, float);
void GreyImgAddLorentzian(GreyImg *, float, float, float, float);
void GreyImgWritePrimaryTo(GreyImg *, const char *, bool);
void GreyImgAppendImageTo(GreyImg *, const char *);
void DestroyGreyImg(GreyImg *);
/** @endcond */

#endif /* #ifndef GREY_IMG_H */
