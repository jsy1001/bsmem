/**
 * @file
 * Implementation of BsmemParam class.
 *
 * Copyright (C) 2015-2017, 2021-2023 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "BsmemParam.h"
#include "e4c_fitsio.h"

#include <glib.h>

#include <string.h>

#ifndef INPUT_PARAM_NAME
#define INPUT_PARAM_NAME "IMAGE-OI INPUT PARAM"
#endif

char *useAmpPhiName[] = {"ALL", "NONE", "AMP", "PHI"};
char *fakeT3AmpName[] = {"MISSING", "ALL", "NONE"};

/**
 * @memberof BsmemParam
 */
BsmemParam *CreateBsmemParam(void)
{
  BsmemParam *this = g_malloc(sizeof(*this));
  this->filename[0] = '\0';
  this->initimgname[0] = '\0';
  this->priorimgname[0] = '\0';
  this->target[0] = '\0';
  this->target_id = -1;
  this->wave_min = 0.0F;
  this->wave_max = 1.0F;
  this->use_vis =
      USE_NONE; /* currently BSMEM cannot use complex visibilities */
  this->use_vis2 = true;
  this->use_t3 = USE_ALL;
  this->uv_max = 1E11F;
  this->maxiter = 200;
  this->flux = 1.0F;
  this->fluxerr = 1E-1F;
  this->initflux = 0.01F;
  this->regularization = 4;
  this->entropy_func = 1;
  this->alpha = 100.0F;
  this->v2a = 1.0F;
  this->t3ampa = 1.0F;
  this->t3phia = 1.0F;
  this->v2b = 0.0F;
  this->t3ampb = 0.0F;
  this->t3phib = 0.0F;
  this->biserrtype = 0;
  this->fake_t3amp = FAKE_MISSING;

  return this;
}

/**
 * Read optional string-valued header keyword.
 *
 * The value pointed to by @a keyval is left unchanged if the keyword
 * is missing or has no value.
 *
 * @return true if keyword read successfully, false otherwise
 */
static bool read_key_opt_string(fitsfile *fits, const char *keyname,
                                char *const keyval)
{
  int status = 0;

  fits_write_errmark();
  if (fits_read_key(fits, TSTRING, keyname, keyval, NULL, &status))
  {
    if (status == KEY_NO_EXIST || status == VALUE_UNDEFINED)
    {
      fits_clear_errmark(); /* clear last error from CFITSIO stack */
      return false;
    }
    else
    {
      throwf(FitsException, "Failed to read '%s' keyword - %s", keyname,
             e4c_fits_error_report(status));
    }
  }
  return true;
}

/**
 * Read optional floating point-valued header keyword.
 *
 * The value pointed to by @a keyval is left unchanged if the keyword
 * is missing or has no value.
 *
 * @return true if keyword read successfully, false otherwise
 */
static bool read_key_opt_float(fitsfile *fits, const char *keyname,
                               float *const keyval)
{
  int status = 0;

  fits_write_errmark();
  if (fits_read_key(fits, TFLOAT, keyname, keyval, NULL, &status))
  {
    if (status == KEY_NO_EXIST || status == VALUE_UNDEFINED)
    {
      fits_clear_errmark(); /* clear last error from CFITSIO stack */
      return false;
    }
    else
    {
      throwf(FitsException, "Failed to read '%s' keyword - %s", keyname,
             e4c_fits_error_report(status));
    }
  }
  return true;
}

/**
 * Read optional boolean-valued header keyword.
 *
 * The value pointed to by @a keyval is left unchanged if the keyword
 * is missing or has no value.
 *
 * @return true if keyword read successfully, false otherwise
 */
static bool read_key_opt_bool(fitsfile *fits, const char *keyname,
                              bool *const keyval)
{
  int status = 0, val;

  fits_write_errmark();
  if (fits_read_key(fits, TLOGICAL, keyname, &val, NULL, &status))
  {
    if (status == KEY_NO_EXIST || status == VALUE_UNDEFINED)
    {
      fits_clear_errmark(); /* clear last error from CFITSIO stack */
      return false;
    }
    else
    {
      throwf(FitsException, "Failed to read '%s' keyword - %s", keyname,
             e4c_fits_error_report(status));
    }
  }
  *keyval = val;
  return true;
}

/**
 * Read UseAmpPhi from string-valued header keyword.
 *
 * @exception FitsException if keyword is missing or has an illegal value.
 */
static void read_key_use_amp_phi(fitsfile *fits, const char *keyname,
                                 UseAmpPhi *const keyval)
{
  int status = 0;
  char val[FLEN_VALUE];

  fits_read_key(fits, TSTRING, keyname, val, NULL, &status);
  if (status)
    throwf(FitsException, "Failed to read '%s' keyword - %s", keyname,
           e4c_fits_error_report(status));

  *keyval = USE_NULL;
  for (int i = 0; i < NUM_USE_AMP_PHI; i++)
  {
    if (strcmp(val, useAmpPhiName[i]) == 0) *keyval = (UseAmpPhi)i;
  }
  if (*keyval == USE_NULL)
    throwf(FitsException,
           "Illegal value '%s' for '%s' keyword - "
           "expected 'NONE', 'ALL', 'AMP' or 'PHI'",
           val, keyname);
}

/**
 * Read FakeT3Amp from optional string-valued header keyword.
 *
 * The value pointed to by @a keyval is left unchanged if the keyword
 * is missing or has no value.
 *
 * @return true if keyword read successfully, false if keyword is missing or has
 * no value
 * @exception FitsException if keyword has an illegal value.
 */
static bool read_key_fake_t3amp(fitsfile *fits, const char *keyname,
                                FakeT3Amp *const keyval)
{
  int status = 0;
  char val[FLEN_VALUE];

  fits_write_errmark();
  if (fits_read_key(fits, TSTRING, keyname, val, NULL, &status))
  {
    if (status == KEY_NO_EXIST || status == VALUE_UNDEFINED)
    {
      fits_clear_errmark(); /* clear last error from CFITSIO stack */
      return false;
    }
    else
    {
      throwf(FitsException, "Failed to read '%s' keyword - %s", keyname,
             e4c_fits_error_report(status));
    }
  }

  *keyval = FAKE_NULL;
  for (int i = 0; i < NUM_FAKE_T3AMP; i++)
  {
    if (strcmp(val, fakeT3AmpName[i]) == 0) *keyval = (FakeT3Amp)i;
  }
  if (*keyval == FAKE_NULL)
    throwf(FitsException,
           "Illegal value '%s' for '%s' keyword - "
           "expected 'MISSING', 'ALL', or 'NONE'",
           val, keyname);
  return true;
}

/**
 * @memberof BsmemParam
 */
BsmemParam *BsmemParamFromFilename(const char *filename)
{
  /* Create object, with default values for optional parameters */
  BsmemParam *this = CreateBsmemParam();

  /* Read input parameters HDU */
  fitsfile *fits;
  e4c_using_fits(fits, filename, FITS_OPEN_READONLY)
  {
    int status = 0;
    int use_vis2;
    fits_movnam_hdu(fits, BINARY_TBL, INPUT_PARAM_NAME, 0, &status);
    if (status)
      throwf(FitsException, "Failed to lookup parameters HDU - %s",
             e4c_fits_error_report(status));

    /* Read mandatory standard parameters */
    fits_read_key(fits, TSTRING, "INIT_IMG", this->initimgname, NULL, &status);
    this->target_id = -1;
    fits_read_key(fits, TFLOAT, "WAVE_MIN", &this->wave_min, NULL, &status);
    fits_read_key(fits, TFLOAT, "WAVE_MAX", &this->wave_max, NULL, &status);
    fits_read_key(fits, TLOGICAL, "USE_VIS2", &use_vis2, NULL, &status);
    fits_read_key(fits, TINT, "MAXITER", &this->maxiter, NULL, &status);
    if (status)
      throwf(FitsException, "Failed to read input parameters - %s",
             e4c_fits_error_report(status));

    read_key_use_amp_phi(fits, "USE_VIS", &this->use_vis);
    read_key_use_amp_phi(fits, "USE_T3", &this->use_t3);
    this->use_vis2 = use_vis2 != 0;

    /* Read optional standard parameters */
    bool autow;
    if (read_key_opt_bool(fits, "AUTO_WGT", &autow))
      this->regularization = autow ? 4 : 3;
    /* else use default set in constructor */
    read_key_opt_float(fits, "RGL_WGT", &this->alpha);
    // TODO: RGL_NAME = entropy_func
    read_key_opt_string(fits, "RGL_PRIO", this->priorimgname);
    read_key_opt_string(fits, "TARGET", this->target);
    read_key_opt_float(fits, "FLUX", &this->flux);

    /* Read BSMEM-specific parameters (all optional) */
    read_key_opt_float(fits, "UV_MAX", &this->uv_max);
    read_key_opt_float(fits, "FLUXERR", &this->fluxerr);
    read_key_opt_float(fits, "INITFLUX", &this->initflux);
    read_key_opt_float(fits, "V2A", &this->v2a);
    read_key_opt_float(fits, "T3AMPA", &this->t3ampa);
    read_key_opt_float(fits, "T3PHIA", &this->t3phia);
    read_key_opt_float(fits, "V2B", &this->v2b);
    read_key_opt_float(fits, "T3AMPB", &this->t3ampb);
    read_key_opt_float(fits, "T3PHIB", &this->t3phib);
    read_key_fake_t3amp(fits, "FAKET3A", &this->fake_t3amp);
  }
  g_strlcpy(this->filename, filename, FLEN_FILENAME);

  return this;
}

/**
 * Return deep copy of BsmemParam instance.
 * @memberof BsmemParam
 */
BsmemParam *BsmemParamCopy(const BsmemParam *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  BsmemParam *copy = g_malloc(sizeof(*copy));
  memcpy(copy, this, sizeof(*this));
  return copy;
}

/**
 * @memberof BsmemParam
 */
void BsmemParamSetFilter(const BsmemParam *this, oi_filter_spec *const pFilter)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (pFilter == NULL)
    throw(NullPointerException, "Argument 'pFilter' is NULL.");

  init_oi_filter(pFilter);
  // TODO: set target_id from target name?
  pFilter->target_id = this->target_id;
  pFilter->wave_range[0] = this->wave_min;
  pFilter->wave_range[1] = this->wave_max;
  pFilter->uvrad_range[0] = 0.;
  pFilter->uvrad_range[1] = this->uv_max;
  // FIXME: filter vis amp and phi separately
  pFilter->accept_vis = (this->use_vis == USE_ALL);
  pFilter->accept_vis2 = this->use_vis2;
  pFilter->accept_t3amp = (this->use_t3 == USE_ALL || this->use_t3 == USE_AMP);
  pFilter->accept_t3phi = (this->use_t3 == USE_ALL || this->use_t3 == USE_PHI);
  pFilter->accept_flagged = false;
}

/**
 * @memberof BsmemParam
 */
void BsmemParamAppendTo(BsmemParam *this, const char *filename)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");

  /* Write input parameters HDU */
  fitsfile *fits;
  e4c_using_fits(fits, filename, FITS_OPEN_READWRITE)
  {
    int status = 0;
    int use_vis2 = this->use_vis2;
    int autow = !(this->regularization == 3);
    fits_create_tbl(fits, BINARY_TBL, 0, 0, NULL, NULL, NULL, INPUT_PARAM_NAME,
                    &status);
    if (status)
      throwf(FitsException, "Failed to create input parameters table - %s",
             e4c_fits_error_report(status));

    /* Write mandatory standard parameters */
    fits_write_key(fits, TSTRING, "INIT_IMG", this->initimgname,
                   "Identifier of the initial image HDU", &status);
    fits_write_key(fits, TFLOAT, "WAVE_MIN", &this->wave_min,
                   "[m] Minimum wavelength to select", &status);
    fits_write_key(fits, TFLOAT, "WAVE_MAX", &this->wave_max,
                   "[m] Maximum wavelength to select", &status);
    fits_write_key(fits, TSTRING, "USE_VIS", useAmpPhiName[this->use_vis],
                   "Use complex visibility data if any", &status);
    fits_write_key(fits, TLOGICAL, "USE_VIS2", &use_vis2,
                   "Use squared visibility data if any", &status);
    fits_write_key(fits, TSTRING, "USE_T3", useAmpPhiName[this->use_t3],
                   "Use triple product data if any", &status);
    fits_write_key(fits, TINT, "MAXITER", &this->maxiter,
                   "Maximum number of iterations to run", &status);

    /* Write optional standard parameters */
    fits_write_key(fits, TLOGICAL, "AUTO_WGT", &autow,
                   "Automatic regularization weight", &status);
    fits_write_key(fits, TFLOAT, "RGL_WGT", &this->alpha,
                   "Weight of the regularization", &status);
    if (this->priorimgname[0] != '\0')
      fits_write_key(fits, TSTRING, "RGL_PRIO", this->priorimgname,
                     "Identifier of the prior image HDU", &status);
    if (this->target[0] != '\0')
      fits_write_key(fits, TSTRING, "TARGET", this->target,
                     "Identifier of the target object to reconstruct", &status);
    else // TODO: or omit TARGET keyword?
      fits_write_key_null(fits, "TARGET", "No target specified", &status);
    fits_write_key(fits, TFLOAT, "FLUX", &this->flux, "Assumed total flux",
                   &status);

    /* Write BSMEM-specific parameters */
    fits_write_key(fits, TFLOAT, "UV_MAX", &this->uv_max,
                   "[waves] Maximum UV radius to select", &status);
    fits_write_key(fits, TFLOAT, "FLUXERR", &this->fluxerr,
                   "Error on zero-baseline V^2 point", &status);
    fits_write_key(fits, TFLOAT, "INITFLUX", &this->initflux,
                   "Initial image flux", &status);
    fits_write_key(fits, TFLOAT, "V2A", &this->v2a,
                   "Squared visibility error factor", &status);
    fits_write_key(fits, TFLOAT, "T3AMPA", &this->t3ampa,
                   "Triple amplitude error factor", &status);
    fits_write_key(fits, TFLOAT, "T3PHIA", &this->t3phia,
                   "Closure phase error factor", &status);
    fits_write_key(fits, TFLOAT, "V2B", &this->v2b,
                   "Squared visibility error offset", &status);
    fits_write_key(fits, TFLOAT, "T3AMPB", &this->t3ampb,
                   "Triple amplitude error offset", &status);
    fits_write_key(fits, TFLOAT, "T3PHIB", &this->t3phib,
                   "Closure phase error offset", &status);
    fits_write_key(fits, TSTRING, "FAKET3A", fakeT3AmpName[this->fake_t3amp],
                   "Fake triple amplitudes from powerspectra", &status);

    if (status)
      throwf(FitsException, "Failed to write input parameters - %s",
             e4c_fits_error_report(status));
  }
}

/**
 * @memberof BsmemParam
 */
void DestroyBsmemParam(BsmemParam *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  g_free(this);
}
