/**
 * @file
 * Implementation of GreyImg class.
 *
 * Copyright (C) 2015-2018, 2021 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "GreyImg.h"
#include "e4c_fitsio.h"

#include <glib.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>

static float square(float x)
{
  return x * x;
}

/**
 * Constructor.
 * @memberof GreyImg
 *
 * @param name       Name of image, written as FITS HDUNAME keyword.
 * @param naxis1     First (fast) dimension of image (FITS ordering).
 * @param naxis2     Second (slow) dimension of image (FITS ordering).
 * @param pixelsize  Image pixel size in milliarcseconds.
 *
 * @return GreyImg *  New GreyImg instance.
 * @exception NullPointerException if @a name is NULL.
 */
GreyImg *CreateGreyImg(const char *name, long naxis1, long naxis2,
                       float pixelsize)
{
  if (name == NULL) throw(NullPointerException, "Argument 'name' is NULL.");

  GreyImg *this = g_malloc(sizeof(*this));
  g_strlcpy(this->name, name, LEN_NAME);
  this->naxis1 = naxis1;
  this->naxis2 = naxis2;
  this->pixelsize = pixelsize;
  this->image = g_malloc0(naxis1 * naxis2 * sizeof(this->image[0]));

  return this;
}

/**
 * Create new GreyImg instance from current FITS (image) HDU.
 *
 * @param fits       CFITSIO file pointer.
 * @param pixelsize  Image pixel size in milliarcseconds, or negative value to
 *                   set it from the WCS keywords in the FITS HDU.
 *
 * @return GreyImg *  New GreyImg instance.
 * @exception NullPointerException if @a fits is NULL.
 */
static GreyImg *GreyImgFromFitsHdu(fitsfile *fits, float pixelsize)
{
  int status = 0;
  char hduname[FLEN_VALUE];
  long naxes[2], fpixel[2] = {1L, 1L};
  double cdelt[2];

  if (fits == NULL) throw(NullPointerException, "Argument 'fits' is NULL.");

  /* Read image header */
  fits_get_img_size(fits, 2, naxes, &status);
  if (status)
    throwf(FitsException, "Failed to read image header - %s",
           e4c_fits_error_report(status));
  fits_write_errmark();
  fits_read_key(fits, TSTRING, "HDUNAME", &hduname, NULL, &status);
  if (status)
  {
    status = 0;
    fits_clear_errmark();
    g_strlcpy(hduname, "NoName", FLEN_VALUE);
  }
  if (pixelsize < 0.0F)
  {
    fits_read_key(fits, TDOUBLE, "CDELT1", &cdelt[0], NULL, &status);
    fits_read_key(fits, TDOUBLE, "CDELT2", &cdelt[1], NULL, &status);
    if (status)
      throw(FitsException, "CDELT1/2 keywords missing, pixelsize unknown");
    if (fabs(cdelt[0] - cdelt[1]) > 1E-10)
      throwf(FitsException,
             "Image pixels are not square (CDELT1=%lg, CDELT2=%lg)", cdelt[0],
             cdelt[1]);
    pixelsize = (float)(cdelt[0] / MAS_TO_DEG);
  }
  GreyImg *this = g_malloc(sizeof(*this));
  g_strlcpy(this->name, hduname, LEN_NAME);
  this->naxis1 = naxes[0];
  this->naxis2 = naxes[1];
  this->pixelsize = pixelsize;

  /* Allocate and read image data */
  this->image = g_malloc(naxes[0] * naxes[1] * sizeof(this->image[0]));
  fits_read_pix(fits, TFLOAT, fpixel, naxes[0] * naxes[1], 0, this->image, 0,
                &status);
  if (status)
    throwf(FitsException, "Failed to read image data - %s",
           e4c_fits_error_report(status));

  return this;
}

/**
 * Create new GreyImg instance from standard FITS input file.
 * @memberof GreyImg
 *
 * @param filename    Input filename.
 * @param hdunamekey  Input parameter keyword giving HDUNAME of image.
 *
 * @exception NullPointerException if @a filename or @a hdunamekey is NULL.
 */
GreyImg *GreyImgFromInputFilename(const char *filename, const char *hdunamekey)
{
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");
  if (hdunamekey == NULL)
    throw(NullPointerException, "Argument 'hdunamekey' is NULL.");

  GreyImg *this = NULL;

  fitsfile *fits;
  e4c_using_fits(fits, filename, FITS_OPEN_READONLY)
  {
    int status = 0;
    int ihdu, nhdu, hdutype;
    char hduname[FLEN_VALUE];

    fits_movnam_hdu(fits, BINARY_TBL, INPUT_PARAM_NAME, 0, &status);
    if (status)
      throwf(FitsException, "Failed to lookup parameters HDU - %s",
             e4c_fits_error_report(status));
    fits_read_key(fits, TSTRING, hdunamekey, &hduname, NULL, &status);
    if (status) throwf(FitsException, "%s keyword missing", hdunamekey);

    fits_get_num_hdus(fits, &nhdu, &status);
    for (ihdu = 1; ihdu <= nhdu; ihdu++)
    {
      char chduname[FLEN_VALUE];

      fits_movabs_hdu(fits, ihdu, &hdutype, &status);
      fits_write_errmark();
      fits_read_key(fits, TSTRING, "HDUNAME", &chduname, NULL, &status);
      if (status)
      {
        status = 0;
        fits_clear_errmark(); /* clear last error from CFITSIO stack */
        continue;             /* next HDU */
      }
      if (strcmp(chduname, hduname) != 0) continue; /* next HDU */
      break;                                        /* current HDU matches */
    }
    if (ihdu > nhdu) throwf(FitsException, "No HDU with HDUNAME='%s'", hduname);
    this = GreyImgFromFitsHdu(fits, -1.0F);
  }
  return this;
}

/**
 * Create new GreyImg instance from FITS primary HDU.
 * @memberof GreyImg
 *
 * The HDU must contain the WCS CDELTi keywords.
 *
 * @param filename  Input filename.
 *
 * @exception NullPointerException if @a filename is NULL.
 */
GreyImg *GreyImgFromImageFilename(const char *filename)
{
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");

  GreyImg *this = NULL;
  fitsfile *fits;
  e4c_using_fits(fits, filename, FITS_OPEN_READONLY)
  {
    int status = 0;

    fits_movabs_hdu(fits, 1, NULL, &status);
    if (status)
      throwf(FitsException, "Failed to access FITS primary HDU - %s",
             e4c_fits_error_report(status));
    this = GreyImgFromFitsHdu(fits, -1.0F);
  }
  return this;
}

/**
 * Create new GreyImg instance from FITS primary HDU without WCS keywords.
 * @memberof GreyImg
 *
 * If WCS keywords are present, they are silently ignored.
 *
 * @param filename   Input filename.
 * @param pixelsize  Image pixel size in milliarcseconds.
 *
 * @exception NullPointerException if @a filename is NULL.
 * @exception IllegalArgumentException if @pixelsize is negative or zero.
 */
GreyImg *GreyImgFromImageFilenameNoWCS(const char *filename, float pixelsize)
{
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");
  if (pixelsize <= 0.0)
    throwf(IllegalArgumentException, "Illegal value for 'pixelsize' (%f).",
           pixelsize);

  GreyImg *this = NULL;
  fitsfile *fits;
  e4c_using_fits(fits, filename, FITS_OPEN_READONLY)
  {
    int status = 0;

    fits_movabs_hdu(fits, 1, NULL, &status);
    if (status)
      throwf(FitsException, "Failed to access FITS primary HDU - %s",
             e4c_fits_error_report(status));
    this = GreyImgFromFitsHdu(fits, pixelsize);
  }
  return this;
}

/**
 * Return deep copy of GreyImg instance.
 * @memberof GreyImg
 */
GreyImg *GreyImgCopy(const GreyImg *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  GreyImg *copy = g_malloc(sizeof(*copy));
  memcpy(copy, this, sizeof(*this));
  size_t size = this->naxis1 * this->naxis2 * sizeof(this->image[0]);
  copy->image = g_malloc(size);
  memcpy(copy->image, this->image, size);
  return copy;
}

/**
 * Does the image have the same dimension in both axes?
 * @memberof GreyImg
 *
 * @param this  GreyImg instance.
 * @return bool  true if the dimensions match, false otherwise.
 * @exception NullPointerException if @a this is NULL.
 */
bool GreyImgIsSquare(const GreyImg *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  return this->naxis1 == this->naxis2;
}

/**
 * Return the sum of the pixel values in the current image.
 * @memberof GreyImg
 *
 * @param this  GreyImg instance.
 * @return float  The total image flux.
 * @exception NullPointerException if @a this is NULL.
 */
float GreyImgGetFlux(const GreyImg *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  float total = 0.0F;
  for (int j = 0; j < this->naxis2; j++)
    for (int i = 0; i < this->naxis1; i++)
      total += this->image[j * this->naxis1 + i];
  return total;
}

/**
 * Normalise the current image to unit sum.
 * @memberof GreyImg
 *
 * @param this  GreyImg instance to normalise.
 * @exception NullPointerException if @a this is NULL.
 */
void GreyImgNormalise(GreyImg *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  float total = GreyImgGetFlux(this);
  for (int j = 0; j < this->naxis2; j++)
    for (int i = 0; i < this->naxis1; i++)
      this->image[j * this->naxis1 + i] /= total;
}

/**
 * Set pixels less than or equal to @a threshold to @a blank.
 * @memberof GreyImg
 *
 * @param this       GreyImg instance to modify.
 * @param threshold  Replace pixel values at this theshold or below.
 * @param blank      Replacement pixel value.
 * @return bool  true if any pixel values replaced.
 * @exception NullPointerException if @a this is NULL.
 */
bool GreyImgSetBlank(GreyImg *this, float threshold, float blank)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  bool replaced = false;
  for (int j = 0; j < this->naxis2; j++)
    for (int i = 0; i < this->naxis1; i++)
      if (this->image[j * this->naxis1 + i] <= threshold)
      {
        this->image[j * this->naxis1 + i] = blank;
        replaced = true;
      }
  return replaced;
}

/**
 * Add a rectangle component between two specified corners to the current image.
 * @memberof GreyImg
 *
 * @param this  GreyImg instance to normalise.
 * @param minX  First corner position on first (fast) FITS axis /pix.
 * @param minY  First corner position on second (slow) FITS axis /pix.
 * @param maxX  Second (opposite) corner position on first (fast) FITS axis
 * /pix.
 * @param maxY  Second (opposite) corner position on second (slow) FITS axis
 * /pix.
 * @param flux  Integrated flux of component (over entire rectangle).
 * @exception NullPointerException if @a this is NULL.
 */
void GreyImgAddRectangle(GreyImg *this, float minX, float minY, float maxX,
                         float maxY, float flux)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  float height = flux / ((maxY - minY) * (maxX - minX));

  // Rounding is okay as min >= 0 and max <= naxis (should always be true).
  // Round to get as close as possible to requested corners.
  for (int iy = (minY + 0.5); iy < (maxY + 0.5); iy++)
  {
    for (int ix = (minX + 0.5); ix < (maxX + 0.5); ix++)
    {
      this->image[iy * this->naxis1 + ix] += height;
    }
  }
}

/**
 * Add a Dirac delta fundtion component to the current image.
 * @memberof GreyImg
 *
 * @param this  GreyImg instance to normalise.
 * @param posX  Delta function position on first (fast) FITS axis /pix.
 * @param posY  Delta function position on second (slow) FITS axis /pix.
 * @param flux  Integrated flux of Dirac delta.
 * @exception NullPointerException if @a this is NULL.
 */
void GreyImgAddDirac(GreyImg *this, float posX, float posY, float flux)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  // round to get as close as possible to requested location
  int ix = (posX + 0.5);
  int iy = (posY + 0.5);
  this->image[iy * this->naxis1 + ix] += flux;
}

/**
 * Add a circular centred uniform disk component to the current image.
 * @memberof GreyImg
 *
 * @param this      GreyImg instance to normalise.
 * @param xpos      Component position on first (fast) FITS axis /pix.
 * @param ypos      Component position on second (slow) FITS axis /pix.
 * @param diameter  Diameter of disk /pix.
 * @param flux      Integrated flux of component (over entire image).
 * @exception NullPointerException if @a this is NULL.
 */
void GreyImgAddDisk(GreyImg *this, float xpos, float ypos, float flux,
                    float diameter)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  float height = flux * 4 / (M_PI * square(diameter));
  for (int iy = 0; iy < this->naxis2; iy++)
  {
    for (int ix = 0; ix < this->naxis1; ix++)
    {
      float rsq = square(ix - xpos) + square(iy - ypos);
      if (rsq <= square(diameter / 2.0))
        this->image[iy * this->naxis1 + ix] += height;
    }
  }
}

/**
 * Add a circular Gaussian component to the current image.
 * @memberof GreyImg
 *
 * @param this  GreyImg instance to normalise.
 * @param xpos  Component position on first (fast) FITS axis /pix.
 * @param ypos  Component position on second (slow) FITS axis /pix.
 * @param flux  Integrated flux of component (over entire image).
 * @param fwhm  Full width at half maximum intensity /pix.
 * @exception NullPointerException if @a this is NULL.
 */
void GreyImgAddGaussian(GreyImg *this, float xpos, float ypos, float flux,
                        float fwhm)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  float totFlux = 0.0;

  // Get total flux for normalisation.
  for (int iy = 0; iy < this->naxis2; iy++)
  {
    for (int ix = 0; ix < this->naxis1; ix++)
    {
      float rsq = square(ix - xpos) + square(iy - ypos);
      totFlux += exp(-4. * log(2.) * rsq / square(fwhm));
    }
  }

  // Add the Gaussian.
  for (int iy = 0; iy < this->naxis2; iy++)
  {
    for (int ix = 0; ix < this->naxis1; ix++)
    {
      float rsq = square(ix - xpos) + square(iy - ypos);
      this->image[iy * this->naxis1 + ix] +=
          (exp(-4. * log(2.) * rsq / square(fwhm)) * flux / totFlux);
    }
  }
}

/**
 * Add a Lorentzian component to the current image.
 * @memberof GreyImg
 *
 * @param this  GreyImg instance to normalise.
 * @param xpos  Component position on first (fast) FITS axis /pix.
 * @param ypos  Component position on second (slow) FITS axis /pix.
 * @param flux  Integrated flux of component (over entire image).
 * @param fwhm  Full width at half maximum intensity /pix.
 * @exception NullPointerException if @a this is NULL.
 */
void GreyImgAddLorentzian(GreyImg *this, float xpos, float ypos, float flux,
                          float fwhm)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  float totFlux = 0.0;
  float gamma = fwhm / 2.0;

  // Get total flux for normalisation.
  for (int iy = 0; iy < this->naxis2; iy++)
  {
    for (int ix = 0; ix < this->naxis1; ix++)
    {
      float rsq = square(ix - xpos) + square(iy - ypos);
      totFlux += gamma / (rsq + square(gamma));
    }
  }

  // Add the Lorentzian to the image, with the correct total flux.
  for (int iy = 0; iy < this->naxis2; iy++)
  {
    for (int ix = 0; ix < this->naxis1; ix++)
    {
      float rsq = square(ix - xpos) + square(iy - ypos);
      this->image[iy * this->naxis1 + ix] +=
          gamma / (rsq + square(gamma)) * flux / totFlux;
    }
  }
}

/**
 * Create FITS image HDU containing the current image.
 *
 * @exception FitsException on CFITSIO error.
 */
static void GreyImgCreateFitsHdu(GreyImg *this, fitsfile *fits)
{
  int status = 0, ihdu;
  long naxes[2], fpixel[2] = {1L, 1L};
  double crpix = 1.0, crval = 0.0, cdelt;

  naxes[0] = this->naxis1;
  naxes[1] = this->naxis2;
  fits_create_img(fits, FLOAT_IMG, 2, naxes, &status);
  fits_write_key(fits, TSTRING, "HDUNAME", this->name, NULL, &status);
  fits_get_hdu_num(fits, &ihdu);
  if (ihdu > 1)
    fits_write_key(fits, TSTRING, "EXTNAME", this->name, NULL, &status);
  fits_write_key(fits, TDOUBLE, "CRPIX1", &crpix, "Reference pixel", &status);
  fits_write_key(fits, TDOUBLE, "CRPIX2", &crpix, "Reference pixel", &status);
  fits_write_key(fits, TDOUBLE, "CRVAL1", &crval,
                 "Coordinate at reference pixel", &status);
  fits_write_key(fits, TDOUBLE, "CRVAL2", &crval,
                 "Coordinate at reference pixel", &status);
  cdelt = (double)(this->pixelsize) * MAS_TO_DEG;
  // TODO: write in mas in comment?
  fits_write_key(fits, TDOUBLE, "CDELT1", &cdelt,
                 "Coordinate increment per pixel", &status);
  fits_write_key(fits, TDOUBLE, "CDELT2", &cdelt,
                 "Coordinate increment per pixel", &status);
  fits_write_key(fits, TSTRING, "CUNIT1", "deg",
                 "Physical units for CDELT1 and CRVAL1", &status);
  fits_write_key(fits, TSTRING, "CUNIT2", "deg",
                 "Physical units for CDELT2 and CRVAL2", &status);
  fits_write_key(fits, TSTRING, "CTYPE1", "RA", NULL, &status);
  fits_write_key(fits, TSTRING, "CTYPE2", "DEC", NULL, &status);
  fits_write_pix(fits, TFLOAT, fpixel, this->naxis1 * this->naxis2, this->image,
                 &status);
  if (status)
    throwf(FitsException, "Error writing image HDU - %s",
           e4c_fits_error_report(status));
}

/**
 * @memberof GreyImg
 *
 * @exception NullPointerException if @a this or @a filename is NULL.
 */
void GreyImgWritePrimaryTo(GreyImg *this, const char *filename, bool clobber)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");

  fitsfile *fits;
  e4c_using_fits(fits, filename, clobber ? FITS_CLOBBER : FITS_CREATE)
  {
    GreyImgCreateFitsHdu(this, fits);
  }
  // TODO: delete file on exception?
}

/**
 * @memberof GreyImg
 *
 * @exception NullPointerException if @a this or @a filename is NULL.
 */
void GreyImgAppendImageTo(GreyImg *this, const char *filename)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");

  fitsfile *fits;
  e4c_using_fits(fits, filename, FITS_OPEN_READWRITE)
  {
    GreyImgCreateFitsHdu(this, fits);
  }
}

/**
 * Destructor.
 * @memberof GreyImg
 *
 * @param this  GreyImg instance to destroy.
 * @exception NullPointerException if @a this is NULL.
 */
void DestroyGreyImg(GreyImg *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  g_free(this->image);
  g_free(this);
}
