/**
 * @file
 * Implementation of BsmemRunner class.
 *
 * Copyright (C) 2015-2018, 2021, 2022 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "BsmemRunner.h"
#include "BsmemParam.h"
#include "e4c_fitsio.h"

#include <glib.h>
#include <glib/gstdio.h> /* g_rename(), g_rmdir() */

#include <stdlib.h> /* mkdtemp() */
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define e4c_dispose_g_memory(memory, failed) g_free(memory)

#ifndef MSVS
#define FTEST ftest_
#define UVCHK uvchk_
#define OPSET opset_
#define OP1 op1_
#define TROP1 trop1_
#define UAREA uarea_
#define MEINIT meinit_
#define MEMSET memset_
#define MEMGET memget_
#define USETM usetm_
#define UGETM ugetm_
#define MEREST merest_
#define MESAVE mesave_
#define MEMTRQ memtrq_
#define MEM4 mem4_
#define MOVIE4 movie4_
#define MASK4 mask4_
#define MEMEX memex_
#define VMEMEX vmemex_
#define VOPUS vopus_
#define VTROP vtrop_
#define USAVE usave_
#define UREST urest_
#ifdef __stdcall
#undef __stdcall
#endif
#define __stdcall
#endif

static float square(float x);
static void BsmemRunnerSetMemsysPow(BsmemRunner *);
static void BsmemRunnerSetMemsysBis(BsmemRunner *);

// TODO: move to header file?
extern void __stdcall UAREA(int *, int *, int *, int *, int *, int *);
extern void __stdcall MEINIT(void);
extern void __stdcall MEMSET(int *, int *, int *, float *, float *, float *,
                             float *, float *);
extern void __stdcall MEMGET(int *, int *, int *, float *, float *, float *,
                             float *, float *);
extern void __stdcall USETM(float *, int *, float *, int *);
extern void __stdcall UGETM(float *, int *, float *, int *);
extern void __stdcall MEMEX(float *, int *, int *);
extern void __stdcall MEMTRQ(float *, int *, float *);
extern void __stdcall MEREST(void);
extern void __stdcall MESAVE(void);
extern void __stdcall MEM4(float *, int *, float *, float *, float *, float *,
                           float *, float *, float *, float *, float *, float *,
                           float *, float *, int *, int *);
// extern void __stdcall MOVIE4(float *, int *, int *, int *);
// extern void __stdcall MASK4(float *, float *, float *, float *, int *, int
// *);

void __stdcall VMEMEX(float *, float *);
void __stdcall VTROP(float *, float *);
void __stdcall VOPUS(float *, float *);

static USER *user = NULL;

/**
 * Initialise MEMSYS and related data.
 */
static void BsmemRunnerInitUser(BsmemRunner *this)
{
  int level = 10, nrand = 1, iseed = 1234;
  float def = -1.0, aim = 1.0, rate = 1.0, acc = -1.0, utol = 0.1;
  float uscale, vscale;
  int method[4];
  int ncells, nstore;

  /*
   * Initialise global pointer
   */
  user = &this->user;

  /*
   * Map dimensions and parameters
   */
  this->user.iNX = this->initimg->naxis1; // TODO: check order of dims
  this->user.iNY = this->initimg->naxis2;
  this->user.iNUV = this->data->nuv;
  this->user.iNpow = this->data->npow;
  this->user.iNbis = this->data->nbis;
  this->user.xyint = this->initimg->pixelsize;

  /*
   * Allocate MEMSYS storage areas
   */
  this->ndata = this->data->npow + 2 * this->data->nbis;
  ncells = this->user.iNX * this->user.iNY;
  nstore = 6 * ncells + 9 * this->ndata;
  UAREA(&ncells, &ncells, &this->ndata, &nstore, &level, this->kb);
  this->st = g_malloc(nstore * sizeof(this->st[0]));

  /*
   * Assign memory to work spaces
   */
  /* Opus/Tropus */
  this->user.current_visi =
      g_malloc(this->data->nuv * sizeof(this->user.current_visi[0]));
  this->user.current_diffvisi =
      g_malloc(this->data->nuv * sizeof(this->user.current_diffvisi[0]));
  this->user.data_phasor =
      g_malloc(this->data->nbis * sizeof(this->user.data_phasor[0]));
  this->user.UV_point =
      g_malloc(2 * this->data->nuv * sizeof(this->user.UV_point[0]));

  for (int i = 0; i < this->data->nuv; i++)
  {
    this->user.current_visi[i] = 0.0;
    this->user.current_diffvisi[i] = 0.0;
  }

  /*
   * Initialise this->user.UV_point and bispectrum uv references for transforms
   */
  this->user.iNUV = this->data->nuv;
  uscale = 2.0 * (float)(this->user.iNX) * this->user.xyint * MAS;
  vscale = 2.0 * (float)(this->user.iNY) * this->user.xyint * MAS;
  g_debug("uscale=%g", uscale);
  for (int i = 0; i < this->user.iNUV; i++)
  {
    this->user.UV_point[2 * i] = uscale * this->data->uv[i].u;
    this->user.UV_point[2 * i + 1] = vscale * this->data->uv[i].v;
  }
  this->user.data = this->data;

  /* Convert exp(-I*closure phase) to Real/Imaginary parts */
  for (int ii = 0; ii < this->data->nbis; ii++)
    this->user.data_phasor[ii] =
        (cos(this->data->bis[ii].bisphi * M_PI / 180.0) -
         I * sin(this->data->bis[ii].bisphi * M_PI / 180.0));

  /* Set up MEMSYS data area */
  BsmemRunnerSetMemsysPow(this);
  BsmemRunnerSetMemsysBis(this);

  /* Set NFFT transform parameters, including uv points */
  int NN[2], nn[2];
  NN[0] = this->user.iNX;
  nn[0] = 2 * NN[0];
  NN[1] = this->user.iNX;
  nn[1] = 2 * NN[1];
  nfft_init_guru(&this->user.p, 2, NN, this->user.iNUV, nn, 6,
                 (PRE_FULL_PSI | MALLOC_F_HAT | MALLOC_X | MALLOC_F |
                  FFTW_INIT | FFT_OUT_OF_PLACE),
                 FFTW_ESTIMATE | FFTW_DESTROY_INPUT);

  for (int i = 0; i < this->user.iNUV; i++)
  {
    this->user.p.x[2 * i] = this->data->uv[i].v * this->user.xyint * MAS;
    this->user.p.x[2 * i + 1] = this->data->uv[i].u * this->user.xyint * MAS;
  }

  nfft_precompute_one_psi(&this->user.p);

  /*
   * Initialise MEMSYS
   */
  MEINIT();
  method[0] = this->inparam->regularization;
  method[1] = this->inparam->entropy_func;
  method[2] = 1; /* Gaussian noise */
  method[3] = 2; /* for non-linearity */
  if (this->inparam->regularization == 3) aim = this->inparam->alpha;

  MEMSET(method, &nrand, &iseed, &aim, &rate, &def, &acc, &utol);
}

/**
 * Free storage allocated by BsmemRunnerInitUser().
 */
static void BsmemRunnerFreeUser(BsmemRunner *this)
{
  nfft_finalize(&this->user.p);
  g_free(this->st);
  g_free(this->user.current_visi);
  g_free(this->user.current_diffvisi);
  g_free(this->user.data_phasor);
  g_free(this->user.UV_point);
}

static GreyImg *create_initimg(BsmemData *data, float pixelsize, int dim,
                               int modeltype, float modelwidth)
{
  if (pixelsize <= 0.)
  {
    pixelsize = 1.0 / (2.0 * MAS * OVERSAMPLING * data->uv_max);
    g_debug("Pixel size: Automatic, %.4f mas", pixelsize);
  }
  else
  {
    g_debug("Pixel size: User-defined, %.4f mas", pixelsize);
  }
  GreyImg *initimg = CreateGreyImg(INIT_IMG_NAME, dim, dim, pixelsize);
  switch (modeltype)
  {
  case 0:
    GreyImgAddRectangle(initimg, 0, 0, initimg->naxis1, initimg->naxis2, 1.0);
    g_debug("Created flat prior initial image");
    break;
  case 1:
    GreyImgAddDirac(initimg, initimg->naxis1 / 2, initimg->naxis2 / 2, 1.0);
    g_debug("Created centred Dirac delta initial image");
    break;
  case 2:
    GreyImgAddDisk(initimg, initimg->naxis1 / 2, initimg->naxis2 / 2, 1.0,
                   modelwidth / pixelsize);
    g_debug("Created centred %.2f mas diameter circular disk initial image",
            modelwidth);
    break;
  case 3:
    GreyImgAddGaussian(initimg, initimg->naxis1 / 2, initimg->naxis2 / 2, 1.0,
                       modelwidth / pixelsize);
    g_debug("Created centred %.2f mas FWHM circular Gaussian initial image",
            modelwidth);
    break;
  case 4:
    GreyImgAddLorentzian(initimg, initimg->naxis1 / 2, initimg->naxis2 / 2, 1.0,
                         modelwidth / pixelsize);
    g_debug("Created centred %.2f mas FWHM circular Lorentzian initial image",
            modelwidth);
    break;
  default:
    throwf(IllegalArgumentException, "Invalid model type: %d", modeltype);
  }
  return initimg;
}

// TODO: API to set verbose
static BsmemRunner *CreateBsmemRunner(GreyImg *initimg, GreyImg *priorimg,
                                      BsmemParam *inparam, BsmemData *data)
{
  if (initimg == NULL)
    throw(NullPointerException, "Argument 'initimg' is NULL.");
  if (inparam == NULL)
    throw(NullPointerException, "Argument 'inparam' is NULL.");
  if (data == NULL) throw(NullPointerException, "Argument 'data' is NULL.");

  // TODO: check parameters
  if (data->npow == 1 && data->nbis == 0)
    throw(IllegalArgumentException, "Dataset contains no data.");

  BsmemRunner *this = g_malloc(sizeof(*this));
  this->inparam = inparam;
  this->inparam->use_vis =
      USE_NONE; // TODO: warn if complex visibilities in data
  this->data = data;
  this->output = CreateBsmemOutput();
  this->initimg = initimg;
  if (GreyImgSetBlank(this->initimg, 0.0, MOD_BLANK))
    g_message("Negative/zero values in initial image are not permitted. "
              "All have been replaced by %g.",
              MOD_BLANK);
  GreyImgNormalise(this->initimg);
  this->priorimg = priorimg;
  if (this->priorimg != NULL)
  {
    if (GreyImgSetBlank(this->priorimg, 0.0, MOD_BLANK))
      g_message("Negative/zero values in prior image are not permitted. "
                "All have been replaced by %g.",
                MOD_BLANK);
    GreyImgNormalise(this->priorimg);
  }
  this->currentimg = CreateGreyImg("OUTPUT1", initimg->naxis1, initimg->naxis2,
                                   initimg->pixelsize);
  this->verbose = true;
  this->total_iter = 0;

  BsmemRunnerInitUser(this);
  BsmemRunnerRestart(this);

  return this;
}

/**
 * Create BsmemRunner instance from standardised FITS file.
 * @memberof BsmemRunner
 *
 * If file does not contain a prior image, the initial image is used.
 *
 * @param filename  Input FITS filename.
 * @return BsmemRunner *  New BsmemRunner instance.
 * @exception NullPointerException if @a filename is NULL.
 */
BsmemRunner *BsmemRunnerFromInput(const char *filename)
{
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");

  GreyImg *img, *priorimg;
  BsmemParam *inparam;
  BsmemData *data;

  img = GreyImgFromInputFilename(filename, "INIT_IMG");
  try
  {
    priorimg = GreyImgFromInputFilename(filename, "RGL_PRIO");
  }
  catch (FitsException)
  {
    priorimg = NULL;
    g_message("No prior image found, using initial image");
  }
  inparam = BsmemParamFromFilename(filename);
  float uvmax = 1.0 / (2.0 * MAS * OVERSAMPLING * img->pixelsize);
  if (uvmax < inparam->uv_max)
  {
    g_message("Pixel size too large for data");
    inparam->uv_max = uvmax;
  }
  data = BsmemDataFromFilename(filename, inparam);

  BsmemRunner *this = CreateBsmemRunner(img, priorimg, inparam, data);
  return this;
}

/**
 * Create BsmemRunner instance from OIFITS file and initial image object.
 * @memberof BsmemRunner
 *
 * @param filename  Input OIFITS filename.
 * @param param     BSMEM scalar parameters (including data selection).
 * @param initimg   Initial image.
 * @return BsmemRunner *  New BsmemRunner instance.
 * @exception NullPointerException if @a filename, @a param,
 *            or @a initimg is NULL.
 */
BsmemRunner *BsmemRunnerFromImage(const char *filename, const BsmemParam *param,
                                  const GreyImg *initimg)
{
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");
  if (param == NULL) throw(NullPointerException, "Argument 'param' is NULL.");
  if (initimg == NULL)
    throw(NullPointerException, "Argument 'initimg' is NULL.");

  GreyImg *img, *priorimg = NULL;
  BsmemParam *inparam;
  BsmemData *data;

  inparam = BsmemParamCopy(param);
  float uv_max = 1.0 / (2.0 * MAS * OVERSAMPLING * initimg->pixelsize);
  if (uv_max < inparam->uv_max) inparam->uv_max = uv_max;
  data = BsmemDataFromFilename(filename, inparam);
  img = GreyImgCopy(initimg);

  g_assert_cmpint(strlen(img->name), <, FLEN_FILENAME);
  strcpy(inparam->initimgname, img->name);

  BsmemRunner *this = CreateBsmemRunner(img, priorimg, inparam, data);
  return this;
}

/**
 * Create BsmemRunner instance from OIFITS file and initial image parameters.
 * @memberof BsmemRunner
 *
 * @param filename    Input OIFITS filename.
 * @param param       BSMEM scalar parameters (including data selection).
 * @param pixelsize   Initial image pixel size /mas (<= 0. for automatic).
 * @param dim         Initial image dimension /pix.
 * @param modeltype   Initial image model type (0: flat; 1: Dirac;
 *                    2: uniform disk; 3: Gaussian; 4: Lorentzian).
 * @param modelwidth  Initial image model width /mas.
 * @return BsmemRunner *  New BsmemRunner instance.
 * @exception NullPointerException if @a filename or @a param is NULL.
 */
BsmemRunner *BsmemRunnerFromArgs(const char *filename, const BsmemParam *param,
                                 float pixelsize, int dim, int modeltype,
                                 float modelwidth)
{
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");
  if (param == NULL) throw(NullPointerException, "Argument 'param' is NULL.");

  GreyImg *img, *priorimg = NULL;
  BsmemParam *inparam;
  BsmemData *data;

  inparam = BsmemParamCopy(param);
  if (pixelsize > 0.0)
  {
    float uv_max = 1.0 / (2.0 * MAS * OVERSAMPLING * pixelsize);
    if (uv_max < inparam->uv_max) inparam->uv_max = uv_max;
  }

  /* Read data first to choose appropriate pixel size */
  data = BsmemDataFromFilename(filename, inparam);
  img = create_initimg(data, pixelsize, dim, modeltype, modelwidth);

  g_assert_cmpint(strlen(img->name), <, FLEN_FILENAME);
  strcpy(inparam->initimgname, img->name);

  BsmemRunner *this = CreateBsmemRunner(img, priorimg, inparam, data);
  return this;
}

/**
 * @memberof BsmemRunner
 *
 * @exception NullPointerException if @a this is NULL.
 */
void BsmemRunnerPrintData(const BsmemRunner *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  for (int i = 0; i < this->data->npow; i++)
  {
    printf("Pow %03d Amp: %9.6f +_ %8.6f | S/N %4.1f\n", i,
           this->data->pow[i].pow, this->data->pow[i].powerr,
           fabsf(this->data->pow[i].pow / this->data->pow[i].powerr));
  }

  for (int i = 0; i < this->data->nbis; i++)
  {
    printf("Bis %03d %3s: %9.6f +_ %8.6f | S/N %4.1f | Phi: %10.6f +_ %9.6f\n",
           i, this->data->bis[i].fakeamp ? "Fak" : "Amp",
           this->data->bis[i].bisamp, this->data->bis[i].bisamperr,
           fabsf(this->data->bis[i].bisamp / this->data->bis[i].bisamperr),
           this->data->bis[i].bisphi, this->data->bis[i].bisphierr);
  }
}

/**
 * Print parameters to stdout.
 * @memberof BsmemRunner
 *
 * @exception NullPointerException if @a this is NULL.
 */
void BsmemRunnerPrintParam(const BsmemRunner *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  printf("Maximum uv radius:\t%.4g waves\n", this->data->uv_max);
  printf("Array resolution:\t%.4f mas\n",
         1. / (2.0 * MAS * this->data->uv_max));
  printf("Pixel size:\t\t%.4f mas\n", this->initimg->pixelsize);
  printf("Image dimension:\t%ld pixels\n", this->initimg->naxis1);
  printf("Image fov:\t\t%.2f mas\n",
         this->initimg->naxis1 * this->initimg->pixelsize);
  printf("Pix/fastest fringe:\t%.4f\n",
         1.0 / (this->data->uv_max * this->initimg->pixelsize * MAS));

  printf("Entropy functional:\t");
  if (this->inparam->entropy_func == 1)
    printf("Gull-Skilling entropy\n");
  else if (this->inparam->entropy_func == 2)
    printf("Positive/negative Gull-Skilling entropy\n");
  else if (this->inparam->entropy_func == 4)
    printf("Quadratic/L2\n");
  printf("Hyperparameter scheme:\t");
  if (this->inparam->regularization == 1)
    printf("Classic Bayesian\n");
  else if (this->inparam->regularization == 2)
    printf("Classic Bayesian with noise scaling\n");
  else if (this->inparam->regularization == 3)
    printf("Fixed alpha=%f\n", this->inparam->alpha);
  else if (this->inparam->regularization == 4)
    printf("Chi2 = N method\n");
  printf("Maximum n# iterations:\t");
  if (this->inparam->maxiter <= 0)
    printf("Unlimited\n");
  else
    printf("%d\n", this->inparam->maxiter);
  printf("Total flux:\t\t%f\n", this->inparam->flux);
  printf("Error on zero-bas V^2:\t%f\n", this->inparam->fluxerr);
  printf("Initial flux:\t\t%f\n", this->inparam->initflux);
  printf("Use VIS2 data:\t\t%s\n", this->inparam->use_vis2 ? "all" : "none");
  char *use_t3;
  with(use_t3, e4c_dispose_g_memory)
  {
    use_t3 = g_ascii_strdown(useAmpPhiName[this->inparam->use_t3], -1);
  }
  use
  {
    printf("Use T3 data:\t\t%s\n", use_t3);
  }
  char *fake_t3amp;
  with(fake_t3amp, e4c_dispose_g_memory)
  {
    fake_t3amp = g_ascii_strdown(fakeT3AmpName[this->inparam->fake_t3amp], -1);
  }
  use
  {
    printf("Fake T3AMP data:\t%s\n", fake_t3amp);
  }
}

/**
 * @memberof BsmemRunner
 *
 * @exception NullPointerException if @a this is NULL.
 */
void BsmemRunnerPrintState(const BsmemRunner *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  // TODO: print state
}

/**
 * @memberof BsmemRunner
 *
 * @exception NullPointerException if @a this is NULL.
 */
void BsmemRunnerRestart(BsmemRunner *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  for (int i = 0; i < this->user.iNX * this->user.iNY; i++)
    this->st[this->kb[2] + i] =
        (this->inparam->initflux * this->initimg->image[i]);

  /*
   * Set pixel-weighting
   */
  const float *priorimage;
  if (this->priorimg != NULL)
    priorimage = this->priorimg->image;
  else
    priorimage = this->initimg->image;
  for (int i = 0; i < this->user.iNX * this->user.iNY; i++)
    /* kb[0] points at FORTRAN area ST(1): */
    this->st[this->kb[0] + i] =
        priorimage[i]; // TODO: normalise priorimage? Note old bsmem normalised
                       // to initflux

  /*
   * Set V0, VIS etc...
   */
  float *temp = g_malloc(this->ndata * sizeof(temp[0]));
  VMEMEX(this->initimg->image, temp); // TODO: why is this needed?
  g_free(temp);

  float consistency;
  for (int i = 1; i < 5; i++)
  {
    MEMTRQ(&this->st[0], &i, &consistency);
    if (consistency > 1e-5)
      throwf(RuntimeException, "Consistency error %f", consistency);
  }

  this->total_iter = 0;

  /* Set outputs */
  this->output->last_img[0] = '\0';
  this->output->niter = 0;
  this->output->converge = false;
  this->output->chisq = 0.0F;
  this->output->entropy = 0.0F;
  this->output->flux = this->inparam->initflux;
  this->output->alpha = this->inparam->alpha;
}

static void BsmemRunnerPrintIteration(BsmemRunner *this, float entropy,
                                      float chisq, float scale, float plow,
                                      float phigh, float glow, float ghigh,
                                      float omega, float alpha, int istat,
                                      int ntrans)
{
  if (this->verbose)
  {
    printf("Iteration %d Ntrans === %d istat === %d%d%d%d%d%d%d \n",
           this->total_iter, ntrans, istat % 2, istat / 2 % 2, istat / 4 % 2,
           istat / 8 % 2, istat / 16 % 2, istat / 32 % 2, istat / 64 % 2);
    printf("Entropy === %f  Chisq === %f Flux === %f "
           "Alpha === %f Omega === %f \n",
           entropy, chisq / (float)this->ndata, this->user.imflux, alpha,
           omega);
    printf("Logprob === %f Good Measurements === %f Scale === %f \n",
           (plow + phigh) / 2., (glow + ghigh) / 2., scale);
  }
}

/**
 * @memberof BsmemRunner
 *
 * @param this       BsmemRunner instance.
 * @param niter      Number of iterations to do.
 * @param niterdone  Return location for actual number of iterations done,
 *                   or NULL.
 * @return bool  true if reconstruction has converged, false otherwise.
 * @exception NullPointerException if @a this is NULL.
 * @exception RuntimeException if maximum iterations reached.
 */
bool BsmemRunnerIterate(BsmemRunner *this, int niter, int *const niterdone)
{
  int i, stop;
  /* MEM4() inputs */
  int memrun;
  /* MEM4() outputs */
  float entropy, test, chisq, scale;
  float plow, phigh, pdev, glow, ghigh, gdev, omega, alpha;
  int istat, ntrans;

  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  if (this->total_iter == 0)
  {
    memrun = 1; /* initialise a new run */
    MEM4(this->st, &memrun, &entropy, &test, &chisq, &scale, &plow, &phigh,
         &pdev, &glow, &ghigh, &gdev, &omega, &alpha, &istat, &ntrans);
    BsmemRunnerPrintIteration(this, entropy, chisq, scale, plow, phigh, glow,
                              ghigh, omega, alpha, istat, ntrans);
  }

  if (niter < 0)
    niter = 200000; /* iterate until converged or maxiter reached */

  stop = 0;
  i = 0;
  memrun = 2; /* continue the previous run */
  do
  {
    MEM4(this->st, &memrun, &entropy, &test, &chisq, &scale, &plow, &phigh,
         &pdev, &glow, &ghigh, &gdev, &omega, &alpha, &istat, &ntrans);
    this->total_iter++;
    i++;
    BsmemRunnerPrintIteration(this, entropy, chisq, scale, plow, phigh, glow,
                              ghigh, omega, alpha, istat, ntrans);
    if (isnan(omega) > 0)
      throw(RuntimeException, "Omega has been affected by a NaN value. "
                              "Model is probably inadequate.");

    if (((istat % 2) == 0) && ((istat / 2 % 2) == 0))
      stop++;
    else
      stop = 0;
    if (i >= niter) stop = 6;
    if ((this->total_iter >= this->inparam->maxiter) &&
        (this->inparam->maxiter != -1))
      stop = 6;       /* throw exception after updating outputs */
  } while (stop < 5); // TODO: compare with new constant or parameter

  /* Update this->currentimg */
  for (int i = 0; i < this->user.iNX * this->user.iNY; i++)
    this->currentimg->image[i] = this->st[this->kb[0] + i];
  g_snprintf(this->currentimg->name, sizeof(this->currentimg->name), "OUTPUT%d",
             this->total_iter);

  /* Update output parameters */
  g_assert_cmpint(strlen(this->currentimg->name), <,
                  sizeof(this->output->last_img));
  strcpy(this->output->last_img, this->currentimg->name);
  this->output->niter = this->total_iter;
  this->output->converge = ((istat % 2) == 0) && ((istat / 2 % 2) == 0);
  this->output->chisq = chisq / (float)this->ndata;
  this->output->entropy = entropy;
  this->output->flux = this->user.imflux;
  this->output->alpha = alpha;

  /* Update model data */
  BsmemDataSetModel(this->data, this->user.current_visi);

  if (niterdone != NULL) *niterdone = i;

  if ((this->total_iter >= this->inparam->maxiter) &&
      (this->inparam->maxiter != -1))
    throw(RuntimeException, "Maximum iterations has been reached");

  return this->output->converge;
}

/**
 * @memberof BsmemRunner
 *
 * @exception NullPointerException if @a this is NULL.
 */
void BsmemRunnerCentreImage(BsmemRunner *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  // TODO: re-centre current image
}

/**
 * Create temporary directory in same directory as @a filename.
 *
 * @param template  Template for directory name, must end with "XXXXXX".
 * @return char *  Newly allocated string containing the pathname of the
 *                 directory that was created.
 * @exception RuntimeException if the directory part of @a filename
 *                             does not exist.
 */
static char *mktmpdir(const char *filename, const char *template)
{
  char *dirname, *tmpdir;
  with(dirname, e4c_dispose_g_memory)
  {
    dirname = g_path_get_dirname(filename);
  }
  use
  {
    if (!g_file_test(dirname, G_FILE_TEST_EXISTS))
      throwf(RuntimeException, "Output directory '%s' does not exist", dirname);
    tmpdir = g_build_filename(dirname, template, NULL);
    if (mkdtemp(tmpdir) == NULL)
      throwf(RuntimeException, "Failed to create temporary directory '%s': %s",
             tmpdir, strerror(errno));
  }
  return tmpdir;
}

/**
 * Write the current state to the specified FITS file.
 * @memberof BsmemRunner
 *
 * @param this      BsmemRunner instance.
 * @param filename  Output FITS pathname.
 * @param clobber   If true, allow overwrite of existing output file.
 *
 * The current reconstructed image is written to the primary HDU.
 * @exception NullPointerException if @a this or @a filename is NULL.
 * @exception IllegalArgumentException if @a filename starts with '!'.
 *            To allow overwriting of the output file, use the
 *            @a clobber argument.
 * @exception RuntimeException if @a clobber is false and @a filename exists.
 * @exception RuntimeException if the directory part of @a filename
 *                             does not exist.
 */
void BsmemRunnerWriteTo(const BsmemRunner *this, const char *filename,
                        bool clobber)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");
  if (filename == NULL)
    throw(NullPointerException, "Argument 'filename' is NULL.");
  if (filename[0] == '!')
    throw(IllegalArgumentException, "Filename must not start with '!'.");

  if (!clobber && g_file_test(filename, G_FILE_TEST_EXISTS))
    throwf(RuntimeException, "Output file '%s' exists", filename);

  char *tmpdir, *tmpfilename;
  with(tmpdir, e4c_dispose_g_memory)
  {
    tmpdir = mktmpdir(filename, "temp_bsmem_XXXXXX");
  }
  use
  {
    with(tmpfilename, e4c_dispose_g_memory)
    {
      tmpfilename = g_build_filename(tmpdir, "output.fits", NULL);
    }
    use
    {
      GreyImgWritePrimaryTo(this->currentimg, tmpfilename, true);
      GreyImgAppendImageTo(this->initimg, tmpfilename);
      if (this->priorimg != NULL)
        GreyImgAppendImageTo(this->priorimg, tmpfilename);
      BsmemParamAppendTo(this->inparam, tmpfilename);
      BsmemOutputAppendTo(this->output, tmpfilename);
      BsmemDataAppendTo(this->data, tmpfilename);

      if (g_rename(tmpfilename, filename) == -1)
        throwf(RuntimeException, "Failed to rename output file '%s' -> '%s'",
               tmpfilename, filename);
    }
    g_rmdir(tmpdir);
  }
}

/**
 * Destructor.
 * @memberof BsmemRunner
 *
 * @param this  BsmemRunner instance to destroy.
 * @exception NullPointerException if @a this is NULL.
 */
void DestroyBsmemRunner(BsmemRunner *this)
{
  if (this == NULL) throw(NullPointerException, "Argument 'this' is NULL.");

  DestroyGreyImg(this->initimg);
  if (this->priorimg != NULL) DestroyGreyImg(this->priorimg);
  DestroyGreyImg(this->currentimg);

  BsmemRunnerFreeUser(this);
  DestroyBsmemOutput(this->output);
  DestroyBsmemData(this->data);
  DestroyBsmemParam(this->inparam);

  g_free(this);
}

static float square(float x)
{
  return x * x;
}

static void BsmemRunnerSetMemsysPow(BsmemRunner *this)
{
  for (int i = 0; i < this->data->npow; i++)
  {
    this->st[this->kb[20] + i] = this->data->pow[i].pow;
    float err_pow = (this->inparam->v2a * fabs(this->data->pow[i].powerr) +
                     this->inparam->v2b);
    if (err_pow > 0)
    {
      this->st[this->kb[21] + i] = 1.0 / err_pow;
    }
    else
    {
      g_warning("Error on powerspectrum %d <= 0\n", i);
      this->st[this->kb[21] + i] = infinity;
    }
  }
}

static void BsmemRunnerSetMemsysBis(BsmemRunner *this)
{
  for (int i = 0; i < this->data->nbis; i++)
  {
    float err_abs, err_phi, err_rad, err_tan;

    if (this->inparam->biserrtype == 1)
    {
      /* Full elliptic approximation - may need revision */
      if (i == 0) printf("Bispectrum noise:\tFull elliptic approximation \n");
      err_abs = (this->inparam->t3ampa * this->data->bis[i].bisamperr +
                 this->inparam->t3ampb);
      err_phi = (this->inparam->t3phia * this->data->bis[i].bisphierr +
                 this->inparam->t3phib) *
                M_PI / 180.;
      err_rad = sqrt(square(this->data->bis[i].bisamp) *
                         (1. - exp(-square(err_phi))) / 2. +
                     square(err_abs) * (1. - exp(-2. * square(err_phi))) / 2.);
      err_tan = sqrt((square(this->data->bis[i].bisamp) + square(err_abs)) *
                     (1. - exp(-2. * square(err_phi))) / 2.);
    }
    else
    {
      /* Approximation - 1st order */
      if (i == 0)
        printf("Bispectrum noise:\tClassic elliptic approximation \n");
      err_rad = (this->inparam->t3ampa * this->data->bis[i].bisamperr +
                 this->inparam->t3ampb);
      err_tan = (fabs(this->data->bis[i].bisamp *
                      (this->inparam->t3phia * this->data->bis[i].bisphierr +
                       this->inparam->t3phib) *
                      M_PI / 180.0));
    }

    this->st[this->kb[21] + this->data->npow + 2 * i] = 1.0 / err_rad;
    this->st[this->kb[21] + this->data->npow + 1 + 2 * i] = 1.0 / err_tan;

    /* Reset corresponding accuracy to zero if amplitude or phase is missing */
    if (this->data->bis[i].bisamperr > (infinity - 1.0F))
      this->st[this->kb[21] + this->data->npow + 2 * i] = 0.0;
    if (this->data->bis[i].bisphierr > (infinity - 1.0F))
      this->st[this->kb[21] + this->data->npow + 1 + 2 * i] = 0.0;

    /* Set Bispectrum data, rotated by exp(-i*closure_data) */
    if (this->inparam->biserrtype == 1)
    {
      /* Full elliptic approximation - may need revision */
      this->st[this->kb[20] + this->data->npow + 2 * i] =
          (this->data->bis[i].bisamp * (2. - exp(-square(err_phi) / 2.)));
    }
    else
    {
      /* Approximation - 1st order */
      this->st[this->kb[20] + this->data->npow + 2 * i] =
          this->data->bis[i].bisamp;
    }

    this->st[this->kb[20] + this->data->npow + 1 + 2 * i] = 0.0;
  }
}

// TODO: move VMEMEX, VOPUS, VTROP to e.g. bscore.c ?

/**
 * Visible-to-Data transform.
 *
 * Takes present model image and calculates the appropriate non-linear
 * mock data values (powerpectrum and bispectrum points).
 */
void __stdcall VMEMEX(float *image, float *data)
{
  int npow, nbis, nuv;

  npow = user->iNpow;
  nbis = user->iNbis;
  nuv = user->iNUV;

  user->imflux = 0.0;
  for (int ii = 0; ii < user->iNX * user->iNX; ii++)
  {
    user->p.f_hat[ii] = image[ii] + I * 0.0;
    user->imflux += image[ii];
  }
  nfft_trafo(&user->p);

  if (user->imflux > 0.)
    for (int uu = 0; uu < nuv; uu++)
      user->current_visi[uu] = user->p.f[uu]; // / user->imflux;

  /* Powerspectra */
  for (int i = 0; i < npow; i++)
    data[i] = square(cabsf(user->current_visi[i]));

  /* Bispectra */
  for (int i = 0; i < nbis; i++)
  {
    float complex V0ab, V0bc, V0ca, vtemp;
    V0ab = user->current_visi[user->data->bis[i].ab.iuv];
    V0bc = user->current_visi[user->data->bis[i].bc.iuv];
    V0ca = user->current_visi[user->data->bis[i].ca.iuv];
    if (user->data->bis[i].ab.sign < 0) V0ab = conj(V0ab);
    if (user->data->bis[i].bc.sign < 0) V0bc = conj(V0bc);
    if (user->data->bis[i].ca.sign < 0) V0ca = conj(V0ca);

    vtemp = V0ab * V0bc * V0ca * user->data_phasor[i];
    data[npow + 2 * i] = crealf(vtemp);
    data[npow + 2 * i + 1] = cimagf(vtemp);
  }
}

/**
 * Visible-to-Data differential transform
 *
 * Note current_diffvisi is dVisibility, dh is dImage, dd is dData.
 */
void __stdcall VOPUS(float *dh, float *dd)
{
  int npow, nbis, nuv;

  npow = user->iNpow;
  nbis = user->iNbis;
  nuv = user->iNUV;

  for (int ii = 0; ii < user->iNX * user->iNX; ii++)
    user->p.f_hat[ii] = dh[ii] + I * 0.0;
  nfft_trafo(&user->p);

  if (user->imflux > 0.)
    for (int uu = 0; uu < nuv; uu++)
      user->current_diffvisi[uu] = user->p.f[uu]; // / user->imflux;

  /* Powerspectra */
  for (int i = 0; i < npow; i++)
  {
    dd[i] = 2.0 *
            (crealf(user->current_diffvisi[i]) * crealf(user->current_visi[i]) +
             cimagf(user->current_diffvisi[i]) * cimagf(user->current_visi[i]));
  }

  /* Bispectra */
  for (int i = 0; i < nbis; i++)
  {
    float complex V0ab, V0bc, V0ca, vtemp;
    float complex VISab, VISbc, VISca;
    V0ab = user->current_visi[user->data->bis[i].ab.iuv];
    V0bc = user->current_visi[user->data->bis[i].bc.iuv];
    V0ca = user->current_visi[user->data->bis[i].ca.iuv];
    VISab = user->current_diffvisi[user->data->bis[i].ab.iuv];
    VISbc = user->current_diffvisi[user->data->bis[i].bc.iuv];
    VISca = user->current_diffvisi[user->data->bis[i].ca.iuv];

    /* conjugate if baseline in opposite part of uv-plane */
    if (user->data->bis[i].ab.sign < 0)
    {
      V0ab = conj(V0ab);
      VISab = conj(VISab);
    }
    if (user->data->bis[i].bc.sign < 0)
    {
      V0bc = conj(V0bc);
      VISbc = conj(VISbc);
    }
    if (user->data->bis[i].ca.sign < 0)
    {
      V0ca = conj(V0ca);
      VISca = conj(VISca);
    }

    /* calculate differential response */
    vtemp = user->data_phasor[i] *
            (VISab * V0bc * V0ca + VISbc * V0ca * V0ab + VISca * V0ab * V0bc);
    dd[npow + 2 * i] = crealf(vtemp);
    dd[npow + 2 * i + 1] = cimagf(vtemp);
  }
}

/**
 * Data-to-Visible differential transform.
 */
void __stdcall VTROP(float *dh, float *dd)
{
  int npow, nbis, nuv;

  npow = user->iNpow;
  nbis = user->iNbis;
  nuv = user->iNUV;

  for (int i = 0; i < nuv; i++)
    user->current_diffvisi[i] = 0.0;

  /* Powerspectrum contribution */
  for (int i = 0; i < npow; i++)
    user->current_diffvisi[i] += user->current_visi[i] * 2.0 * dd[i];

  /* Bispectrum contribution */
  for (int i = 0; i < nbis; i++)
  {
    int ab_iuv, bc_iuv, ca_iuv;
    float complex V0ab, V0bc, V0ca;
    float complex VISab, VISbc, VISca;
    float complex bs;

    /* store uv references for simplification */
    ab_iuv = user->data->bis[i].ab.iuv;
    bc_iuv = user->data->bis[i].bc.iuv;
    ca_iuv = user->data->bis[i].ca.iuv;

    /* previous visibilities */
    V0ab = conj(user->current_visi[ab_iuv]);
    V0bc = conj(user->current_visi[bc_iuv]);
    V0ca = conj(user->current_visi[ca_iuv]);

    /* conjugate if baseline in opposite part of uv-plane */
    if (user->data->bis[i].ab.sign < 0) V0ab = conj(V0ab);
    if (user->data->bis[i].bc.sign < 0) V0bc = conj(V0bc);
    if (user->data->bis[i].ca.sign < 0) V0ca = conj(V0ca);

    /* input bispectrum differential */
    bs = ((dd[npow + 2 * i] + I * dd[npow + 2 * i + 1]) *
          conj(user->data_phasor[i]));

    /* differential response calculation */
    VISab = bs * V0bc * V0ca;
    VISbc = bs * V0ca * V0ab;
    VISca = bs * V0ab * V0bc;

    /* conjugate if baseline in opposite part of uv-plane */
    if (user->data->bis[i].ab.sign < 0) VISab = conj(VISab);
    if (user->data->bis[i].bc.sign < 0) VISbc = conj(VISbc);
    if (user->data->bis[i].ca.sign < 0) VISca = conj(VISca);

    /* Add to differential response */
    user->current_diffvisi[ab_iuv] += VISab;
    user->current_diffvisi[bc_iuv] += VISbc;
    user->current_diffvisi[ca_iuv] += VISca;
  }

  for (int uu = 0; uu < nuv; uu++)
    user->p.f[uu] = user->current_diffvisi[uu]; // / user->imflux;

  nfft_adjoint(&user->p);
  for (int ii = 0; ii < user->iNX * user->iNX; ii++)
    dh[ii] = crealf(user->p.f_hat[ii]);
}

// USAVE and UREST are needed for linking with MEMSYS library

/**
 * Routine for saving crucial MEMSYS areas and scalars on disk.
 */
void __stdcall USAVE(int *ints, int *nints, float *reals, int *nreals)
{
}

/**
 * Routine for restoring crucial MEMSYS areas and scalars from disk.
 */
void __stdcall UREST(int *ints, int *nints, float *reals, int *nreals)
{
}
