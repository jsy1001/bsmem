/**
 * @file
 * Definition of BsmemParam class.
 *
 * Copyright (C) 2015-2017, 2021-2023 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#ifndef BSMEM_PARAM_H
#define BSMEM_PARAM_H

#include <oifilter.h>
#include <fitsio.h>

#include <stdbool.h>

// TODO: define descriptive comments for parameter keywords

typedef enum
{
  USE_NULL = -1,
  USE_ALL = 0,
  USE_NONE,
  USE_AMP,
  USE_PHI,
  NUM_USE_AMP_PHI

} UseAmpPhi;

extern char *useAmpPhiName[];

typedef enum
{
  FAKE_NULL = -1,
  FAKE_MISSING = 0,
  FAKE_ALL,
  FAKE_NONE,
  NUM_FAKE_T3AMP

} FakeT3Amp;

extern char *fakeT3AmpName[];

/**
 * @class BsmemParam
 * BSMEM input parameters class.
 */
typedef struct
{
  /** @publicsection */

  /*
   * Image references
   */
  /** Pathname of input FITS file */
  char filename[FLEN_FILENAME];
  /** HDUNAME of initial image (INIT_IMG keyword) */
  char initimgname[FLEN_VALUE];
  /** HDUNAME of prior image (RGL_PRIO keyword), optional */
  char priorimgname[FLEN_VALUE];

  /*
   * Data selection parameters
   */
  char target[FLEN_VALUE]; /**< Target object identifier, optional */
  int target_id;           /**< Target object number, -1 if unset */
  float wave_min;          /**< Minimum wavelength to select (in meters) */
  float wave_max;          /**< Maximum wavelength to select (in meters) */
  UseAmpPhi use_vis;       /**< Use complex visibility data if any */
  bool use_vis2;           /**< Use squared visibility data if any */
  UseAmpPhi use_t3;        /**< Use triple product data if any */
  float uv_max; /**< Maximum uv radius to select (in waves), non-std */

  /*
   * Standard parameters
   */
  int maxiter; /**< Maximum number of iterations to run */
  float alpha; /**< Hyperparameter (RGL_WGT keyword), optional */
  float flux;  /**< Assumed total flux */

  /*
   * BSMEM-specific parameters
   */
  float fluxerr;      /**< Error for zero-baseline flux datum */
  float initflux;     /**< Initial image total flux */
  int regularization; /**< MEMSYS regularization hyperparam scheme (1-4) */
  int entropy_func;   /**< MEMSYS entropy function (1, 2, or 4) */
  float v2a;          /**< Squared visibility error factor */
  float t3ampa;       /**< Triple amplitude error factor */
  float t3phia;       /**< Closure phase error factor */
  float v2b;          /**< Squared visibility error offset */
  float t3ampb;       /**< Triple amplitude error offset */
  float t3phib;       /**< Closure phase error offset */
  int biserrtype;
  FakeT3Amp
      fake_t3amp; /**< Triple amplitudes to fake (missing, all, or none) */

} BsmemParam;

/** @cond DOCUMENT_METHODS_AS_FUNCTIONS */
BsmemParam *CreateBsmemParam(void);
BsmemParam *BsmemParamFromFilename(const char *);
BsmemParam *BsmemParamCopy(const BsmemParam *);
void BsmemParamSetFilter(const BsmemParam *, oi_filter_spec *const);
void BsmemParamPrint(const BsmemParam *);
void BsmemParamAppendTo(BsmemParam *, const char *);
void DestroyBsmemParam(BsmemParam *);
/** @endcond */

#endif /* #ifndef BSMEM_PARAM_H */
