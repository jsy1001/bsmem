/**
 * @file
 * Definition of BsmemData class.
 *
 * Copyright (C) 2015-2017, 2021 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#ifndef BSMEM_DATA_H
#define BSMEM_DATA_H

#include "BsmemParam.h"

#include <oifile.h>
#include <fitsio.h>
#include <glib.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <complex.h>

#define infinity 1.0e9

typedef struct
{
  float u;
  float v;

} BsmemUV;

typedef struct
{
  int iuv;
  short sign;

} BsmemUVRef;

typedef struct
{
  float pow;
  float powerr;
  float powmodel;
  BsmemUVRef ab;
  int target_id;
  double mjd;
  double int_time;
  double eff_wave;
  int sta_index[2];      // TODO: associate with arrname
  int corrindx_vis2data; // with corrname
  int extver;
  long irec;
  int iwave;

} BsmemPow;

typedef struct
{
  bool fakeamp;
  float bisamp;
  float bisamperr;
  float bisphi;
  float bisphierr;
  float bisampmodel;
  float bisphimodel;
  BsmemUVRef ab;
  BsmemUVRef bc;
  BsmemUVRef ca;
  int target_id;
  double mjd;
  double int_time;
  double eff_wave;
  int sta_index[3];   // with arrname
  int corrindx_t3amp; // with corrname
  int corrindx_t3phi; // with corrname
  int extver;
  long irec;
  int iwave;

} BsmemBis;

typedef struct
{
  target object;              /**< single target record */
  GList *arrayList;           /**< Linked list of oi_array structs */
  GList *wavelengthList;      /**< Linked list of oi_wavelength structs */
  GList *corrList;            /**< Linked list of oi_corr structs */
  GList *vis2List;            /**< Linked list of oi_vis2 structs */
  GList *t3List;              /**< Linked list of oi_t3 structs */
  GHashTable *arrayHash;      /**< Hash table of oi_array, indexed by ARRNAME */
  GHashTable *wavelengthHash; /**< Hash table of oi_wavelength,
                                   indexed by INSNAME */
  GHashTable *corrHash;       /**< Hash table of oi_corr, indexed by CORRNAME */

} oi_fits_model;

/**
 * @class BsmemData
 * BSMEM UV data class.
 */
typedef struct
{
  /** @publicsection */
  int nuv;
  int npow;
  int nbis;
  BsmemUV *uv;
  BsmemPow *pow;
  BsmemBis *bis;
  float uv_max;
  oi_fits origdata;

  /** @privatesection */
  oi_fits_model outdata;

} BsmemData;

/** @cond DOCUMENT_METHODS_AS_FUNCTIONS */
BsmemData *CreateBsmemData(const BsmemParam *);
BsmemData *BsmemDataFromFilename(const char *, const BsmemParam *);
bool BsmemDataFindUV(const BsmemData *, BsmemUV, float, BsmemUVRef *const);
BsmemUV BsmemDataGetPowAB(const BsmemData *, int);
BsmemUV BsmemDataGetBisAB(const BsmemData *, int);
BsmemUV BsmemDataGetBisBC(const BsmemData *, int);
BsmemUV BsmemDataGetBisCA(const BsmemData *, int);
float BsmemDataCalcModelPow(const BsmemData *, int, const float complex *);
float complex BsmemDataCalcModelBis(const BsmemData *, int,
                                    const float complex *);
void BsmemDataSetModel(BsmemData *, const float complex *);
void BsmemDataWriteTo(BsmemData *, const char *, bool);
void BsmemDataAppendTo(BsmemData *, const char *);
void DestroyBsmemData(BsmemData *);
/** @endcond */

#endif /* #ifndef BSMEM_DATA_H */
