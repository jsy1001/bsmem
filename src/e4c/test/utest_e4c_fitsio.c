/**
 * @file
 * Unit tests of resource handling API for CFITSIO library.
 */

#include "e4c_fitsio.h"
#include <glib.h>
#include <string.h>

#define FILENAME "utest_e4c_fitsio.fits"


typedef struct
{
  int dummy;

} TestFixture;


static void delete_fits(const char *filename)
{
  fitsfile *fits;
  int status = 0;

  fits_open_file(&fits, filename, READWRITE, &status);
  fits_delete_file(fits, &status);
  if (status)
    g_error("Failed to delete FITS file '%s' - CFITSIO error %d",
            FILENAME, status);
}

static void setup_fixture(TestFixture *fix, gconstpointer userData)
{
  fitsfile *fits;
  int status = 0;
  long naxes[2] = {64L, 64L};

  fits_create_file(&fits, "!" FILENAME, &status);
  fits_create_img(fits, BYTE_IMG, 2, naxes, &status);
  fits_close_file(fits, &status);
  if (status)
    g_error("Failed to create test FITS file '%s' - CFITSIO error %d",
            FILENAME, status);
  
  e4c_context_begin(E4C_TRUE);
}

static void teardown_fixture(TestFixture *fix, gconstpointer userData)
{
  e4c_context_end();

  delete_fits(FILENAME);
}

static void test_open_with(TestFixture *fix, gconstpointer userData)
{
  fitsfile *fits;

  with(fits, e4c_dispose_fits)
  {
    int status = 0;
    if (fits_open_file(&fits, FILENAME, READWRITE, &status))
      throwf(FitsOpenException,
             "Failed to open FITS file '%s' - %s",
             FILENAME, e4c_fits_error_report(status));
  }
  use
  {
    int status = 0;
    if (fits_update_key(fits, TSTRING, "TELESCOP", "WHT", NULL,
                        &status))
      throwf(FitsException, "Failed to update header - %s",
             e4c_fits_error_report(status));
  }
}

static void test_open_using(TestFixture *fix, gconstpointer userData)
{
  fitsfile *fits;

  e4c_using_fits(fits, FILENAME, FITS_OPEN_READWRITE)
  {
    // implicit: {int status = 0;
    //            fits_open_file(&fits, FILENAME, READWRITE, &status);} 
    int status = 0;
    if (fits_update_key(fits, TSTRING, "OBSERVER", "E. Hubble", NULL, &status))
      throwf(FitsException, "Failed to update header - %s",
             e4c_fits_error_report(status));
    // implicit: fclose(fits);
  }
}

static void test_create_using(TestFixture *fix, gconstpointer userData)
{
  fitsfile *fits;

  delete_fits(FILENAME);
  e4c_using_fits(fits, FILENAME, FITS_CREATE)
  {
    // implicit: {int status = 0;
    //            fits_create_file(&fits, FILENAME, &status);}
    int status = 0;
    long naxes[2] = {64L, 64L};
    if (fits_create_img(fits, BYTE_IMG, 2, naxes, &status))
      throwf(FitsException,
             "Failed to create image HDU - %s", e4c_fits_error_report(status));
    if (fits_update_key(fits, TSTRING, "OBSERVER", "E. Hubble", NULL, &status))
      throwf(FitsException, "Failed to update header - %s",
             e4c_fits_error_report(status));
    // implicit: fclose(fits);
  }
}

static void test_clobber_using(TestFixture *fix, gconstpointer userData)
{
  fitsfile *fits;

  e4c_using_fits(fits, FILENAME, FITS_CLOBBER)
  {
    // implicit: {int status = 0;
    //            fits_create_file(&fits, "!" FILENAME, &status);}
    int status = 0;
    long naxes[2] = {64L, 64L};
    if (fits_create_img(fits, BYTE_IMG, 2, naxes, &status))
      throwf(FitsException,
             "Failed to create image HDU - %s", e4c_fits_error_report(status));
    if (fits_update_key(fits, TSTRING, "OBSERVER", "E. Hubble", NULL, &status))
      throwf(FitsException, "Failed to update header - %s",
             e4c_fits_error_report(status));
    // implicit: fclose(fits);
  }
}

static void test_error_report(TestFixture *fix, gconstpointer userData)
{
  const int n = 20, status = BAD_HDU_NUM;
  const char msg1[] = "A test message";
  char desc[FLEN_STATUS];
  
  fits_write_errmsg(msg1);
  g_assert_cmpstr(e4c_fits_errstack(), ==, msg1);
  for (int i = 0; i < n; i++)
    fits_write_errmsg(msg1);
  g_assert_cmpint(strlen(e4c_fits_errstack()), ==, n * strlen(msg1) + n - 1);

  fits_write_errmsg(msg1);
  const char *report = e4c_fits_error_report(status);
  g_assert(g_str_has_suffix(report, msg1));
  fits_get_errstatus(status, desc);
  g_assert(strstr(report, desc) != NULL);
}

int main(int argc, char *argv[])
{
  g_test_init(&argc, &argv, NULL);
  
  g_test_add("/bsmem/e4c_fitsio/open_with", TestFixture, NULL,
             setup_fixture, test_open_with, teardown_fixture);
  g_test_add("/bsmem/e4c_fitsio/open_using", TestFixture, NULL,
             setup_fixture, test_open_using, teardown_fixture);
  g_test_add("/bsmem/e4c_fitsio/create_using", TestFixture, NULL,
             setup_fixture, test_create_using, teardown_fixture);
  g_test_add("/bsmem/e4c_fitsio/clobber_using", TestFixture, NULL,
             setup_fixture, test_clobber_using, teardown_fixture);
  g_test_add("/bsmem/e4c_fitsio/error_report", TestFixture, NULL,
             setup_fixture, test_error_report, teardown_fixture);
  
  return g_test_run();
}
