/**
 * @file
 * Definition of resource handling API for CFITSIO library.
 */

#ifndef E4C_FITSIO_H
#define E4C_FITSIO_H

#include "e4c.h"
#include <fitsio.h>
#include <stdbool.h>

typedef enum
{
  FITS_CREATE,
  FITS_CLOBBER,
  FITS_OPEN_READONLY,
  FITS_OPEN_READWRITE

} FitsModeType;


/*
 * acquire function for fits resource, calls fits_open_file()
 */
void e4c_acquire_fits(fitsfile **pfits, const char *filename,
                      FitsModeType mode);

/*
 * dispose function for fits resource, calls fits_close_file()
 */
void e4c_dispose_fits(fitsfile *fits, bool useFailed);

/**
 * @name Resource handling exceptions
 *
 * @{
 */

/**
 * This exception is thrown when a FITS file error occurs.
 *
 * `FitsException` is the general type of exceptions produced by failed FITS
 * operations.
 *
 * @par     Extends:
 *          #InputOutputException
 */
E4C_DECLARE_EXCEPTION(FitsException);

/**
 * This exception is thrown when a FITS file cannot be opened.
 *
 * `FitsOpenException` is thrown by e4c_using_fits() when
 * fits_open_file() does not return zero for whatever reason.
 *
 * @par     Extends:
 *          FitsException
 */
E4C_DECLARE_EXCEPTION(FitsOpenException);

/**
 * This exception is thrown when a FITS file cannot be closed.
 *
 * `FitsCloseException` is thrown by e4c_using_fits() when
 * fits_close_file() does not return zero for whatever reason.
 *
 * @par     Extends:
 *          FitsException
 */
E4C_DECLARE_EXCEPTION(FitsCloseException);

/** @} */

/**
 * @name Convenience macros for acquiring and disposing resources
 *
 * These macros let you acquire and dispose different kinds of resources
 * according to the *dispose pattern*.
 *
 * @{
 */

/**
 * Introduces a block of code with automatic acquisition and disposal of a
 * CFITSIO file pointer.
 *
 * @param   fits
 *          The file pointer to be acquired, used and then disposed
 * @param   filename
 *          The path of the file to be opened
 * @param   iomode
 *          The access mode for the file (FITS_CREATE, FITS_CLOBBER,
            FITS_OPEN_READONLY or FITS_OPEN_READWRITE)
 *
 * This macro lets you acquire and dispose (open and close) files according to
 * the *dispose pattern*:
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
 *   fitsfile *fits;
 *
 *   e4c_using_fits(fits, FILENAME, FITS_OPEN_READWRITE)
 *   {
 *     // implicit: {int status = 0;
 *     //            fits_open_file(&fits, FILENAME, READWRITE, &status);}
 *     int status = 0;
 *     fits_update_key(fits, TSTRING, "OBSERVER", "E. Hubble", NULL, &status);
 *     // implicit: fclose(fits);
 *   }
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * @exception  FitsOpenException
 *             If fits_open_file() does not return zero,
 *             or an argument is invalid.
 * @exception  FitsCloseException
 *             If fits_close_file() does not return zero.
 *
 */
#define e4c_using_fits(fits, filename, iomode)                          \
  E4C_WITH(fits, e4c_dispose_fits)                                      \
  {                                                                     \
    e4c_acquire_fits(&fits, filename, iomode);                          \
  }                                                                     \
  E4C_USE

/** @} */

const char *e4c_fits_error_report(int);
const char *e4c_fits_errstack(void);

#endif  /* #ifndef E4C_FITSIO_H */
