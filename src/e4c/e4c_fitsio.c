/**
 * @file
 * Implementation of resource handling API for CFITSIO file pointers.
 */

#include "e4c_fitsio.h"
#include <string.h>

#define MAX_ERRSTACK 20

E4C_DEFINE_EXCEPTION(FitsException, "FITS file error.", InputOutputException);
E4C_DEFINE_EXCEPTION(FitsOpenException, "Error opening FITS file",
                     FitsException);
E4C_DEFINE_EXCEPTION(FitsCloseException, "Error closing FITS file.",
                     FitsException);

void e4c_acquire_fits(fitsfile **pfits, const char *filename, FitsModeType mode)
{
  if (pfits == NULL)
    throw(NullPointerException, NULL);

  char filename2[FLEN_FILENAME];
  int status = 0;
  switch (mode)
  {
  case FITS_CREATE:
    fits_create_file(pfits, filename, &status);
    break;
  case FITS_CLOBBER:
    filename2[0] = '!';
    if (strlen(filename) > sizeof(filename2) - 2)  /* initial ! and final \0 */
      throw(FitsOpenException, "Filename is too long.");
    strcpy(&filename2[1], filename);
    fits_create_file(pfits, filename2, &status);
    break;
  case FITS_OPEN_READONLY:
    fits_open_file(pfits, filename, READONLY, &status);
    break;
  case FITS_OPEN_READWRITE:
    fits_open_file(pfits, filename, READWRITE, &status);
    break;
  default:
    throw(FitsOpenException, "Invalid value for 'mode' argument.");
  }
  if (status)
    throwf(FitsOpenException,
           "Failed to create/open FITS file '%s' - %s",
           filename, e4c_fits_error_report(status));
}

void e4c_dispose_fits(fitsfile *fits, bool useFailed)
{
  if (fits == NULL)
    throw(NullPointerException, NULL);

  int status = 0;
  if (fits_close_file(fits, &status))
    throwf(FitsCloseException,
           "Failed to close FITS file - %s", e4c_fits_error_report(status));
}

static void get_errstack(char *dest, int nmax)
{
  int end = 0, count = 0;
  char msg[FLEN_ERRMSG];

  dest[0] = '\0';
  while (fits_read_errmsg(msg) != 0 && ++count <= nmax)
  {
    int len = strlen(msg);
    strncpy(&dest[end], msg, len);
    dest[end + len] = '\n';
    end += (len + 1);
  }
  if (count > 0)
    dest[end - 1] = '\0';  /* remove final \n */
  fits_clear_errmsg(); /* clear any messages past nmax */
}

const char *e4c_fits_error_report(int status)
{
  static char report[30 + FLEN_STATUS + MAX_ERRSTACK * FLEN_ERRMSG];
  char desc[FLEN_STATUS];
  int end;

  fits_get_errstatus(status, desc);
  end = snprintf(report, sizeof(report),
                 "CFITSIO error %d: %s\n", status, desc);
  get_errstack(&report[end], MAX_ERRSTACK);
  return report;
}

const char *e4c_fits_errstack(void)
{
  static char errstack[MAX_ERRSTACK * FLEN_ERRMSG];

  get_errstack(errstack, MAX_ERRSTACK);
  return errstack;
}
