/**
 * @file
 * Implementation of data/model plotting API.
 *
 * Copyright (C) 2015-2018, 2023-2024 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "plot.h"

#include <cpgplot.h>

int iduv = -1, idimage = -1, idpow = -1, idt3amp = -1, idt3phi = -1;

/**
 * Open plot devices.
 */
void openplot(const GreyImg *initimg, const char *dev)
{
  if (initimg == NULL)
    throw(NullPointerException, "Argument 'initimg' is NULL.");
  if (dev == NULL) throw(NullPointerException, "Argument 'dev' is NULL.");

  const float xmax = initimg->naxis1 * initimg->pixelsize;
  const float ymax = initimg->naxis2 * initimg->pixelsize;

  iduv = cpgopen(dev);
  if (iduv <= 0)
    throwf(RuntimeException,
           "Failed to open plot device: cpgopen() returned %d.", iduv);
  cpgask(0);

  idimage = cpgopen(dev);
  if (idimage <= 0)
    throwf(RuntimeException,
           "Failed to open plot device: cpgopen() returned %d.", idimage);
  cpgask(0);
  cpgenv(0.0, xmax, 0.0, ymax, 1, -2);

  idpow = cpgopen(dev);
  if (idpow <= 0)
    throwf(RuntimeException,
           "Failed to open plot device: cpgopen() returned %d.", idpow);
  cpgask(0);

  idt3amp = cpgopen(dev);
  if (idt3amp <= 0)
    throwf(RuntimeException,
           "Failed to open plot device: cpgopen() returned %d.", idt3amp);
  cpgask(0);

  idt3phi = cpgopen(dev);
  if (idt3phi <= 0)
    throwf(RuntimeException,
           "Failed to open plot device: cpgopen() returned %d.", idt3phi);
  cpgask(0);
}

static void getminmax(const float *image, int nx, int ny, float *const min,
                      float *const max)
{
  *min = 1e8;
  *max = -1e8;
  for (int i = 0; i < nx * ny; i++)
  {
    if (*max < image[i]) *max = image[i];
    if (*min > image[i]) *min = image[i];
  }
}

/**
 * Plot image.
 */
void plotimage(const GreyImg *img, float displaypower, bool contour)
{
  if (img == NULL) throw(NullPointerException, "Argument 'img' is NULL.");
  if (idimage <= 0) throw(RuntimeException, "Plot device is not open.");

  float tr[6];
  tr[0] = -1.0 * img->pixelsize / 2.0;
  tr[1] = img->pixelsize;
  tr[2] = 0.0;
  tr[3] = -1.0 * img->pixelsize / 2.0;
  tr[4] = 0.0;
  tr[5] = img->pixelsize;

  float *dispimg = malloc(img->naxis1 * img->naxis2 * sizeof(img[0]));

  /* Flip + power calculation */
  for (int i = 0; i < img->naxis1; i++)
    for (int j = 0; j < img->naxis2; j++)
      dispimg[i * img->naxis2 + img->naxis1 - j - 1] =
          powf(fabs(img->image[i * img->naxis2 + j]), displaypower);

  float min, max;
  getminmax(dispimg, img->naxis1, img->naxis2, &min, &max);

  cpgslct(idimage);
  cpgsci(1);
  cpggray(dispimg, img->naxis1, img->naxis2, 1, img->naxis1, 1, img->naxis2,
          min, max, tr);
  cpgbox("BCITN", 0.0, 0, "BCITN", 0.0, 0);
  cpglab("E-W (mas)", "S-N (mas)", "");
  float C[14];
  if (contour)
  {
    cpgsci(2);
    C[0] = max / 100.0;
    for (int i = 1; i < 7; i++)
      C[i] = C[i - 1] * 2.0;
    cpgcont(dispimg, img->naxis1, img->naxis2, 1, img->naxis1, 1, img->naxis2,
            C, 7, tr);
  }
  free(dispimg);
}

/**
 * Plot UV coordinates.
 */
void plotuv(const BsmemData *data)
{
  if (iduv <= 0) throw(RuntimeException, "Plot device is not open.");

  float max;
  char ulabel[20], vlabel[20];

  max = 0.0;
  for (int i = 0; i < data->nuv; i++)
  {
    if (max < data->uv[i].u) max = data->uv[i].u;
    if (max < data->uv[i].v) max = data->uv[i].v;
  }

  /* Setup plot axes */
  cpgslct(iduv);
  cpgsci(1);
  cpgenv(-1.1, 1.1, -1.1, 1.1, 1, 1);
  g_snprintf(ulabel, sizeof(ulabel), "U /%.1e waves", max);
  g_snprintf(vlabel, sizeof(vlabel), "V /%.1e waves", max);
  cpglab(ulabel, vlabel, "UV COVERAGE");
  cpgsci(4);
  cpgsch(1.0);
  cpgpt1(-1.0, 1.0, 0);
  cpgptxt(-1.0, (1.0 - 0.02), 0.0, -0.05, "powerspectrum");
  cpgsch(1.5);

  /* Plot all powerspectrum UV points */
  for (int i = 0; i < data->npow; i++)
  {
    float u, v;
    u = data->uv[i].u / max;
    v = data->uv[i].v / max;
    cpgpt1(u, v, 0);
    cpgpt1(-u, -v, 0);
  }

  /* Plot all bispectrum UV points */
  cpgsci(2);
  cpgsch(1.0);
  cpgpt1(-1.0, (1.0 - 0.07), 2);
  cpgptxt(-1.0, (1.0 - 0.02 - 0.07), 0.0, -0.05, "bispectrum");
  cpgsch(1.2);
  for (int i = 0; i < data->nbis; i++)
  {
    BsmemUV ab, bc, ca;
    ab = BsmemDataGetBisAB(data, i);
    bc = BsmemDataGetBisBC(data, i);
    ca = BsmemDataGetBisCA(data, i);
    cpgpt1(ab.u / max, ab.v / max, 2);
    cpgpt1(-ab.u / max, -ab.v / max, 2);
    cpgpt1(bc.u / max, bc.v / max, 2);
    cpgpt1(-bc.u / max, -bc.v / max, 2);
    cpgpt1(ca.u / max, ca.v / max, 2);
    cpgpt1(-ca.u / max, -ca.v / max, 2);
  }
}

/**
 * Plot powerspectra.
 */
void plotpow(const BsmemData *data, const BsmemParam *inparam)
{
  if (data == NULL) throw(NullPointerException, "Argument 'data' is NULL.");
  if (inparam == NULL) throw(NullPointerException, "Argument 'inparam' is NULL.");

  if (data->npow > 0)
  {
    if (idpow <= 0) throw(RuntimeException, "Plot device is not open.");
    cpgslct(idpow);
    cpgsci(1);

    float max = 0.0;
    float min = 1e30;
    float r, errlow, errhi;

    for (int i = 0; i < data->npow; i++)
    {
      r = sqrt(data->uv[i].u * data->uv[i].u + data->uv[i].v * data->uv[i].v);
      if (max < r) max = r;
      if (min > r) min = r;
    }
    cpgenv(min, max, 0., data->pow[0].pow, 0, 1);
    cpglab("Spatial frequency (waves)", "Powerspectrum", "Powerspectrum");
    for (int i = 0; i < data->npow; i++)
    {
      BsmemUV ab = BsmemDataGetPowAB(data, i);
      r = sqrt(ab.u * ab.u + ab.v * ab.v);
      cpgsci(2);
      cpgpt1(r, data->pow[i].pow, 0);
      float err_pow = (inparam->v2a * fabs(data->pow[i].powerr) + inparam->v2b);
      errlow = data->pow[i].pow - err_pow;
      errhi = data->pow[i].pow + err_pow;
      cpgerry(1, &r, &errlow, &errhi, 1.0);
      cpgsci(4);
      cpgpt1(r, data->pow[i].powmodel, 4);
    }
  }
}

/**
 * Plot bispectra.
 */
void plott3(const BsmemData *data, const BsmemParam *inparam)
{
  if (data == NULL) throw(NullPointerException, "Argument 'data' is NULL.");
  if (inparam == NULL) throw(NullPointerException, "Argument 'inparam' is NULL.");

  if (data->nbis > 0)
  {
    if (idt3amp <= 0 || idt3phi <= 0)
      throw(RuntimeException, "Plot device is not open.");

    float max = 0.0;
    float min = 1e30;
    float phimax = -190.0;
    float phimin = 190.0;
    float ampmax = 0.0;
    float ampmin = 1e30;

    for (int i = 0; i < data->nbis; i++)
    {
      BsmemUV ab, bc, ca;
      float r, err_amp, err_phi;
      ab = BsmemDataGetBisAB(data, i);
      bc = BsmemDataGetBisBC(data, i);
      ca = BsmemDataGetBisCA(data, i);
      r = sqrt(ab.u * ab.u + ab.v * ab.v + bc.u * bc.u + bc.v * bc.v +
               ca.u * ca.u + ca.v * ca.v);
      if (max < r) max = r;
      if (min > r) min = r;

      err_phi = inparam->t3phia * data->bis[i].bisphierr + inparam->t3phib;
      if (phimax < (data->bis[i].bisphi + err_phi))
        phimax = data->bis[i].bisphi + err_phi;
      if (phimin > (data->bis[i].bisphi - err_phi))
        phimin = data->bis[i].bisphi - err_phi;

      err_amp = inparam->t3ampa * data->bis[i].bisamperr + inparam->t3ampb;
      if (ampmax < (data->bis[i].bisamp + err_amp))
        ampmax = data->bis[i].bisamp + err_amp;
      if (ampmin > (data->bis[i].bisamp - err_amp))
        ampmin = data->bis[i].bisamp - err_amp;
    }

    cpgslct(idt3phi);
    cpgsci(1);
    cpgenv(min, max, phimin, phimax, 0, 1);
    cpglab("Spatial frequency (waves)", "Closure phase", "Closure phase");

    cpgslct(idt3amp);
    cpgsci(1);
    cpgenv(min, max, ampmin, ampmax, 0, 1);
    cpglab("Spatial frequency (waves)", "Triple amplitude", "Triple amplitude");

    for (int i = 0; i < data->nbis; i++)
    {
      BsmemUV ab, bc, ca;
      float r, err_amp, err_phi, errlow, errhi;
      ab = BsmemDataGetBisAB(data, i);
      bc = BsmemDataGetBisBC(data, i);
      ca = BsmemDataGetBisCA(data, i);
      r = sqrt(ab.u * ab.u + ab.v * ab.v + bc.u * bc.u + bc.v * bc.v +
               ca.u * ca.u + ca.v * ca.v);
      cpgslct(idt3phi);
      cpgsci(2);
      cpgpt1(r, data->bis[i].bisphi, 0);
      err_phi = inparam->t3phia * data->bis[i].bisphierr + inparam->t3phib;
      errlow = data->bis[i].bisphi - err_phi;
      errhi = data->bis[i].bisphi + err_phi;
      cpgerry(1, &r, &errlow, &errhi, 1.0);

      cpgslct(idt3amp);
      cpgsci(2);
      cpgpt1(r, data->bis[i].bisamp, 0);
      err_amp = inparam->t3ampa * data->bis[i].bisamperr + inparam->t3ampb;
      errlow = data->bis[i].bisamp - err_amp;
      errhi = data->bis[i].bisamp + err_amp;
      cpgerry(1, &r, &errlow, &errhi, 1.0);

      cpgslct(idt3phi);
      cpgsci(4);
      cpgpt1(r, data->bis[i].bisphimodel, 4);
      cpgslct(idt3amp);
      cpgsci(4);
      cpgpt1(r, data->bis[i].bisampmodel, 4);
    }
  }
}

void closeplot(void)
{
  cpgslct(iduv);
  cpgclos();
  cpgslct(idimage);
  cpgclos();
  cpgslct(idpow);
  cpgclos();
  cpgslct(idt3amp);
  cpgclos();
  cpgslct(idt3phi);
  cpgclos();
}
