/**
 * @file
 * BSMEM application with classic command-line interface.
 *
 * Copyright (C) 2015-2018, 2022-2024 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "bsmem-config.h"
#include "BsmemParam.h"
#include "GreyImg.h"
#include "BsmemRunner.h"
#include "plot.h"
#include "e4c_fitsio.h"
#include "cmdline.h"

#include <glib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
  int noui;
  int clobber;
  char dataFilename[FLEN_FILENAME];
  char outFilename[FLEN_FILENAME];
  char initimgFilename[FLEN_FILENAME];
  float modelwidth;
  int modeltype;
  float pixelsize;
  int dim;
  BsmemParam *param;

} BsmemClassicOptions;

/**
 * Run BSMEM with options pointed to by @a pOpt.
 *
 * @exception FitsException if initial image dimensions are invalid
 */
static void run_bsmem(const BsmemClassicOptions *pOpt)
{
  BsmemRunner *runner = NULL;

  if (pOpt->initimgFilename[0] != '\0')
  {
    /* note any WCS keywords in the FITS file are silently ignored */
    GreyImg *img =
        GreyImgFromImageFilenameNoWCS(pOpt->initimgFilename, pOpt->pixelsize);
    /* Implement old behaviour: '-w' value or default width must
       match FITS image dimensions */
    if (img->naxis1 != pOpt->dim || img->naxis2 != pOpt->dim)
      throwf(FitsException,
             "Size of initial image in '%s' (%d x %d) "
             "doesn't match expected size (%d x %d)",
             pOpt->initimgFilename, img->naxis1, img->naxis2, pOpt->dim,
             pOpt->dim);
    runner = BsmemRunnerFromImage(pOpt->dataFilename, pOpt->param, img);
    DestroyGreyImg(img);
  }
  else
  {
    runner =
        BsmemRunnerFromArgs(pOpt->dataFilename, pOpt->param, pOpt->pixelsize,
                            pOpt->dim, pOpt->modeltype, pOpt->modelwidth);
  }
  BsmemRunnerPrintParam(runner);
  if (!pOpt->noui)
  {
    printf("\nThis version of BSMEM does not accept commands.\n");
    printf("Press ENTER to enable redisplay and iterate until converged "
           "[REDISP ON; DO -1].\n");
    char line[80];
    fgets(line, sizeof(line), stdin);
    openplot(runner->initimg, "/xserve");
    plotuv(runner->data);
  }

  while (!BsmemRunnerIterate(runner, 1, NULL))
  {
    if (!pOpt->noui)
    {
      plotimage(runner->currentimg, 1.0, true);
      plotpow(runner->data, runner->inparam);
      plott3(runner->data, runner->inparam);
    }
    BsmemRunnerWriteTo(runner, pOpt->outFilename, true);
  }
  BsmemRunnerWriteTo(runner, pOpt->outFilename, true);
  DestroyBsmemRunner(runner);
  if (!pOpt->noui)
  {
    printf("Press ENTER to finish.\n");
    char line[80];
    fgets(line, sizeof(line), stdin);
    closeplot();
  }
}

/**
 * Duplicate @a arg, converting abbreviated long options.
 *
 * Any multi-character option prefixed by "-" has its prefix changed
 * to "--" for compatibility with GNU-compliant parsers.
 */
static char *convert_arg(const char *arg)
{
  if (strlen(arg) > 2 && g_str_has_prefix(arg, "-") &&
      !g_str_has_prefix(arg, "--"))
    return g_strdup_printf("-%s", arg);
  else
    return g_strdup(arg);
}

/**
 * Convert enum_use_t3 to UseAmpPhi.
 */
static UseAmpPhi convert_use_t3(enum enum_use_t3 arg)
{
  switch (arg)
  {
  case use_t3_arg_all:
    return USE_ALL;
  case use_t3_arg_none:
    return USE_NONE;
  case use_t3_arg_amp:
    return USE_AMP;
  case use_t3_arg_phi:
    return USE_PHI;
  default:
    return USE_NULL;
  }
}

/**
 * Convert enum_fake_t3amp to FakeT3Amp.
 */
static FakeT3Amp convert_fake_t3amp(enum enum_fake_t3amp arg)
{
  switch (arg)
  {
  case fake_t3amp_arg_missing:
    return FAKE_MISSING;
  case fake_t3amp_arg_all:
    return FAKE_ALL;
  case fake_t3amp_arg_none:
    return FAKE_NONE;
  default:
    return FAKE_NULL;
  }
}

/**
 * Classic bsmem main function.
 */
int main(int argc, char *argv[])
{
  char **argv2 = NULL;
  int ret = 1;

  argv2 = g_malloc(argc * sizeof(argv[0]));
  for (int i = 0; i < argc; i++)
    argv2[i] = convert_arg(argv[i]);

  struct gengetopt_args_info args_info;
  if (cmdline_parser(argc, argv2, &args_info) != 0) goto cleanup;

  if (args_info.version_given)
  {
    printf("%s %s\n", CMDLINE_PARSER_PACKAGE_NAME, bsmem_VERSION);
    ret = 0;
    goto cleanup;
  }

  BsmemClassicOptions opt;
  opt.noui = args_info.noui_flag;
  opt.clobber = args_info.clobber_flag;
  g_strlcpy(opt.dataFilename, args_info.data_arg, FLEN_FILENAME);
  g_strlcpy(opt.outFilename, args_info.output_arg, FLEN_FILENAME);
  if (args_info.sf_given)
    g_strlcpy(opt.initimgFilename, args_info.sf_arg, FLEN_FILENAME);
  else
    opt.initimgFilename[0] = '\0';
  opt.modelwidth = args_info.mw_arg;
  opt.modeltype = args_info.mt_arg;
  if (args_info.pixelsize_given)
    opt.pixelsize = args_info.pixelsize_arg;
  else
    opt.pixelsize = -1.0F; /* automatic */
  opt.dim = args_info.dim_arg;
  opt.param = CreateBsmemParam();
  if (args_info.target_given)
    g_strlcpy(opt.param->target, args_info.target_arg, FLEN_VALUE);
  opt.param->wave_min = args_info.wavmin_arg / 1E9;
  opt.param->wave_max = args_info.wavmax_arg / 1E9;
  opt.param->use_vis2 = (args_info.use_vis2_arg == use_vis2_arg_all);
  opt.param->use_t3 = convert_use_t3(args_info.use_t3_arg);
  opt.param->uv_max = args_info.uvmax_arg;
  opt.param->maxiter = args_info.it_arg;
  opt.param->alpha = args_info.alpha_arg;
  opt.param->flux = args_info.flux_arg;
  opt.param->fluxerr = args_info.ferr_arg;
  opt.param->initflux = args_info.mf_arg;
  opt.param->regularization = args_info.autoalpha_arg;
  opt.param->entropy_func = args_info.regul_arg;
  opt.param->v2a = args_info.v2a_arg;
  opt.param->v2b = args_info.v2b_arg;
  opt.param->t3ampa = args_info.t3ampa_arg;
  opt.param->t3ampb = args_info.t3ampb_arg;
  opt.param->t3phia = args_info.t3phia_arg;
  opt.param->t3phib = args_info.t3phib_arg;
  opt.param->biserrtype = args_info.berr_arg;
  opt.param->fake_t3amp = convert_fake_t3amp(args_info.fake_t3amp_arg);

  if (!opt.clobber && g_file_test(opt.outFilename, G_FILE_TEST_EXISTS))
  {
    printf("Output file '%s' exists and '--clobber' not specified "
           "-> Exiting...\n",
           opt.outFilename);
  }
  else
  {
    e4c_using_context(E4C_FALSE)
    {
      try
      {
        run_bsmem(&opt);
        ret = 0;
      }
      catch (RuntimeException)
      {
        const e4c_exception *exception = e4c_get_exception();
        printf("\n*** Image reconstruction failed ***\n");
        printf("%s: %s\n\n", exception->name, exception->message);
      }
    }
  }
  DestroyBsmemParam(opt.param);
  /*FALLTHROUGH*/

cleanup:
  if (argv2)
  {
    for (int i = 0; i < argc; i++)
      g_free(argv2[i]);
    g_free(argv2);
  }
  cmdline_parser_free(&args_info);
  return ret;
}
