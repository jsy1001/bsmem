/**
 * @file
 * BsmemRunner test with plots.
 *
 * Copyright (C) 2015-2017, 2024 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#include "BsmemRunner.h"
#include "plot.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  if (argc != 3)
  {
    fprintf(stderr, "Usage: test_BsmemRunner INPUT OUTPUT\n");
    exit(2);
  }

  e4c_using_context(E4C_FALSE)
  {
    BsmemRunner *runner = BsmemRunnerFromInput(argv[1]);
    BsmemRunnerPrintParam(runner);
    openplot(runner->initimg, "/xserv");
    plotuv(runner->data);
    while (!BsmemRunnerIterate(runner, 1, NULL))
    {
      plotimage(runner->currentimg, 1.0, true);
      plotpow(runner->data, runner->inparam);
      plott3(runner->data, runner->inparam);
      BsmemRunnerWriteTo(runner, argv[2], true);
    }
    BsmemRunnerWriteTo(runner, argv[2], true);
    DestroyBsmemRunner(runner);
    closeplot();
  }
}
