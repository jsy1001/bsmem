/**
 * @file
 * Unit tests of plotting API.
 */

#include "plot.h"
#include <cpgplot.h>
#include <glib.h>

#define IMAGE_FILENAME "utest_GreyImg_in.fits"

typedef struct
{
  int dummy;

} TestFixture;

static void setup_fixture(TestFixture *fix, gconstpointer userData)
{
}

static void teardown_fixture(TestFixture *fix, gconstpointer userData)
{
}

static void test_cpgopen(TestFixture *fix, gconstpointer userData)
{
  int id = -1;
  id = cpgopen("/XSERVE");
  g_assert_cmpint(id, >, 0);
  cpgend();
}

static void test_openplot(TestFixture *fix, gconstpointer userData)
{
  /* Note that this test fails when passing E4C_TRUE and pgxwin_server
   * is not already running */
  e4c_using_context(E4C_FALSE)
  {
    GreyImg *img = GreyImgFromImageFilename(IMAGE_FILENAME);
    openplot(img, "/XSERVE");
    closeplot();
    DestroyGreyImg(img);
  }
}

int main(int argc, char *argv[])
{
  g_test_init(&argc, &argv, NULL);

  g_test_add("/bsmem/plot/cpgopen", TestFixture, NULL, setup_fixture,
             test_cpgopen, teardown_fixture);
  g_test_add("/bsmem/plot/openplot", TestFixture, NULL, setup_fixture,
             test_openplot, teardown_fixture);

  return g_test_run();
}
