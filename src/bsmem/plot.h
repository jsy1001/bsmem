/**
 * @file
 * Definition of data/model plotting API.
 *
 * Copyright (C) 2015-2017, 2024 John Young
 *
 * This file is part of BSMEM:
 * https://www.astro.phy.cam.ac.uk/research/ResearchFacilities/software-for-astrophyiscs/bsmem/
 *
 * BSMEM is licensed under a Creative Commons
 * Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * See LICENSE.txt for more details.
 */

#ifndef PLOT_H
#define PLOT_H

#include "GreyImg.h"
#include "BsmemData.h"
#include "BsmemParam.h"

void openplot(const GreyImg *, const char *);
void plotimage(const GreyImg *, float, bool);
void plotuv(const BsmemData *);
void plotpow(const BsmemData *, const BsmemParam *);
void plott3(const BsmemData *, const BsmemParam *);
void closeplot(void);

#endif /* #ifndef PLOT_H */
