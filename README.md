# BSMEM

This is BSMEM, the Cambridge software package for image reconstruction
from optical interferometry data. BSMEM applies a fully Bayesian
approach to the inverse problem of finding the most probable image
given the evidence. BSMEM makes use of the MemSys library created by
Maximum Entropy Data Consultants Ltd. to implement a gradient descent
algorithm for maximising the inference (posterior probability) of an
image, using the entropy of the reconstructed image as the prior
probability.

There are two distinct BSMEM applications:

- `bsmem`: the classic console-mode application.

- `bsmem-ci`: a program that implements the common image reconstruction
  interface described at <https://github.com/JMMC-OpenDev/OI-Imaging-JRA/>.

## Licensing

Since 2017, BSMEM is licensed under a Creative Commons
Attribution-NonCommercial-NoDerivatives 4.0 International License:
<https://creativecommons.org/licenses/by-nc-nd/4.0/>

BSMEM incorporates the MemSys4 MEM kernel by Maximum Entropy Data
Consultants Limited.

BSMEM uses the exceptions4c library by Guillermo Calvo
(<https://github.com/guillermocalvo/exceptions4c>). This library is
distributed with BSMEM and is licensed under the LGPL v3 - see
src/e4c/COPYING.lesser for details.

## Installation

BSMEM has successfully been built on Linux (Ubuntu 14.04, kernel 4.4.0
and CentOS release 6.6, kernel 2.6.32) and OS X (10.9 Mavericks). Any
recent Linux distribution or OS X version should work. Building on
Solaris is no longer supported.

### Requirements

The following libraries must be installed on your system:

1. PGPLOT 5.2 (<http://www.astro.caltech.edu/~tjp/pgplot/>): with at
   least XTERM, PS, and XSERVE drivers activated in the drivers.list
   file.  On Redhat/Fedora Linux install the pgplot-devel package
   (from rpmfusion); on Debian/Ubuntu install the pgplot5 package. If
   using MacPorts on OS X install the pgplot port.

   As an alternative to PGPLOT, you can use Giza
   (<https://danieljprice.github.io/giza/>) as a drop-in replacement.

   If neither PGPLOT nor Giza are installed, only the `bsmem-ci` program will be
   built.

2. CFITSIO 3.x (<http://heasarc.gsfc.nasa.gov/fitsio/>)

   On Redhat/Fedora install the cfitsio-devel package; on
   Debian/Ubuntu install the libcfitsio-dev package. If using
   MacPorts install the cfitsio port.

3. FFTW 3.x  (<http://www.fftw.org/>)

   On Redhat/Fedora install the fftw-devel package; on Debian/Ubuntu
   install the libfftw3-dev package. If using MacPorts install the
   fftw-3 port.

4. NFFT 3.x (<http://www-user.tu-chemnitz.de/~potts/nfft/>)

   Download the source code and build/install using the standard
   sequence of commands: `./configure`, `make`, `make
   install`. Alternatively, binary packages are available for Debian
   Linux and MacOS (via MacPorts). If using Debian Linux install the
   libnfft3-dev package. If using MacPorts install the nfft-3 port.

5. GLib 2.16 or later (<https://developer.gnome.org/glib/>)

   On Redhat/Fedora install the glib2-devel package; on Debian/Ubuntu
   install the libglib2.0-dev package. If using MacPorts install the
   glib2 port.

6. OIFITSlib 2.6.2 or later (<https://github.com/jsy1001/oifitslib/>)

7. CMake 3.6 or later (<https://cmake.org/>)

You will also require both C and Fortran 77 compilers to build BSMEM.
BSMEM has been built successfully using gcc, together with g77 or
gfortran.

#### Building PGPLOT from source

If you need to build PGPLOT from source:

Ensure you have an X11 library installed (e.g. libX11-devel &
libXt-devel packages on Redhat/Fedora, libX11-dev & libxt-dev on
Debian/Ubuntu). Create the makefile using `makemake` (see
`install-unix.txt`), then compile. Check that the C wrapper cpgplot is
installed. Finally, remember to set up your `PGPLOT_DIR` environment
variable to correctly point to the `pgxwin_server`, `rgb.txt`, and
`grfont.dat` files.

### Recommended compilers

On OS X, we recommend the use of gcc 7.3 from MacPorts. Install it and
build BSMEM as follows:

- Install MacPorts (<https://www.macports.org/>)
- Use MacPorts to install gcc7 and gcc_select
- Select this gcc and gfortran:

        sudo port select --set gcc mp-gcc7

- Check that `gcc --version` and `gfortran --version` show 7.3.0
- Make sure that bsmem's dependencies are installed (see above)
- Set `PKG_CONFIG_PATH` to include locations of .pc files for
  libraries not installed through MacPorts, in addition to the
  MacPorts .pc files directory (normally /opt/local/lib/pkgconfig )
- Build bsmem using CMake as outlined in the next section, but invoke
  cmake as `cmake -DCMAKE_C_COMPILER=gcc ..`

### Building BSMEM

CMake is now used for building BSMEM. For those not familiar with CMake,
instructions can be found at <https://cmake.org/runningcmake/>. The following
recipe to build and install BSMEM should work on any platform:

    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release
    cmake --build . --config Release
    cmake --install .

Note that the pkg-config utility is used to locate the GLib and
OIFITSLib libraries that bsmem depends on. The search path for
pkg-config must be configured to include the locations of the .pc
files for those libraries, for example by setting the
`PKG_CONFIG_PATH` environment variable.

## Usage

Instructions for the classic `bsmem` console application are given
in the file [doc/bsmem_console.md](doc/bsmem_console.md).

The common interface executable `bsmem-ci` has no built-in capability
for displaying the data or reconstructed image, and should be used
with a third-party user interface (currently in development). Those
engaged in writing such a user interface should read
[doc/common_interface.md](doc/common_interface.md).

## History

BSMEM was first written in Fortran and completed by David Buscher in
1992 to demonstrate direct maximum entropy reconstruction from optical
aperture synthesis data [Buscher 1994][].

Further developments began in 2002, started by Hrobjartur
Thorsteinsson and continued by Fabien Baron [Baron & Young
2008][]. BSMEM was ported to C, gained compatibility with the OIFITS
format (using code by John Young), and the reconstruction options and
the user interface were improved and expanded.

The common image reconstruction interface [Young et al. 2016][] has
been implemented in BSMEM by John Young.

We acknowledge funding awards from the European Community's Sixth and
Seventh Framework Programmes, which have contributed to the
development of BSMEM.

## References

[Buscher 1994]: http://adsabs.harvard.edu/abs/1994IAUS..158...91B
  "Direct maximum-entropy image reconstruction from the bispectrum,
   D. F. Buscher. In J. G. Robertson and W. J. Tango, editors, IAU
   Symp. 158: Very High Angular Resolution Imaging, p.91 (1994)."

[Baron & Young 2008]: https://doi.org/10.1117/12.789115
  "Image reconstruction at Cambridge University, F. Baron and
   J. S. Young, Proc. SPIE 7013, 70133X (2008)."

[Young et al. 2016]: https://doi.org/10.1117/12.2233381
  "User-friendly imaging algorithms for interferometry, J. S. Young,
  É. Thiébaut, G. Duvert, M. Vannier, P. Garcia, G. Mella, Proc. SPIE
  9907, 99073N (2016)."
